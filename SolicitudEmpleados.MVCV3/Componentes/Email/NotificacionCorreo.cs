﻿using EasyLoan.CorreosManager;
using SolicitudEmpleado.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SolicitudEmpleado.MVCV3.Componentes.Email
{
    public static class NotificacionCorreo
    {
		private static bool Enviar(string appKey)
		{
			return Convert.ToBoolean(ConfigurationManager.AppSettings[appKey]);
		}
        ///
		/// <summary>
        /// Enviar Correo para los documentos pendientes.
        /// </summary>
        /// <param name="solicitud">Instacina de una solicitud</param>
        /// <param name="coleccionDocumentos">Coleccion de Documentos pendientes.</param>
        /// <param name="usuario">Usuario Actual o usuario que esta asignado.</param>
        /// <param name="mensaje">Mensaje en el correo.</param>
        public static void NotificacionDocumentos(SolicitudEmpleado.Data.Solicitud solicitud, IList<SolicitudEmpleado.Models.spGetPropiedadesDeSolicitudResult> coleccionDocumentos, SeguridadComun.Usuario usuario, string mensaje, string nombreOficina, string urlTasadores, string urlSeguro)
        {
            dynamic email = new Postal.Email("NotificacionDocumentos");
            email.Mensaje = mensaje;
            email.To = solicitud.Email;
            email.Encabezado = "Notificación de Documentos Pendientes";
            email.Subtitulo = solicitud.TiposSolicitud.Nombre + ", Solicitud Núm. " + solicitud.SolicitudID.ToString("000000");
            email.Solicitud = solicitud;
            email.Coleccion = coleccionDocumentos;
            email.Usuario = usuario;
            email.Oficina = nombreOficina;
            email.UrlTasadores = urlTasadores;
            email.UrlSeguro = urlSeguro;
			if (Enviar("EmailManager.NotificacionDocumentos"))
				ValidarEnviarCorreo(email);
        }

        public static void NotificacionPrestamoRechazado(SolicitudEmpleado.Data.Solicitud solicitud, SeguridadComun.Usuario usuario, string mensaje, string nombreOficina)
        {
            dynamic email = new Postal.Email("NotificacionPrestamoRechazado");
            email.Mensaje = mensaje;
            email.To = solicitud.Email;
            email.Encabezado = "Notificación de Solicitud Rechazada";
            email.Subtitulo = solicitud.TiposSolicitud.Nombre + ", Solicitud Núm. " + solicitud.SolicitudID.ToString("000000");
            email.Solicitud = solicitud;
            email.Usuario = usuario;
            email.Oficina = nombreOficina;
			if (Enviar("EmailManager.NotificacionPrestamoRechazado"))
				ValidarEnviarCorreo(email);
        }

        public static void NotificacionCambioEstado(SolicitudEmpleado.Data.Solicitud solicitud, SeguridadComun.Usuario usuario, int estatusIDInicial, string nombreOficina)
        {
            if (estatusIDInicial > 0 && solicitud.EstatusID != estatusIDInicial && solicitud.Estatu != null && solicitud.Estatu.EnviarCorreo)
            {
                dynamic email = new Postal.Email("NotificacionCambioEstado");
                email.To = solicitud.Email;
                email.Encabezado = "Notificación Cambio de Estado";
                email.Subtitulo = solicitud.TiposSolicitud.Nombre + ", Solicitud Núm. " + solicitud.SolicitudID.ToString("000000");
                email.Solicitud = solicitud;
                email.Usuario = usuario;
                email.Oficina = nombreOficina;
				if (Enviar("EmailManager.NotificacionCambioEstado"))
					ValidarEnviarCorreo(email);
            }
        }

		public static void NotificacionInvitacion(int idInvitacion, SeguridadComun.Usuario usuario)
		{
			SolicitudEmpleado.Data.dbSolicitudesContext _db = new SolicitudEmpleado.Data.dbSolicitudesContext();
			SolicitudEmpleado.Data.Invitacion invitacion = _db.Invitaciones.Find(idInvitacion);
			string hostLinkUrl = System.Configuration.ConfigurationManager.AppSettings["HostLinkUrl"];
			dynamic email = new Postal.Email("NotificacionInvitacion");
			email.To = invitacion.Correo;
			email.Encabezado = "Notificacion para Llenar Solicitud";
			email.Subtitulo = invitacion.TiposSolicitud.Nombre + "  " + invitacion.Tipo.Nombre;
			email.Usuario = usuario;
			email.Invitacion = invitacion;
			email.LinkUrl = hostLinkUrl + "/Solicitudes/Nueva/" + invitacion.Token;
			email.Mensaje = "Para llanar la solicitud debes acceder a la plataforma ingresando con el click al boton mas abajo. El cual tiene acceso para a llenar la solicitud de " + invitacion.TiposSolicitud.Nombre + "  " + invitacion.Tipo.Nombre;
			email.UsuarioPassword = null;
			email.UsuarioUserName = null;
			if (Enviar("EmailManager.NotificacionInvitacion"))
				ValidarEnviarCorreo(email);
		}

        private static void ValidarEnviarCorreo(dynamic email)
        {
            string correoDePrueba = System.Configuration.ConfigurationManager.AppSettings["correoDePrueba"];
            bool enviarCorreo = Convert.ToBoolean(ConfigurationManager.AppSettings["EnviarCorreo"]);
            bool esCorreoValido = IsValidEmail(email.To);

            if (correoDePrueba != null && correoDePrueba.Length > 0)
            {
                email.To = correoDePrueba;
            }

            if (!enviarCorreo)
                return;

            if (!esCorreoValido)
                return;

            var _emailService = new Postal.EmailService();

            var correo = _emailService.CreateMailMessage(email);
			
			var mailToSend = new CorreosEnviar();
			mailToSend.AppId = Convert.ToInt16(ConfigurationManager.AppSettings["mailManager.App.Id"]);
			mailToSend.Body = correo.Body;
			mailToSend.Enviado = false;
			mailToSend.From = ConfigurationManager.AppSettings["SMTPUserId"];
			mailToSend.To = correo.To.ToString();
			mailToSend.Subject = correo.Subject;
			mailToSend.UsuarioId = ConfigurationManager.AppSettings["SMTPUserId"];
			mailToSend.Prioridad = 0;
			MailsManager.Enviar(mailToSend);
			//MailMessage message // Pendiente en recibiro un mail massage.
        }

        private static bool IsValidEmail(string email)
        {
            var emails = email.Split(',');

            foreach (var e in emails)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(e);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
    }
}