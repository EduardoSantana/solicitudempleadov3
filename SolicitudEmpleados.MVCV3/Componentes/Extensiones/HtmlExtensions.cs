﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml.Linq;
using SolicitudEmpleado.MVCV3.Componentes.Extensiones.Menu;
using SeguridadComun;


namespace SolicitudEmpleado.MVCV3.Componentes.Extensiones
{
	public static class ViewContextExtension
	{
		public static SolicitudEmpleados.MVCV3.Controllers.BaseSeguridadController BaseController(this ViewContext view)
		{
			SolicitudEmpleados.MVCV3.Controllers.BaseSeguridadController baseController = (SolicitudEmpleados.MVCV3.Controllers.BaseSeguridadController)view.Controller;
			return baseController;
		}
	}

	public static class HtmlExtensions
    {
		#region Menu

		public static IHtmlString MenuAplicacion(this HtmlHelper html, string rutaMenu, SolicitudEmpleados.MVCV3.Controllers.SeguridadAppV2 seguridad = null)
        {
			if (seguridad == null)
				seguridad = html.ViewContext.BaseController().ObtenerSeguridad();


			var xml = XElement.Load(Context.Server.MapPath(rutaMenu));
            FiltrarElementosPermiso(xml, seguridad);

            var result = new StringWriter();
            using (var writer = new HtmlTextWriter(result))
            {
                ImprimirOpciones(html.ViewContext, writer, xml.Elements("MenuItem"), true);
            }

            return new HtmlString(result.ToString());
        }

        private static void FiltrarElementosPermiso(XElement xml, SolicitudEmpleados.MVCV3.Controllers.SeguridadAppV2 seguridad)
        {
            bool hadElements;
            var elements = xml.Elements("MenuItem").ToList();
            foreach (var element in elements)
            {
                hadElements = element.HasElements;
                if (hadElements)
                    FiltrarElementosPermiso(element, seguridad);

                if (hadElements && !element.HasElements)
                {
                    element.Remove();
                    continue;
                }

                var nodoAcceso = element.Attribute("Acceso");
                if (nodoAcceso != null && !String.IsNullOrEmpty(nodoAcceso.Value) && !seguridad.TieneAcceso(nodoAcceso.Value))
                    element.Remove();
            }
        }

        private static void ImprimirOpciones(ViewContext context, HtmlTextWriter writer, IEnumerable<XElement> elementos, bool first = false)
        {
            if (!first)
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-menu");
            else
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav navbar-nav");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            foreach (var elemento in elementos)
            {
                var nodoHref = elemento.Attribute("Href");
                var nodoIcon = elemento.Attribute("Icono");
                var nodoProcesar = elemento.Attribute("Procesar");
                var nodoClase = elemento.Attribute("Clase");

                if (elemento.HasElements)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown");
                else if (nodoClase != null)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, nodoClase.Value);

                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                if (elemento.HasElements)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "dropdown-toggle");
                    writer.AddAttribute("data-toggle", "dropdown");
                }

                if (nodoHref == null || String.IsNullOrEmpty(nodoHref.Value))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, ObtenerHref(nodoHref.Value));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                }

                if (nodoIcon != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, nodoIcon.Value);
                    writer.RenderBeginTag(HtmlTextWriterTag.I);
                    writer.RenderEndTag();
                    writer.Write("&nbsp;");
                }

                string valor = elemento.Attribute("Nombre").Value;

                if (nodoProcesar != null && String.Equals(nodoProcesar.Value, Boolean.TrueString))
                    valor = ProcesarContenidoTexto(valor, context);

                writer.Write(valor);
                writer.RenderEndTag();

                if (elemento.HasElements)
                    ImprimirOpciones(context, writer, elemento.Descendants());

                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        private static string ProcesarContenidoTexto(string valor, ViewContext context)
        {
            return ElementosProcesados.ProcesarContenido(valor, context);
        }

        #endregion

        #region Comun

        private static HttpContext Context = HttpContext.Current;

        private static bool ValidarSeguridadModulo(string nombre, SolicitudEmpleados.MVCV3.Controllers.SeguridadAppV2 seguridad)
        {
            return seguridad.TieneAcceso(nombre);
        }

        private static string ObtenerHref(string ruta)
        {
            if (String.Equals(ruta, "#"))
                return ruta;

            if (Uri.IsWellFormedUriString(ruta, UriKind.Absolute))
                return ruta;

            return VirtualPathUtility.ToAbsolute(ruta).ToLower();
        }

        #endregion
    }
}