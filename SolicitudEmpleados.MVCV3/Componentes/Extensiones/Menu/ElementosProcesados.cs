﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleado.MVCV3.Componentes.Extensiones.Menu
{
    public static class ElementosProcesados
    {
        private static readonly Dictionary<string, Func<ViewContext, string>> _elementos;

        static ElementosProcesados()
        {
            _elementos = new Dictionary<string, Func<ViewContext, string>>();
        }

        public static void AgregarElemento(string llave, Func<ViewContext, string> metodo)
        {
            _elementos.Add(llave, metodo);
        }

        public static bool ContieneLlave(string llave)
        {
            return _elementos.ContainsKey(llave);
        }

        public static string Procesar(string llave, ViewContext contexto)
        {
            var elemento = _elementos[llave];
            if (elemento != null)
            {
                return elemento(contexto);
            }

            return null;
        }

        public static string ProcesarContenido(string contenido, ViewContext contexto)
        {
            foreach (var elemento in _elementos)
            {
                if (contenido.Contains(elemento.Key))
                    contenido = contenido.Replace(elemento.Key, elemento.Value.Invoke(contexto));
            }

            return contenido;
        }
    }
}