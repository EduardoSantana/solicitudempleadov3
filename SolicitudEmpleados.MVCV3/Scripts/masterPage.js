﻿$(document).ready(function () {

    GotoUrl("#BuscarCodigo");
    GotoUrl("#BuscarCedula");
    GotoUrl("#BuscarSolicitud");
    GotoUrl("#BuscarReferencia");

});

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function GotoUrl(nombreDelBoton) {

    $(nombreDelBoton).click(function () {

        var qt = 0;
        if (nombreDelBoton == "#BuscarCedula") { qt = 1; }
        if (nombreDelBoton == "#BuscarSolicitud") { qt = 2; }
        if (nombreDelBoton == "#BuscarReferencia") { qt = 3; }
       
        var div = document.getElementById(nombreDelBoton.replace('#', ''));
        var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
        var urlPath = 'solicitudes?q=' + $("#q").val();

        if (qt != 0) {
            urlPath += "&qt=" + qt;
        }

        Redirect(divRoot + urlPath);

    });
}

function Redirect(url) {
    var ua = navigator.userAgent.toLowerCase(),
        isIE = ua.indexOf('msie') !== -1,
        version = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
        var link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click();
    }

        // All other browsers
    else { window.location.href = url; }
}

