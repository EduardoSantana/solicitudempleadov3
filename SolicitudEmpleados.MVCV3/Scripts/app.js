﻿define(function () {
    var app = {
        inicializar: inicializar
    };
    return app;

    function inicializar() {
        configurarTooltips();
    }

    function configurarTooltips() {
        $('.tooltip').tooltip({ placement: 'left' });
    }
});