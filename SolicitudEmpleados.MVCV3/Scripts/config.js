﻿// Definicion del File Finder
var finder = (function () {
    var $body = $('body');

    return {
        getAppFile: getAppFile,
        getCdnFile: getCdnFile
    };

    function getAppFile(file) {
        var base = $body.data('root');
        return base + file;
    }

    function getCdnFile(file) {
        var cnd = $body.data('cdn');
        return cnd + file;
    }
})();

// Configuracion de RequireJS
require.config({
    baseUrl: finder.getAppFile('scripts'),
    paths: {
        // Aqui los plugins que pueden ser utilizados por su app.
    }
});

require(['app'], function (app) { app.inicializar(); });