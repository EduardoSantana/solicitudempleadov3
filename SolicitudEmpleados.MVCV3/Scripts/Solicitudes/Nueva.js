﻿$(document).ready(function () {
    processServerJS();
    $(document).on('blur', "input[type=text]", function () {
        $(this).val(function (_, val) {
            return val.toUpperCase();
        });
    });
});

function processServerJS() {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    $('*[data-validacionjs="true"]').each(function (index, element) {
        // Test App 
        // console.log('element at index ' + index + 'is ' + (this.name));
        // console.log('current element as dom object:' + element);
        // console.log('current element as jQuery object:' + $(this));
        // End Test App 
        $(this).on('change', function () {
            $.ajax({
                url: url + 'api/ValidacionJS/' + this.getAttribute('data-propiedadid') + '?q=' + this.value.trim() + '&InvitacionId=' + $("#solicitud_InvitacionId").val(),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.result = "ok") {
                        $.each(data.lista, function (index, value) {
                            // Test App 
                            // console.log('element at index ' + index + ' is ' + value);
                            // End Test App 
                            if (!isEmpty(value.value)) {
                                $(value.name).val(value.value);
                            }
                           
                        });
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

