﻿var constantePropiedades = {
    SueldoFijo: 16,
    Comisiones: 29,
    GastosRepresentacion: 31,
    OtrosIngresos: 33,
    PrestamoDirecto: 35,
    VacacionesFelices: 36,
    PrestamoHipotecario: 37,
    SeguroVidaHipotecario: 38,
    SeguroIncendioHipotecario: 39,
    PrestamoVehiculo: 40,
    PolizaVehiculo: 41,
    Cooperativa: 42,
    SeguroMedico: 43,
    AporteSeguridadSocial: 44,
    AportePlanRetiro: 45,
    OtrasDeducciones: 46,
    TotalIngreso: 17,
    DisponibleBruta: 18,
    TotalDeducciones: 19,
    DisponibilidadNeta: 20,
    CalificaPorMonto: 48,
    EsPrestamoUnico: 49,
    LiquidacionNeta: 53,
    LiquidacionBruta: 52,
    DisponiblidadCargada: 54,
    ModuloDeConsumo: 55,
    ModuloHipotecario: 56,
    DocumentosPendientes: 62,
    PrestamosRechazado: 82,
    URLTasadores: 77,
    URLSeguro: 78,
    Nombre: 80,
    Apellido: 81,
    Cedula: 82,
    Sexo: 00,
    TipoDeSangre: 83,
    EstadoCivil: 84,
    Telefono: 87,
    FechaNacimiento: 88,
    LugarNacimiento: 89,
    Nacionalidad: 90,
    Direccion: 91,
    Correo: 116,
    CabezeraNombre: 93,
    CabezeraRNC: 94,
    CabezeraCanalIndirecto: 95,
    CabezeraVigenciadeContrato: 96,
    CabezeraOtrosSubsidios: 97,
    CabezeraLineasActuales: 98,
    CabezeraUsuariosAbiertos: 99,
    CabezeraUsuariosCerrados: 100,
    DetalledePlanPlan: 101,
    DetalledePlanAdicional: 102,
    PlanesBusinessFitPlan: 104,
    DetalledePlanCantidad: 105,
    DetalledePlanMinutos: 106,
    DetalledePlanRentaUnitaria: 107,
    DetalledePlanRentaNeta: 108,
    DetalledePlanAdicionalCantidad: 109,
    DetalledePlanAdicionalMinutos: 110,
    DetalledePlanAdicionalRentaUnitaria: 111,
    DetalledePlanAdicionalRentaNeta: 112,
    PlanesBusinessFitCantidad: 113,
    PlanesBusinessFitMinutos: 114,
    PlanesBusinessFitRentaUnitaria: 115,
    PlanesBusinessFitRentaNeta: 116,
    ModelosdeEquiposModelo: 117,
    ModelosdeEquiposPrecio: 118,
    ModelosdeEquiposCantidad: 119,
    ModelosdeEquiposSubtotal: 120,
    PlanesdeDataPlan: 121,
    PlanesdeDataCantidad: 122,
    PlanesdeDataRentaUnitaria: 123,
    PlanesdeDataSubtotal: 124,
    ServiciosOpcionalesRoamingDescripcion: 125,
    ServiciosOpcionalesRoamingCantidad: 152,
    ServiciosOpcionalesRoamingPrecio: 128,
    ServiciosOpcionalesRoamingSubTotal: 129,
    ServiciosOpcionalesDistanciaDescripcion: 1155,
    ServiciosOpcionalesDistanciaCantidad: 1153,
    ServiciosOpcionalesDistanciaPrecio: 1156,
    ServiciosOpcionalesDistanciaSubTotal: 1157,
    DetalledePlanIlimitado: 2156,
    DetalledePlanIlimitadoCantidad: 2157,
    DetalledePlanIlimitadoMinutos: 2158,
    DetalledePlanIlimitadoRentaUnitaria: 2159,
    DetalledePlanIlimitadoRentaNeta: 2160,
};

$(document).ready(function () {
    processServerJS();
    processClientJS();
    manageNavigation();
    $(document).on('blur', "input[type=text]", function () {
        $(this).val(function (_, val) {
            return val.toUpperCase();
        });
    });
    addToTable();
    remToTable();
});

function processClientJS() {

    var propiedades = [
        constantePropiedades.DetalledePlanCantidad, // Deatlle Plan Plan
        constantePropiedades.DetalledePlanAdicionalCantidad,
        constantePropiedades.PlanesBusinessFitCantidad,
        constantePropiedades.ModelosdeEquiposCantidad,
        constantePropiedades.PlanesdeDataCantidad,
        constantePropiedades.ServiciosOpcionalesRoamingCantidad,
        constantePropiedades.ServiciosOpcionalesDistanciaCantidad,
        constantePropiedades.DetalledePlanIlimitadoCantidad,
        constantePropiedades.DetalledePlanIlimitado,
        constantePropiedades.DetalledePlanPlan,
    ];
    var selector = "";

    propiedades.forEach(function (value, index) {
        if (index > 0) { selector += ", "; }
        selector += "*[data-propiedadid='" + value + "']";
    });

    $(selector).on('change', function () {
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanAdicionalCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanAdicionalRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanAdicionalRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.PlanesBusinessFitCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.PlanesBusinessFitRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.PlanesBusinessFitRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ModelosdeEquiposCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ModelosdeEquiposPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ModelosdeEquiposSubtotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.PlanesdeDataCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.PlanesdeDataRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.PlanesdeDataSubtotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ServiciosOpcionalesRoamingCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesRoamingPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesRoamingSubTotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ServiciosOpcionalesDistanciaCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesDistanciaPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesDistanciaSubTotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanIlimitadoCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanIlimitadoRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanIlimitadoRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanPlan) {
            var id = parseInt(this.value.trim());
            if (id == 21) {
                $(".id-" + constantePropiedades.DetalledePlanAdicional).hide();
                $(".id-" + constantePropiedades.DetalledePlanIlimitado).show();
            } else {
                $(".id-" + constantePropiedades.DetalledePlanIlimitado).hide();
                $(".id-" + constantePropiedades.DetalledePlanAdicional).show();
            }

        }

    });

}

function processServerJS() {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    $('*[data-validacionjs="true"]').each(function (index, element) {
        $(this).on('change', function () {
            $.ajax({
                url: url + 'api/ValidacionJSV3/' + this.getAttribute('data-propiedadid') + '?q=' + this.value.trim(),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.result = "ok") {
                        $.each(data.lista, function (index, value) {
                            $(value.name).val(value.value);
                        });
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

function manageNavigation() {
    function moveNavigationTo(index) {
        $(".panels > div").removeClass("active");
        $(".panels > .panel-" + index).addClass("active");
        $("ul.breadcrumbs li").removeClass("active");
        $("ul.breadcrumbs li.li-" + index).addClass("active");
        if (index == 1) {
            $(".back").addClass("hide");
        } else {
            $(".back").removeClass("hide");
        }
    }
    $(document).on('click', ".item-li", function () {
        var index = parseInt($(this).data('index'));
        moveNavigationTo(index);
    });
    $(document).on('click', ".next", function () {
        var index = parseInt($(".panels > .active").data('index')) + 1;
        if ($(".panels > .panel-" + index).length > 0) {
            moveNavigationTo(index);
        }
        else {
            $(".submit-form").click();
        }
    });
    $(document).on('click', ".back", function () {
        var index = parseInt($(".panels > .active").data('index')) - 1;
        moveNavigationTo(index);
    });
}

function addToTable() {
    $(".add-to-table").on("click", function onclick() {
        var trClase = $(this).data("tr");
        var table = $(".ta-" + trClase.replace("tr-", ""));
        var columnas = parseInt(table.data("columnas"));

        var row = "<tr class='can-delete'>";
        for (var i = 1; i <= columnas; i++) {
            row += get_td_HTML(trClase, i);
        }
        row += "</tr>";
        table.find("tbody").append(row);
        $(".rem-to-table").show();
    });
}

function remToTable() {
    $(".rem-to-table").on("click", function onclick() {
        var trClase = $(this).data("tr");
        $(".ta-" + trClase.replace("tr-", "")).find(".can-delete").last().remove();
        var indexProcesarNew = parseInt($("#index-procesar").val());
        indexProcesarNew -= 1;
        $("#index-procesar").val(indexProcesarNew);
        var otroRow = $(".ta-" + trClase.replace("tr-", "")).find(".can-delete");
        if (otroRow.length == 0) {
            $(".rem-to-table").hide();
        }
    });
    $(".rem-to-table").hide();
}

function get_td_HTML(trClase, idIndex) {

    var objTd1 = $("." + trClase)
        .find("td:nth-child(" + idIndex + ")");

    var indexProcesarOLD = objTd1
        .find("input[id*='_PropiedadID']")
        .attr('id')
        .replace(/_/g, '')
        .replace(/Procesar/g, '')
        .replace(/PropiedadID/g, '');

    var indexProcesarNew = parseInt($("#index-procesar").val());

    var td1 = objTd1
        .html()
        .replace(new RegExp("_" + indexProcesarOLD + "_", 'g'), "_" + indexProcesarNew + "_")
        .replace(new RegExp("\\[" + indexProcesarOLD + "\\]", 'g'), "[" + indexProcesarNew + "]");

    indexProcesarNew += 1;
    $("#index-procesar").val(indexProcesarNew);

    return "<td>" + td1 + "</td>";
}