﻿var constantePropiedades = {
    SueldoFijo: 16,
    Comisiones: 29,
    GastosRepresentacion: 31,
    OtrosIngresos: 33,
    PrestamoDirecto: 35,
    VacacionesFelices: 36,
    PrestamoHipotecario: 37,
    SeguroVidaHipotecario: 38,
    SeguroIncendioHipotecario: 39,
    PrestamoVehiculo: 40,
    PolizaVehiculo: 41,
    Cooperativa: 42,
    SeguroMedico: 43,
    AporteSeguridadSocial: 44,
    AportePlanRetiro: 45,
    OtrasDeducciones: 46,
    TotalIngreso: 17,
    DisponibleBruta: 18,
    TotalDeducciones: 19,
    DisponibilidadNeta: 20,
    CalificaPorMonto: 48,
    EsPrestamoUnico: 49,
    LiquidacionNeta: 53,
    LiquidacionBruta: 52,
    DisponiblidadCargada: 54,
    ModuloDeConsumo: 55,
    ModuloHipotecario: 56,
    DocumentosPendientes: 62,
    PrestamosRechazado: 82,
    URLTasadores: 77,
    URLSeguro: 78,
    Nombre: 80,
    Apellido: 81,
    Cedula: 82,
    Sexo: 00,
    TipoDeSangre: 83,
    EstadoCivil: 84,
    Telefono: 87,
    FechaNacimiento: 88,
    LugarNacimiento: 89,
    Nacionalidad: 90,
    Direccion: 91,
    Correo: 116,
    CabezeraNombre: 93,
    CabezeraRNC: 94,
    CabezeraCanalIndirecto: 95,
    CabezeraVigenciadeContrato: 96,
    CabezeraOtrosSubsidios: 97,
    CabezeraLineasActuales: 98,
    CabezeraUsuariosAbiertos: 99,
    CabezeraUsuariosCerrados: 100,
    DetalledePlanPlan: 101,
    DetalledePlanAdicional: 102,
    PlanesBusinessFitPlan: 104,
    DetalledePlanCantidad: 105,
    DetalledePlanMinutos: 106,
    DetalledePlanRentaUnitaria: 107,
    DetalledePlanRentaNeta: 108,
    DetalledePlanAdicionalCantidad: 109,
    DetalledePlanAdicionalMinutos: 110,
    DetalledePlanAdicionalRentaUnitaria: 111,
    DetalledePlanAdicionalRentaNeta: 112,
    PlanesBusinessFitCantidad: 113,
    PlanesBusinessFitMinutos: 114,
    PlanesBusinessFitRentaUnitaria: 115,
    PlanesBusinessFitRentaNeta: 116,
    ModelosdeEquiposModelo: 117,
    ModelosdeEquiposPrecio: 118,
    ModelosdeEquiposCantidad: 119,
    ModelosdeEquiposSubtotal: 120,
    PlanesdeDataPlan: 121,
    PlanesdeDataCantidad: 122,
    PlanesdeDataRentaUnitaria: 123,
    PlanesdeDataSubtotal: 124,
    ServiciosOpcionalesRoamingDescripcion: 125,
    ServiciosOpcionalesRoamingCantidad: 152,
    ServiciosOpcionalesRoamingPrecio: 128,
    ServiciosOpcionalesRoamingSubTotal: 129,
    ServiciosOpcionalesDistanciaDescripcion: 1155,
    ServiciosOpcionalesDistanciaCantidad: 1153,
    ServiciosOpcionalesDistanciaPrecio: 1156,
    ServiciosOpcionalesDistanciaSubTotal: 1157,
    DetalledePlanIlimitado: 2156,
    DetalledePlanIlimitadoCantidad: 2157,
    DetalledePlanIlimitadoMinutos: 2158,
    DetalledePlanIlimitadoRentaUnitaria: 2159,
    DetalledePlanIlimitadoRentaNeta: 2160,
};

$(document).ready(function () {
    processServerJS();
    processClientJS();
});

function processClientJS() {

    var propiedades = [
        constantePropiedades.DetalledePlanCantidad, // Deatlle Plan Plan
        constantePropiedades.DetalledePlanAdicionalCantidad,
        constantePropiedades.PlanesBusinessFitCantidad,
        constantePropiedades.ModelosdeEquiposCantidad,
        constantePropiedades.PlanesdeDataCantidad,
        constantePropiedades.ServiciosOpcionalesRoamingCantidad,
        constantePropiedades.ServiciosOpcionalesDistanciaCantidad,
        constantePropiedades.DetalledePlanIlimitadoCantidad,
    ];
    var selector = "";

    propiedades.forEach(function (value, index) {
        if (index > 0) { selector += ", "; }
        selector += "*[data-propiedadid='" + value + "']";
    });

    $(selector).on('change', function () {
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanAdicionalCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanAdicionalRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanAdicionalRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.PlanesBusinessFitCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.PlanesBusinessFitRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.PlanesBusinessFitRentaNeta + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ModelosdeEquiposCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ModelosdeEquiposPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ModelosdeEquiposSubtotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.PlanesdeDataCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.PlanesdeDataRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.PlanesdeDataSubtotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ServiciosOpcionalesRoamingCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesRoamingPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesRoamingSubTotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.ServiciosOpcionalesDistanciaCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesDistanciaPrecio + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.ServiciosOpcionalesDistanciaSubTotal + "']").val(cantidad * precio);
            }
        }
        if (this.getAttribute('data-propiedadid') == constantePropiedades.DetalledePlanIlimitadoCantidad) {
            var cantidad = parseInt(this.value.trim());
            var precio = parseInt($("*[data-propiedadid='" + constantePropiedades.DetalledePlanIlimitadoRentaUnitaria + "']").val());
            if (cantidad > 0 && precio > 0) {
                $("*[data-propiedadid='" + constantePropiedades.DetalledePlanIlimitadoRentaNeta + "']").val(cantidad * precio);
            }
        }

    });

}

function processServerJS() {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    $('*[data-validacionjs="true"]').each(function (index, element) {
        // Test App 
        // console.log('element at index ' + index + 'is ' + (this.name));
        // console.log('current element as dom object:' + element);
        // console.log('current element as jQuery object:' + $(this));
        // End Test App 
        $(this).on('change', function () {
            $.ajax({
                url: url + 'api/ValidacionJSV3/' + this.getAttribute('data-propiedadid') + '?q=' + this.value.trim(),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.result = "ok") {
                        $.each(data.lista, function (index, value) {
                            // Test App 
                            // console.log('element at index ' + index + ' is ' + value);
                            // End Test App 
                            if (!isEmpty(value.value)) {
                                $(value.name).val(value.value);
                            }

                        });
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

