﻿
$(document).ready(function () {

    var IdTotalIngreso = "#TotalIngreso_0__Valor";
    var IdTotalDesducc = "#TotalDeducciones_0__Valor";
    var clIngresos = ".EditarIngresos";
    var clDeduccin = ".EditarDeducciones";

    calcularControles(clIngresos, IdTotalIngreso, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 1, 1);
    calcularControles(clIngresos, IdTotalIngreso, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 1, 2);

    calcularControles(clDeduccin, IdTotalDesducc, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 2, 1);
    calcularControles(clDeduccin, IdTotalDesducc, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 2, 2);

    cargarTablaPrestamos();
    
});

function cargarTablaPrestamos() {
   
    var nombreDivContenedor = '#divContenedorPrestamos';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');
 
    $(nombreDivContenedor).jtable({
        title: '',
        jqueryuiTheme: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: urlBase + '/PrestamosList'
        },
        fields: {
            NumeroFormato: {
                title: 'Numero'
            },
            Descripcion: {
                title: 'Nombre Producto'
            },
            FechaDesembolso: {
                title: 'Fecha',
                type: 'date',
                displayFormat: 'dd/mm/yy'
            },
            ValorCuota : {
                title: 'Monto Cuota',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.ValorCuota, 2, '.', ',') + '</span>';
                }
            },
            MontoAprobado: {
                title: 'Monto Aprobado',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.MontoAprobado, 2, '.', ',') + '</span>';
                }
            },
            TotalAdeudado: {
                title: 'Total Adeudado',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.TotalAdeudado, 2, '.', ',') + '</span>';
                }
            },
            PorcientoPagado: {
                title: '% Pagado',
                display: function (data) {
                    return '<div class="text-right">' + porcientoPagado(data.record.TotalAdeudado, data.record.MontoAprobado) + '</div>';
                }
            }

        }
        
    });

    $(nombreDivContenedor).jtable('load', {
        Identificacion: $('#solicitud_Identificacion').val(),
        TipoIdentificacion: $('#solicitud_TipoIdentificacionID').val()
    });

}

function porcientoPagado(montoAdeudado, montoTotal) {
    var total = 1 - ( parseFloat(montoAdeudado.replace(',', '')) / parseFloat(montoTotal.replace(',', '')) );

    if (total < 0) {
        total = 0;
    }

    return number_format(total * 100, 0, '.', ',') + '%';

}

function calcularControles(ClaseEditar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo, intLoad) {

    var $Editar = $(ClaseEditar);

    if (intLoad == 1) {
        $Editar.change(function () {
            total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo);
        });
    }
    if (intLoad == 2) {
        total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo);
    }

}

function total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo) {
    var retVal = 0.0;
    if ($Editar.length > 0) {

        for (i = 0; i < $Editar.length; i++) {
            if (!isNaN($Editar[i].value.replace(',', ''))) {
                retVal += parseInt($Editar[i].value.replace(',', ''));
                $Editar[i].value = number_format($Editar[i].value.replace(',', ''), 2, '.', ',')
            }
        }

        $(IdTotal)[0].value = number_format(retVal, 2, '.', ',');

        if (intTipo == 1) {
            var decDisponiblidadBruta = parseFloat(retVal * 45 / 100);
            $(IdDisponiblidadBruta)[0].value = number_format(decDisponiblidadBruta, 2, '.', ',');
            var decTotalDeducciones = parseFloat($(IdDisponiblidadNeta.replace('1', '0'))[0].value.replace(',', ''));
            var decDisponiblidadNeta = parseFloat(decDisponiblidadBruta - decTotalDeducciones);
            //Poner Disponibildiad En Negativo
            //if (decDisponiblidadNeta < 0) {
            //    decDisponiblidadNeta = 0;
            //}
            $(IdDisponiblidadNeta)[0].value = number_format(decDisponiblidadNeta, 2, '.', ',');
        }
        if (intTipo == 2) {
            var decDisponiblidadBruta = parseFloat($(IdDisponiblidadBruta)[0].value.replace(',', ''));
            var decDisponiblidadNeta = parseFloat(decDisponiblidadBruta - retVal);
            //Poner Disponibildiad En Negativo
            //if (decDisponiblidadNeta < 0) {
            //    decDisponiblidadNeta = 0;
            //}
            $(IdDisponiblidadNeta)[0].value = number_format(decDisponiblidadNeta, 2, '.', ',');
        }
    }
}