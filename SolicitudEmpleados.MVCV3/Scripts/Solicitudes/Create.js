﻿
$(document).ready(function () {

    var IdTotalIngreso = "#TotalIngreso_0__Valor";
    var IdTotalDesducc = "#TotalDeducciones_0__Valor";
    var clIngresos = ".EditarIngresos";
    var clDeduccin = ".EditarDeducciones";

    calcularControles(clIngresos, IdTotalIngreso, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 1, 1);
    calcularControles(clIngresos, IdTotalIngreso, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 1, 2);

    calcularControles(clDeduccin, IdTotalDesducc, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 2, 1);
    calcularControles(clDeduccin, IdTotalDesducc, IdTotalIngreso.replace('0', '1'), IdTotalDesducc.replace('0', '1'), 2, 2);

});

function porcientoPagado(montoAdeudado, montoTotal) {
    var total = 1 - (parseFloat(montoAdeudado.replace(',', '')) / parseFloat(montoTotal.replace(',', '')));

    if (total < 0) {
        total = 0;
    }

    return number_format(total * 100, 0, '.', ',') + '%';

}

function calcularControles(ClaseEditar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo, intLoad) {

    var $Editar = $(ClaseEditar);

    if (intLoad == 1) {
        $Editar.change(function () {
            total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo);
        });
    }
    if (intLoad == 2) {
        total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo);
    }

}

function total_suma($Editar, IdTotal, IdDisponiblidadBruta, IdDisponiblidadNeta, intTipo) {
    var retVal = 0.0;
    if ($Editar.length > 0) {

        for (i = 0; i < $Editar.length; i++) {
            if (!isNaN($Editar[i].value.replace(',', ''))) {
                retVal += parseInt($Editar[i].value.replace(',', ''));
                $Editar[i].value = number_format($Editar[i].value.replace(',', ''), 2, '.', ',')
            }
        }

        $(IdTotal)[0].value = number_format(retVal, 2, '.', ',');

        if (intTipo == 1) {
            var decDisponiblidadBruta = parseFloat(retVal * 45 / 100);
            $(IdDisponiblidadBruta)[0].value = number_format(decDisponiblidadBruta, 2, '.', ',');
            var decTotalDeducciones = parseFloat($(IdDisponiblidadNeta.replace('1', '0'))[0].value.replace(',', ''));
            var decDisponiblidadNeta = parseFloat(decDisponiblidadBruta - decTotalDeducciones);
            //Poner Disponibildiad En Negativo
            //if (decDisponiblidadNeta < 0) {
            //    decDisponiblidadNeta = 0;
            //}
            $(IdDisponiblidadNeta)[0].value = number_format(decDisponiblidadNeta, 2, '.', ',');
        }
        if (intTipo == 2) {
            var decDisponiblidadBruta = parseFloat($(IdDisponiblidadBruta)[0].value.replace(',', ''));
            var decDisponiblidadNeta = parseFloat(decDisponiblidadBruta - retVal);
            //Poner Disponibildiad En Negativo
            //if (decDisponiblidadNeta < 0) {
            //    decDisponiblidadNeta = 0;
            //}
            $(IdDisponiblidadNeta)[0].value = number_format(decDisponiblidadNeta, 2, '.', ',');
        }
    }
}