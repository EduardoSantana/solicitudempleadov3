﻿function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getQueryString(item) {
    var retVal = '';
    var $_GET = getQueryParams(document.location.search);
    var collectSearchString = $_GET[item];
    if (!isBlank(collectSearchString)) {
        retVal = collectSearchString;
    }
    return retVal;
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

function escapeRegex(value) {
    return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
}

function BuscarEnTablaCliente(IdBoton, ClasRow, Columnas) {
    $(IdBoton).bind("keyup paste", function () {
        var searchText = $(IdBoton).val(),
            regex = new RegExp(escapeRegex(searchText), "gi"),
            $items = $(ClasRow).show();

        //Bloque para buscar en client.
        //if (searchText) {
        //    $items.each(function () {
        //        var $tds = $("td", this).slice(0, Columnas),
        //            text = $tds.text();
        //        if (text.search(regex) === -1) {
        //            $(this).hide();
        //        } else {
        //            $(this).show();
        //        }
        //    });
        //}
        //Termina Bloque para buscar en client.

        //Bloque para buscar en Servidor.
        var nombreDivContenedor = '#divContenedorSolicitudes';
        var div = document.getElementById(nombreDivContenedor.replace('#', ''));
        var viewType = div.getAttribute('data-viewtype');

        var delay = 800; // milliseconds

        setTimeout(function () {
            //your code to be executed after 1 seconds
            $(nombreDivContenedor).jtable('load', {
                EmpresaID: 1,
                EstatusID: $('#EstatusID').val(),
                TipoID: $('#TipoID').val(),
                Asignado: $('#Asignado').val(),
                searchString: $(IdBoton).val()
            });
        }, delay);
        //Termina Bloque para buscar en servidor.
        
    });
}

function AjustesDeTabla() {
    var IdBoton = '#search-table';
    var ClasRow = '.jtable-data-row';
    var Columnas = 7; // Cantidad de Columnas de la tabla
    $(".jtable-toolbar").append('<input id="' + IdBoton.replace("#", "") + '" type="search" placeholder="Búsqueda..." style="margin: 0px; margin-right: 0px;" >');
    BuscarEnTablaCliente(IdBoton, ClasRow, Columnas);
    $("#ui-id-4").text("Asignar Solicitud");
}

function CargarTablaSolicitudes() {

    var nombreDivContenedor = '#divContenedorSolicitudes';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');
    var viewType = div.getAttribute('data-viewtype');
    var supervisor = div.getAttribute('data-supervisor');
    var appName = div.getAttribute('data-app-name');
    var q = getQueryString("q");
    var qt = getQueryString("qt");
   
    $(nombreDivContenedor).jtable({
        title: "Listado de " + appName,
        jqueryuiTheme: true,
        paging: true,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/SolicitudesList' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&q=' + q +
                                '&qt=' + qt +
                                '&viewType=' + viewType,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                            $(nombreDivContenedor + " .popover-link").popover({ placement: 'top', trigger: 'hover', html: true });
                            $(nombreDivContenedor + " .jtable-edit-command-button").attr("title", "Asignar");
                            if (viewType == 0 || supervisor != 'true') {
                                $(nombreDivContenedor + " .jtable-command-column").hide();
                                $(nombreDivContenedor + " .jtable-command-column-header").hide();
                            }
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            },
            updateAction: urlBase + '/SolicitudUpdate'
        },
        fields: {
            SolicitudID: {
                title: 'Número',
                key: true,
                create: false,
                edit: false,
                list: true,
                width: '10%',
                display: function (data) {
                    var returnUrl = "";
                    if (viewType > 1) {
                        returnUrl = '?returnUrl=' + window.location.pathname;
                    }
                    var botonProcesar = (viewType > 0) ? ' <a style="margin-left: 5px;" title="Procesar Solicitud" href="' + urlBase + '/Procesar/' + data.record.SolicitudID + returnUrl + '" > <i class="icon-cog"></i></a>' : '';
                    return $('<a data-content="<b>Código:</b> ' + data.record.CodigoEmpleado +
                                      ' <br><b>Nombre:</b> ' + data.record.NombreEmpleado +
                                      '" title="Detalles de la Solicitud: ' + data.record.SolicitudID +
                                      '" class="popover-link" href="' + urlBase + '/Detalle/' + data.record.SolicitudID + '">' + data.record.SolicitudID + '</a> ' +
                                      botonProcesar);
                }
            },
            NombreEmpleado: {
                title: 'Nombre',
                width: '23%',
                create: false,
                edit: false
            },

            EstatusID: {
                title: 'Estado',
                options: urlBase + '/GetEstatusOptions',
                width: '10%',
                create: false,
                edit: false
            },
            TipoID: {
                title: 'Tipo',
                options: urlBase + '/GetTiposOptions',
                width: '20%',
                create: false,
                edit: false
            },
            Fecha: {
                title: 'Fecha',
                type: 'date',
                displayFormat: 'dd/mm/yy',
                width: '12%',
                create: false,
                edit: false
            },
            Monto: {
                title: 'Monto',
                width: '12%',
                create: false,
                edit: false,
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.Monto, 2, '.', ',') + '</span>';
                }
            },
            AsignadoVisible: {
                title: 'Asignada a',
                width: '10%',
                create: false,
                edit: false,
                display: function (data) {

                    var linkSeguridad = "cargarModal('" + urlBase.replace('Solicitudes', '') + "Seguridad/Usuario/{0}')";
                    linkSeguridad = linkSeguridad.replace('{0}', data.record.AsignadoA);


                    if (data.record.AsignadoA != null) {
                        return $("<a data-content='" + data.record.AsignadoANombre + "'" +
                                      ' title="Asignado ' + data.record.AsignadoA + '"' +
                                      ' class="popover-link_no_mostrar" href="#" ' + 
                                      ' onclick="' + linkSeguridad + '">' + data.record.AsignadoA + '</a>');
                    } else {
                        return '';
                    }
                    
                }
                //<a href="#" onclick="cargarModal('@Url.Action("Edit", "Propiedades", new { id = item.PropiedadID })')">Editar</a>
            },
            AsignadoA: {
                title: 'Asignada a',
                width: '10%',
                create: false,
                edit: true,
                list: false,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetAsignadoOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetAsignadoOptions/' + data.record.SolicitudID;
                }
            }
        }
    });

    $('.table-update-trigger .form-control').on('change', function () {
        var IdBoton = '#search-table';
        $(nombreDivContenedor).jtable('load', {
            EmpresaID: 1,
            EstatusID: $('#EstatusID').val(),
            TipoID: $('#TipoID').val(),
            Asignado: $('#Asignado').val(),
            searchString: $(IdBoton).val()
        });
    });
    
    $(nombreDivContenedor).jtable('load');
}

$(document).ready(function ()
{
    CargarTablaSolicitudes();
    AjustesDeTabla();
});
