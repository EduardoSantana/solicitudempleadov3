﻿//----------------------------------------
// Breadcrumbs
//----------------------------------------

$(".breadcrumbs li").last().addClass("last");

$('.breadcrumbs li a').each(function () {

    if ($(this).parent('li').hasClass('active') || $(this).parent('li').hasClass('first')) {

    } else {

        $(this).css('width', 140 + 'px');

        $(this).mouseover(function () {
            $(this).css('width', 160 + 'px');
        });

        $(this).mouseout(function () {
            $(this).css('width', 140 + 'px');
        });
    }

});