﻿
$(document).ready(function () {

    cargarTablaPrestamos();

});

function cargarTablaPrestamos() {

    var nombreDivContenedor = '#divContenedorPrestamos';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');

    $(nombreDivContenedor).jtable({
        title: '',
        jqueryuiTheme: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: urlBase + '/PrestamosList'
        },
        fields: {
            NumeroFormato: {
                title: 'Numero'
            },
            Descripcion: {
                title: 'Producto'
            },
            FechaDesembolso: {
                title: 'Fecha',
                type: 'date',
                displayFormat: 'dd/mm/yy'
            },
            ValorCuota: {
                title: 'Cuota',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.ValorCuota, 2, '.', ',') + '</span>';
                }
            },
            MontoAprobado: {
                title: 'Aprobado',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.MontoAprobado, 2, '.', ',') + '</span>';
                }
            },
            TotalAdeudado: {
                title: 'Adeudado',
                display: function (data) {
                    return '<span style="float: right;">' + number_format(data.record.TotalAdeudado, 2, '.', ',') + '</span>';
                }
            },
            PorcientoPagado: {
                title: '% Pagado',
                display: function (data) {
                    return '<div class="text-right">' + porcientoPagado(data.record.TotalAdeudado, data.record.MontoAprobado) + '</div>';
                }
            }

        }

    });

    $(nombreDivContenedor).jtable('load', {
        Identificacion: $('#Identificacion').val(),
        TipoIdentificacion: $('#TipoIdentificacionID').val()
    });

}

function porcientoPagado(montoAdeudado, montoTotal) {
    var total = 1 - (parseFloat(montoAdeudado.replace(',', '')) / parseFloat(montoTotal.replace(',', '')));

    if (total < 0) {
        total = 0;
    }

    return number_format(total * 100, 0, '.', ',') + '%';

}
