﻿$(document).ready(function () {

    cargarTablaDisponibilidad();
});

function cargarTablaDisponibilidad() {
    var nombreDivContenedor = '#divContenedorDisponiblidad';
    var idBotonBuscar = '#btnBuscar';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');
    $(nombreDivContenedor).jtable({
        title: '',
        jqueryuiTheme: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: urlBase + '/EmpleadosList'
        },
        fields: {
            Codigo: {
                title: 'Código',
                display: function (data) {
                    var retVal = $('<a title="Consulta Disponiblidad" href="' + urlBase + '/Detalle/' + data.record.Codigo + '" > ' + data.record.Codigo + ' </i>  </a>');
                    if (data.record.IdEmpresa != "1") {
                        retVal = $('<a title="Consulta Disponiblidad" href="' + urlBase + '/Detalle/' + data.record.Codigo + '?empresa=' + data.record.IdEmpresa + '" > ' + data.record.Codigo + ' </i>  </a>');
                    }
                    return retVal;
                }
            },
            Identificacion: {
                title: 'Identificación'
            },
            NombreCompleto: {
                title: 'Nombre'
            },
            IdEmpresa: {
                title: 'Empresa',
                options: urlBase + '/GetEmpresaOptions'
            }

        }
    });

    $(idBotonBuscar).click(function (e) {
        e.preventDefault();

        $(nombreDivContenedor).jtable('load', {
            codigoEmpleado: $('#codigoEmpleado').val(),
            EmpresaID: $('#EmpresaID').val()
        });

    });


}
