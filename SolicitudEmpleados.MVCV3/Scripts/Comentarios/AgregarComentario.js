﻿$(document).ready(function () {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    $("#btnAddComents").on("click", function () {
        $.ajax({
            url: url + 'Comentarios/ComentarioInsert',
            type: 'POST',
            dataType: 'json',
            data: {
                intSolicitudID: $("#intSolicitudID").val(),
                strValor: $("#strValor").val()
            },
            success: function (data) {
                if (data.Result == "OK") {
                    var row =   " <tr>  " +
                                "     <td>   <a href='#'>Comentario</a>  " +
                                "     </td>  " +
                                "     <td>  " + data.User +
                                "     </td>  " +
                                "     <td>  " + data.Date +
                                "     </td>  " +
                                "     <td>  " + $("#strValor").val() +
                                "     </td>  " +
                                " </tr>  ";
                    $("#ta-comentarios tbody").append(row);
                    $("#strValor").val('');
                } else {
                    alert(data.Message);
                }
            }
        });
    })
});