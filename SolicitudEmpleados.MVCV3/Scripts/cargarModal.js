﻿function cargarModal(url) {
    var $modal = $('#page-modal');

    if (!$modal.hasClass('hide')) {
        $modal.modal('hide');
    }

    $.get(url)
        .done(function (html) {
            $modal.find('.modal-body').html(html);
            var texto = $modal.find('.titulo').text();
            $modal.find('h3').html(texto);
            $modal.find('.titulo').hide();
            $modal.find('.region').removeClass("row");
            $modal.find('.form-actions').addClass("modal-footer");
            $modal.find('.form-actions').removeClass("col-md-offset-2");
            $modal.find('.form-actions').removeClass("form-actions");
            $modal.modal('show');
        });
}

function cargarModalHTML(html, titulo) {
    var $modal = $('#page-modal');

    if (!$modal.hasClass('hide')) {
        $modal.modal('hide');
    }

    $modal.find('.modal-body').html(html);
    $modal.find('.modal-title').html(titulo);
    $modal.modal('show');

}
