﻿$(document).ready(function () {
    var body = document.getElementsByTagName('body');
    var url = body[0].getAttribute('data-root');
    ActualizarNombre();

    $("#UsuarioID").change(function () {
        ActualizarNombre();
    });
    
    function ActualizarNombre() {
        $.ajax({
            url: url + 'Bloqueos/BuscarEmpleados' +
                        '?codigoEmpleado=' + $("#UsuarioID").val() +
                        '&EmpresaID=' + $("#EmpresaID").val(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.Records.length > 0) {
                    $("#NombreEmpleado").val(data.Records[0].NombreCompleto);
                }
            }
        });
    }
});