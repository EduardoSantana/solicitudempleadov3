﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data.Entity;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Microsoft.AspNet.Identity.EntityFramework
{

}



namespace SolicitudEmpleados.MVCV3.Models
{

	public partial class ApplicationRole : IdentityRole
	{
		public ApplicationRole() : base() { }
		public ApplicationRole(string name, string description) : base(name)
		{
			this.Description = description;
		}
		public virtual string Description { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Accesos> Accesos { get; set; }
	}

	public partial class Accesos
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Accesos()
		{
			Roles = new HashSet<ApplicationRole>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int idAcceso { get; set; }

		[Required]
		[StringLength(256)]
		public string Nombre { get; set; }

		[StringLength(256)]
		public string Descripcion { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<ApplicationRole> Roles { get; set; }
	}



	//public class ApplicationUser : IdentityUser<string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
	//{
	//	public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
	//	{
	//		var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
	//		return userIdentity;
	//	}
	//}
	//public class ApplicationUser : IdentityUser

	// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
	public class ApplicationUser : IdentityUser<string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
	{
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, string> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int idUsuario { get; set; }

		public bool? activo { get; set; }

		[Required]
		[StringLength(256)]
		[DisplayName("Apellido")]
		public string apellido { get; set; }

		[StringLength(256)]
		[DisplayName("Cedula")]
		public string cedula { get; set; }

		[StringLength(256)]
		public string cedulaForm { get; set; }

		[StringLength(256)]
		[DisplayName("Celular")]
		public string celular { get; set; }

		[StringLength(256)]
		public string celularForm { get; set; }
		[DisplayName("Codigo Empleado")]
		public int? codigoEmpleado { get; set; }

		[StringLength(256)]
		public string codOficina { get; set; }

		[StringLength(256)]
		[DisplayName("Correo")]
		public string correo { get; set; }

		[StringLength(256)]
		public string departamento { get; set; }

		public int? empresa { get; set; }

		public int? ext { get; set; }

		[StringLength(256)]
		public string ip { get; set; }

		[StringLength(256)]
		public string navegador { get; set; }

		[Required]
		[StringLength(256)]
		[DisplayName("Nombre")]
		public string nombre { get; set; }

		[StringLength(256)]
		public string nombreCompleto { get; set; }

		[StringLength(256)]
		public string nomOficina { get; set; }

		[StringLength(256)]
		public string puesto { get; set; }

		[StringLength(256)]
		[DisplayName("Telefono")]
		public string telefono { get; set; }

		[StringLength(256)]
		public string telefonoForm { get; set; }

		[StringLength(256)]
		public string usuario { get; set; }

		[StringLength(256)]
		public string clave { get; set; }

		//internal Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager userManager)
		//{
		//	throw new NotImplementedException();
		//}
	}

	//public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    //{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
	{
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Accesos>()
				.Property(e => e.Nombre)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.Property(e => e.Descripcion)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.HasMany(e => e.Roles)
				.WithMany(e => e.Accesos)
				.Map(m => m.ToTable("RolesAccesos").MapLeftKey("idAcceso").MapRightKey("idRol"));

			// Change these from IdentityRole to ApplicationRole:
			EntityTypeConfiguration<ApplicationRole> entityTypeConfiguration1 = modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles");

			base.OnModelCreating(modelBuilder);
		}

		public virtual DbSet<Accesos> Accesos { get; set; }

	
	}

}