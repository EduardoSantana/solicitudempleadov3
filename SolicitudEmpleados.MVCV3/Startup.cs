﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolicitudEmpleados.MVCV3.Startup))]
namespace SolicitudEmpleados.MVCV3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
