﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using SeguridadComun;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class SeguridadController : BaseSeguridadController
	{
        //
        // GET: /Seguridad/Usuario-Conectado

        [ActionName("Usuario-Conectado")]
        public ActionResult UsuarioConectado()
        {
            //var seguridad = ObtenerSeguridad();
            var usuario = ObtenerUsuario();

            ViewBag.OpcionesAvanzadas = true;
			ViewBag.GrupoUsuario = "NombreGrupo";

            if (Request.IsAjaxRequest())
                return PartialView(usuario);

            return View(usuario);
        }

        //
        // GET: /Seguridad/Usuario

        public ActionResult Usuario(int id)
        {
            var usuario = BuscarUsuario(id);

            if (usuario == null)
                return HttpNotFound();

            if (Request.IsAjaxRequest())
                return PartialView(usuario);

            return View(usuario);
        }

    }
}