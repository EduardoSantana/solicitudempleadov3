﻿using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SolicitudEmpleados.MVCV3.Controllers.Api
{
	public class ValidacionJSV3Controller : ApiController
	{
		private dbSolicitudesContext dbSolicitud = new dbSolicitudesContext();

		[ResponseType(typeof(retvalidacionjs))]
		// POST: api/ValidacionJS
		public IHttpActionResult Post(int id, string q)
		{
            var retVal = new retvalidacionjs();
            
            if (id == CodigoPropiedad.DetalledePlanPlan)
			{
                var objPlan = dbSolicitud.Planes.Find(Convert.ToInt32(q));
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanMinutos), value = Convert.ToString(objPlan.minutos) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaUnitaria), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaNeta), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanMinutos), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaUnitaria), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanRentaNeta), value = "" });
                }
            }
            if (id == CodigoPropiedad.DetalledePlanAdicional)
            {
                var objPlan = dbSolicitud.Planes_Adicional.Find(Convert.ToInt32(q));
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalMinutos), value = Convert.ToString(objPlan.minutos) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalRentaUnitaria), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalRentaNeta), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalMinutos), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalRentaUnitaria), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanAdicionalRentaNeta), value = "" });
                }
            }
            if (id == CodigoPropiedad.PlanesBusinessFitPlan)
            {
                var objPlan = dbSolicitud.Planes_Bsfit.Find(Convert.ToInt32(q));
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitMinutos), value = Convert.ToString(objPlan.minutos) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitRentaUnitaria), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitRentaNeta), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitMinutos), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitRentaUnitaria), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesBusinessFitRentaNeta), value = "" });
                }
            }
            if (id == CodigoPropiedad.PlanesdeDataPlan)
            {
                var objPlan = dbSolicitud.Planes_Bsfit.Find(Convert.ToInt32(q));
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataRentaUnitaria), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataSubtotal), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataRentaUnitaria), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.PlanesdeDataSubtotal), value = "" });
                }
            }
            if (id == CodigoPropiedad.ModelosdeEquiposModelo)
            {
                var objPlan = dbSolicitud.Modelos.Find(q);
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposPrecio), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposSubtotal), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposPrecio), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.ModelosdeEquiposSubtotal), value = "" });
                }
            }
            if (id == CodigoPropiedad.DetalledePlanIlimitado)
            {
                var objPlan = dbSolicitud.Planes_Flotilla.Find(Convert.ToInt32(q));
                if (objPlan != null)
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoCantidad), value = "1" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoMinutos), value = Convert.ToString(objPlan.minutos) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoRentaUnitaria), value = Convert.ToString(objPlan.precio) });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoRentaNeta), value = Convert.ToString(objPlan.precio) });
                }
                else
                {
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoCantidad), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoMinutos), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoRentaUnitaria), value = "" });
                    retVal.lista.Add(new retitem { name = string.Format("*[data-propiedadid='{0}']", CodigoPropiedad.DetalledePlanIlimitadoRentaNeta), value = "" });
                }
            }
            return Ok(retVal);
		}
	}
}
