﻿using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.MVCV3.Componentes.Email;
using SeguridadComun;
using System.Web.Configuration;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class NotificacionController : BaseController
    {
        // GET: Notificacion
        public ActionResult Nueva(int id = 0)
        {
            var modelo = new NuevaNotificacionViewModel();
            modelo.SolicitudId = id;
            ViewBag.ListaTipoNotificacion = new SelectList(unitOfWork.SolicitudesRepository.GetPropiedades(id, PropiedadesTipos.TiposNotificaciones).ToList(), "PropiedadID", "Nombre");
            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }

        [HttpPost]
        public ActionResult Nueva(NuevaNotificacionViewModel modelo)
        {
            ViewBag.ListaTipoNotificacion = new SelectList(unitOfWork.SolicitudesRepository.GetPropiedades(modelo.SolicitudId, PropiedadesTipos.TiposNotificaciones).ToList(), "PropiedadID", "Nombre");

            if (modelo == null || modelo.SolicitudId < 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var solicitud = unitOfWork.SolicitudesRepository.GetByID(modelo.SolicitudId);

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var coleccionDocumentos = unitOfWork.SolicitudesRepository
                       .GetPropiedades(solicitud.SolicitudID, SolicitudEmpleado.Helpers.PropiedadesTipos.Documentos)
                       .Where(c => Convert.ToBoolean(c.Valor))
                       .ToList();

                    SeguridadComun.Usuario usuario;
                    if (solicitud.AsignadoA != null && solicitud.AsignadoA > 0)
                    {
                        usuario = BuscarUsuario((int)solicitud.AsignadoA);
                    }
                    else
                    {
                        usuario = ObtenerUsuario();
                    }

                    //var oficina = BuscarOficina(usuario.codOficina);
                    string nombreOficina = "";
                    //if (oficina != null)
                    //{
                    //    nombreOficina = oficina.Descripcion;
                    //}

                    if (modelo.TipoNotificacion == CodigoPropiedad.DocumentosPendientes)
                    {
                        string urlTasadores = unitOfWork.SolicitudesRepository.getPropiedadById(0, CodigoPropiedad.URLTasadores).PorDefecto;

                        string urlSeguro = unitOfWork.SolicitudesRepository.getPropiedadById(0, CodigoPropiedad.URLSeguro).PorDefecto;

                        NotificacionCorreo.NotificacionDocumentos(solicitud, coleccionDocumentos, usuario, modelo.Mensaje, nombreOficina, urlTasadores, urlSeguro);

                    }
                    else if (modelo.TipoNotificacion == CodigoPropiedad.PrestamosRechazado)
                    {
                        NotificacionCorreo.NotificacionPrestamoRechazado(solicitud, usuario, modelo.Mensaje, nombreOficina);
                    }
                    TempData["MensajeError"] = "ok|Notificacion Enviada Correctamente.";
                }
                catch (Exception ex)
                {
                    TempData["MensajeError"] = "error|" + ex.Message;
                }
            }

            return RedirectToAction("Procesar", "Solicitudes", new { id = modelo.SolicitudId });
        }
    }
}