﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class EstatusController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: Estatus
        public ActionResult Index()
        {
            var estatus = db.Estatus.Include(e => e.TiposSolicitud);
            return View(estatus.ToList());
        }

        // GET: Estatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estatu estatu = db.Estatus.Find(id);
            if (estatu == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(estatu);

            return View(estatu);
        }

        // GET: Estatus/Create
        public ActionResult Create()
        {
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Estatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EstatusID,Nombre,Descripcion,TipoSolicitudID,Activo,Accion,EnviarCorreo")] Estatu estatu)
        {
            if (ModelState.IsValid)
            {
                estatu.AuditoriaCrear(currentUser.codigoEmpleado);
                db.Estatus.Add(estatu);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
                    ModelState.AddModelError("", "No se ha podido guardar estado. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(estatu);
                }
                catch (Exception ex)
                {
                    ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
                    ModelState.AddModelError("", "No se ha podido guardar estado. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(estatu);
                }

                return RedirectToAction("Index");
            }

            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", estatu.TipoSolicitudID);
            
            if (Request.IsAjaxRequest())
                return PartialView(estatu);

            return View(estatu);
        }

        // GET: Estatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estatu estatu = db.Estatus.Find(id);
            if (estatu == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", estatu.TipoSolicitudID);
            if (Request.IsAjaxRequest())
                return PartialView(estatu);

            return View(estatu);
        }

        // POST: Estatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EstatusID,Nombre,Descripcion,TipoSolicitudID,Activo,Accion,EnviarCorreo")] Estatu estatu)
        {
            if (ModelState.IsValid)
            {
                estatu.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(estatu).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
                    ModelState.AddModelError("", "No se ha podido guardar estado. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(estatu);
                }
                catch (Exception ex)
                {
                    ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
                    ModelState.AddModelError("", "No se ha podido guardar estado. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(estatu);
                }

                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", estatu.TipoSolicitudID);
            if (Request.IsAjaxRequest())
                    return PartialView(estatu);

            return View(estatu);
        }

        // GET: Estatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estatu estatu = db.Estatus.Find(id);
            if (estatu == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(estatu);

            return View(estatu);
        }

        // POST: Estatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estatu estatu = db.Estatus.Find(id);
            db.Estatus.Remove(estatu);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
                ModelState.AddModelError("", "No se ha podido eliminar estado. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(estatu);
            }

            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
