﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;
using SolicitudEmpleados.MVCV3.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class TiposController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();
		private ApplicationDbContext dbAuth = new ApplicationDbContext();
        // GET: Tipos
        public ActionResult Index()
        {
            var tipos = db.Tipos.Include(t => t.TiposSolicitud);
            return View(tipos.ToList());
        }

        // GET: Tipos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo tipo = db.Tipos.Find(id);
            if (tipo == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(tipo);

            return View(tipo);
        }

        // GET: Tipos/Create
        public ActionResult Create()
        {
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
			ViewBag.Leer = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre");
			ViewBag.Escribir = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre");
			ViewBag.Editar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre");
			ViewBag.Eliminar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre");
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Tipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "TipoID,Nombre,Descripcion,TipoSolicitudID,CreadoPor,FechaCreacion,ModificadoPor,FechaModificacion,Activo,Leer, Escribir, Editar, Eliminar ")] Tipo tipo)
        {
            if (ModelState.IsValid)
            {
                tipo.AuditoriaCrear(currentUser.codigoEmpleado);
                db.Tipos.Add(tipo);
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView(tipo);

                return RedirectToAction("Index");
            }

            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", tipo.TipoSolicitudID);
			ViewBag.Leer = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Leer);
			ViewBag.Escribir = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Escribir);
			ViewBag.Editar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Editar);
			ViewBag.Eliminar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Eliminar);
            if (Request.IsAjaxRequest())
                return PartialView(tipo);

            return View(tipo);
        }

        // GET: Tipos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo tipo = db.Tipos.Find(id);
            if (tipo == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", tipo.TipoSolicitudID);
			ViewBag.Leer = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Leer);
			ViewBag.Escribir = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Escribir);
			ViewBag.Editar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Editar);
			ViewBag.Eliminar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Eliminar);
            if (Request.IsAjaxRequest())
                return PartialView(tipo);

            return View(tipo);
        }

        // POST: Tipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "TipoID,Nombre,Descripcion,TipoSolicitudID,CreadoPor,FechaCreacion,ModificadoPor,FechaModificacion,Activo,Leer, Escribir, Editar, Eliminar ")] Tipo tipo)
        {
            if (ModelState.IsValid)
            {
                tipo.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(tipo).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", tipo.TipoSolicitudID);
			ViewBag.Leer = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Leer);
			ViewBag.Escribir = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Escribir);
			ViewBag.Editar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Editar);
			ViewBag.Eliminar = new SelectList(dbAuth.Accesos, "idAcceso", "Nombre", tipo.Eliminar);
            if (Request.IsAjaxRequest())
                    return PartialView(tipo);

            return View(tipo);
        }

        // GET: Tipos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo tipo = db.Tipos.Find(id);
            if (tipo == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(tipo);

            return View(tipo);
        }

        // POST: Tipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tipo tipo = db.Tipos.Find(id);
            db.Tipos.Remove(tipo);
            db.SaveChanges();
            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
