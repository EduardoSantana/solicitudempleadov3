﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleados.MVCV3.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using SolicitudEmpleado.Models;


namespace SolicitudEmpleados.MVCV3.Controllers
{
	[ValidarAcceso("AccesoUsuarios")]
	public class UsuariosController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(applicationUser);

            return View(applicationUser);
        }

		// GET: Usuarios/Roles/5
		[ValidarAcceso("AccesoRoles")]
		public ActionResult Roles(string id)
		{
			var modelo = new UsuariosRolesViewModel();
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = db.Users.Find(id);
			if (applicationUser == null)
			{
				return HttpNotFound();
			}

			modelo.UserId = applicationUser.Id;
			modelo.UserName = applicationUser.UserName;
			modelo.nombre = applicationUser.nombre;
			modelo.apellido = applicationUser.apellido;
			modelo.idUsuario = applicationUser.idUsuario;
			modelo.Listado = db.Roles.Where(p => p.Users.Any(p2 => p2.UserId == id)).Select(p3=> new RolesListViewModel { RolName = p3.Name, RolId = p3.Id }).ToList();

			if (Request.IsAjaxRequest())
				return PartialView(modelo);

			return View(modelo);
		}
		[ValidarAcceso("AccesoRoles")]
		public ActionResult AgregarRoles(string id)
		{
			var modelo = new RolesListViewModel();
			modelo.UserId = id;

			ViewBag.RolId = new SelectList(db.Roles.Where(p => !p.Users.Any(p2 => p2.UserId == modelo.UserId)).ToList(), "Name", "Name");

			if (Request.IsAjaxRequest())
				return PartialView(modelo);

			return View(modelo);
		}

		[HttpPost]
		[ValidarAcceso("AccesoRoles")]
		[ValidateAntiForgeryToken]
		public ActionResult AgregarRoles(RolesListViewModel modelo)
		{
			ViewBag.RolId = new SelectList(db.Roles.Where(p => !p.Users.Any(p2 => p2.UserId == modelo.UserId)).ToList(), "Name", "Name", modelo.RolId);

			try
			{
				var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
				if (UserManager != null)
				{
					UserManager.AddToRole(modelo.UserId, modelo.RolId);
				}
				//db.SaveChanges();
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}

				return View(modelo);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

				return View(modelo);
			}

			return RedirectToAction("Roles", new { id = modelo.UserId });

		}
		[ValidarAcceso("AccesoRoles")]
		public ActionResult EliminarRoles(string id, string rolesId)
		{
			
			try
			{
				var objUsuario = db.Users.Find(id);
				foreach (var item in objUsuario.Roles.ToList())
				{
					if(item.RoleId == rolesId)
					{
						objUsuario.Roles.Remove(item);
					}
				}
				db.SaveChanges();
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}
				
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}
				
			}

			return RedirectToAction("Roles", "Usuarios", new { id = id });

		}


		// GET: Usuarios/Create
		public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,idUsuario,activo,apellido,cedula,cedulaForm,celular,celularForm,codigoEmpleado,codOficina,correo,departamento,empresa,ext,ip,navegador,nombre,nombreCompleto,nomOficina,puesto,telefono,telefonoForm,usuario,clave,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
			applicationUser.Id = "";
            if (ModelState.IsValid)
            {

				try
                {
					
					var user = new ApplicationUser()
					{
						Id = (Guid.NewGuid()).ToString(),
						UserName = applicationUser.UserName,
						Email = applicationUser.Email,
						nombre = applicationUser.nombre,
						apellido = applicationUser.apellido,
						usuario = applicationUser.UserName,
						correo = applicationUser.Email,
						codigoEmpleado = applicationUser.codigoEmpleado,
						activo = true
					};

					var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
					if (UserManager != null)
					{
						var currentUser = UserManager.Find(user.UserName, applicationUser.PasswordHash);
						if (currentUser == null)
						{
							var result = UserManager.Create(user, applicationUser.PasswordHash);
							if (result.Errors.Any())
							{
								foreach (var item in result.Errors)
								{
									ModelState.AddModelError("", "No se ha podido guardar ApplicationUser. Por los siguientes errores: ");
									ModelState.AddModelError("", item);
								}
								return View(applicationUser);
							}
						}
					}

					db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ModelState.AddModelError("", "No se ha podido guardar ApplicationUser. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(applicationUser);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar ApplicationUser. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(applicationUser);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(applicationUser);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(applicationUser);

            return View(applicationUser);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(applicationUser);

            return View(applicationUser);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,idUsuario,activo,apellido,cedula,cedulaForm,celular,celularForm,codigoEmpleado,codOficina,correo,departamento,empresa,ext,ip,navegador,nombre,nombreCompleto,nomOficina,puesto,telefono,telefonoForm,usuario,clave,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                applicationUser.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(applicationUser);

            return View(applicationUser);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(applicationUser);

            return View(applicationUser);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar ApplicationUser. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(applicationUser);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
