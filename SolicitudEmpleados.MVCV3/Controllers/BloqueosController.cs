﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.DAL;
using SeguridadComun;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class BloqueosController : BaseController
    {
        // GET: Bloqueos
        public ActionResult Index()
        {
            var courses = viewUsuarioBloqueados.LoadUsuariosBloqueados();
            return View(courses.ToList());
        }

        // GET: Bloqueos/Detalle/5
        public ActionResult Detalle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var detalle = new DetalleBloqueoViewModel();

            var usuarioDetalle = (from pp in viewUsuarioBloqueados.LoadUsuariosBloqueados() 
                                 where pp.UsuarioID == id 
                                 select pp).FirstOrDefault();

            var usuarioDetalleLogs = (from pp in unitOfWork.BloqueosLogRepository.Get()
                                where pp.UsuarioID == id
                                orderby pp.FechaCreacion descending
                                select pp).ToList();

            detalle.bloqueo = usuarioDetalle;

            detalle.bloqueosLogs = usuarioDetalleLogs;

            if (usuarioDetalle == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(detalle);

            return View(detalle);
        }

        // GET: Bloqueos/Crear
        [OutputCache(Duration = 0, VaryByParam = "*", NoStore = true)]
        public ActionResult Bloquear(int? id, int? empresaID)
        {
            ViewBag.EmpresaID = new SelectList(unitOfWork.EmpresaRepository.Get().ToList(), "EmpresaID", "Nombre");
            if (id != null && empresaID != null) 
            {
                var bloqueo = new Bloqueo();
                bloqueo.UsuarioID = (int)id;
                bloqueo.EmpresaID = (int)empresaID;

                if (Request.IsAjaxRequest())
                    return PartialView(bloqueo);
                
                return View(bloqueo);
            }

            if (Request.IsAjaxRequest())
                return PartialView();
 
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Bloquear([Bind(Include = "BloqueoID,UsuarioID,Motivo,Comentario,EmpresaID,Nombre")] Bloqueo bloqueo)
        {
            //Empleado temp1 = ws.BuscarEmpleadoUnico((int)bloqueo.UsuarioID, bloqueo.EmpresaID);

			Usuario temp1 = BuscarUsuario((int)bloqueo.UsuarioID);

			if (ModelState.IsValid)
            {
                if (temp1 != null)
                {
                    ViewBag.Nombre = temp1.nombreCompleto;
                    bloqueo.CreadoPor_UsuarioDeRed = currentUser.nombreCompleto;
                    bloqueo.CreadoPor = currentUser.codigoEmpleado;
                    bloqueo.IP = currentUser.ip;
                    bloqueo.FechaCreacion = DateTime.Now;
                    bloqueo.Nombre = temp1.nombreCompleto;
                    BloqueosLog nuevoLog = CrearNuevoLog(bloqueo, true);
                    unitOfWork.BloqueosLogRepository.Insert(nuevoLog);
                    unitOfWork.BloqueoRepository.Insert(bloqueo);

                    try
                    {
                        unitOfWork.Save();
                        return RedirectToAction("Detalle", new { id = bloqueo.UsuarioID });
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        ViewBag.EmpresaID = new SelectList(unitOfWork.EmpresaRepository.Get().ToList(), "EmpresaID", "Nombre");
                        ModelState.AddModelError("", "No se ha podido Guardar bloqueo. Por los siguientes errores: ");
                        var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                        foreach (var item in listaValidaciones)
                        {
                            foreach (var itemInside in item.ValidationErrors)
                            {
                                ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                            }
                        }

                        return View(bloqueo);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.EmpresaID = new SelectList(unitOfWork.EmpresaRepository.Get().ToList(), "EmpresaID", "Nombre");
                        ModelState.AddModelError("", "No se ha podido Guardar bloqueo. Por los siguientes errores: ");
                        ModelState.AddModelError("", ex.Message);
                        if (ex.InnerException != null)
                        {
                            string mensajeErrorInner = ex.InnerException.ToString();
                            if (mensajeErrorInner.Length > 500)
                            {
                                mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                            }
                            ModelState.AddModelError("", mensajeErrorInner);
                        }

                        return View(bloqueo);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "No Existe el Usuario con ID seleccionado. Por favor elige nuevo usuario.");
                }
                ViewBag.EmpresaID = new SelectList(unitOfWork.EmpresaRepository.Get().ToList(), "EmpresaID", "Nombre");
                return View(bloqueo);

            }
            else if (temp1 != null)
            {
                ViewBag.Nombre = temp1.nombreCompleto;
            }
                
            ViewBag.EmpresaID = new SelectList(unitOfWork.EmpresaRepository.Get().ToList(), "EmpresaID", "Nombre", bloqueo.EmpresaID);
            return View(bloqueo);
        }

        private BloqueosLog CrearNuevoLog(Bloqueo bloqueo, bool bloqueado)
        {
            var nuevoLog = new BloqueosLog();

            nuevoLog.Comentario = bloqueo.Comentario;
            nuevoLog.Motivo = bloqueo.Motivo;
            nuevoLog.Nombre = bloqueo.Nombre;
            nuevoLog.CreadoPor_UsuarioDeRed = currentUser.nombreCompleto;
            nuevoLog.CreadoPor = currentUser.codigoEmpleado;
            nuevoLog.Bloqueado = bloqueado;
            nuevoLog.FechaCreacion = DateTime.Now;
            nuevoLog.EmpresaID = bloqueo.EmpresaID;
            nuevoLog.UsuarioID = bloqueo.UsuarioID;
            nuevoLog.IP = currentUser.ip;

            return nuevoLog;
        }

        // GET: Bloqueos/Eliminar/5
        [OutputCache(Duration = 0, VaryByParam = "*", NoStore = true)]
        public ActionResult Desbloquear(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bloqueo bloqueo = unitOfWork.BloqueoRepository.GetByID(id);
            if (bloqueo == null)
            {
                return HttpNotFound();
            }
            bloqueo.Motivo = "";
            bloqueo.Comentario = "";

            if (Request.IsAjaxRequest())
                return PartialView(bloqueo);

            return View(bloqueo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Desbloquear(Bloqueo bloqueo)
        {
            if (ModelState.IsValid) { 
                BloqueosLog nuevoLog = CrearNuevoLog(bloqueo, false);
                unitOfWork.BloqueosLogRepository.Insert(nuevoLog);
                unitOfWork.BloqueoRepository.Delete(bloqueo.BloqueoID);

                try
                {
                    unitOfWork.Save();
                    return RedirectToAction("Detalle", new { id = bloqueo.UsuarioID });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Desbloquear. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(bloqueo);
                }
                
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult BuscarEmpleados(int codigoEmpleado, int EmpresaID = 1)
        {
            try
            {
                //var listaEmpleados = ws.BuscarEmpleados(codigoEmpleado.ToString(), EmpresaID,  1);

				//var listaEmpleados = ws.BuscarEmpleados(codigoEmpleado, EmpresaID, 1);

				var listaEmpleados = new List<Usuario>();

				var usuario = BuscarUsuario(codigoEmpleado);

				listaEmpleados.Add(usuario);

				return Json(new { Result = "OK", Records = listaEmpleados, TotalRecordCount = listaEmpleados.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}
