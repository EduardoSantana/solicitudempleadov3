﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class ReferenciaEikonsController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: ReferenciaEikons
        public ActionResult Index()
        {
            return View(db.ReferenciasEikons.ToList());
        }

        // GET: ReferenciaEikons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferenciaEikon referenciaEikon = db.ReferenciasEikons.Find(id);
            if (referenciaEikon == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(referenciaEikon);

            return View(referenciaEikon);
        }

        // GET: ReferenciaEikons/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: ReferenciaEikons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReferenciaEikonId,Nombre")] ReferenciaEikon referenciaEikon)
        {
            if (ModelState.IsValid)
            {
                referenciaEikon.AuditoriaCrear(currentUser.codigoEmpleado);
                db.ReferenciasEikons.Add(referenciaEikon);
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView(referenciaEikon);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(referenciaEikon);

            return View(referenciaEikon);
        }

        // GET: ReferenciaEikons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferenciaEikon referenciaEikon = db.ReferenciasEikons.Find(id);
            if (referenciaEikon == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(referenciaEikon);

            return View(referenciaEikon);
        }

        // POST: ReferenciaEikons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReferenciaEikonId,Nombre")] ReferenciaEikon referenciaEikon)
        {
            if (ModelState.IsValid)
            {
                referenciaEikon.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(referenciaEikon).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(referenciaEikon);

            return View(referenciaEikon);
        }

        // GET: ReferenciaEikons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferenciaEikon referenciaEikon = db.ReferenciasEikons.Find(id);
            if (referenciaEikon == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(referenciaEikon);

            return View(referenciaEikon);
        }

        // POST: ReferenciaEikons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReferenciaEikon referenciaEikon = db.ReferenciasEikons.Find(id);
            db.ReferenciasEikons.Remove(referenciaEikon);
            db.SaveChanges();
            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
