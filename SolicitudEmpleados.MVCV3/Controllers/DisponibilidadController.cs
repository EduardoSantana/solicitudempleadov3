﻿using SeguridadComun;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class DisponibilidadController : BaseController
    {

        #region Paginas Actions

        public ActionResult Index()
        {
            ViewBag.EmpresaID = new SelectList(getListaEmpresa(), "EmpresaID", "Nombre");
            return View();
        }

        public ActionResult Detalle(int id, int empresa = 1)
        {
            var retVal = new DetalleDisponibViewModel();
			var empleado = BuscarUsuario(id);
			//var empleado = ws.BuscarEmpleadoUnico(id, empresa);

			retVal.codigoEmpleado = empleado.codigoEmpleado;
            retVal.nombreEmpleado = empleado.nombreCompleto;
			retVal.cedula = empleado.cedulaForm;
			retVal.posicion = empleado.puesto;
			retVal.tiempoServicio = "";
			retVal.fechaIngreso = DateTime.Now;
			retVal.Identificacion = empleado.cedulaForm;
            retVal.TipoIdentificacionID = 1;

            //Para Buscar Directamente en el Web Service.
            var objDisponbilidad = unitOfWork.SolicitudesRepository.GetDisponiblidadLista(id, empresa, true);

            //Para Buscar Como sale en procesar, agrupando los resultados.
            //var objDisponbilidad = unitOfWork.SolicitudesRepository.GetDisponiblidadLista(id, empresa, 0);

            retVal.ingresos = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).ToList();
            retVal.deduccio = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones).ToList();

            retVal.totalIngresos = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.TotalesIngresos).ToList();
            retVal.totalDeduccio = objDisponbilidad.Where(p => p.TipoPropiedadID == PropiedadesTipos.TotalesDeducciones).ToList();

            return View(retVal);
        }

        #endregion

        #region Lista Empleados JSON

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult EmpleadosList(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "", string codigoEmpleado = "", int EmpresaID = 1)
        {
            try
            {
                if (EmpresaID == 0)
                {
                    EmpresaID = 1;
                }
				//var listaEmpleados = ws.BuscarEmpleados(codigoEmpleado, EmpresaID, 1);
				var listaEmpleados = new List<Usuario>();

				var usuario = BuscarUsuario(int.Parse(codigoEmpleado));

				listaEmpleados.Add(usuario);

				return Json(new { Result = "OK", Records = listaEmpleados, TotalRecordCount = listaEmpleados.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

    }
}