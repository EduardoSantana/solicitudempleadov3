﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.DAL;
using SeguridadComun;
using SolicitudEmpleado.Models;


namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class ComentariosController : BaseController
    {

        #region Agregar Listado de Comentarios

        public PartialViewResult _Comentarios(int SolicitudID)
        {
            ViewBag.SolicitudID = SolicitudID;
            IEnumerable<Historial> historiales = unitOfWork.SolicitudesRepository.Get().Where(m => m.SolicitudID == SolicitudID).FirstOrDefault().Historials;
            return PartialView("_Comentarios", historiales);
        }

        [ChildActionOnly]
        public PartialViewResult _AgregarComentario(int SolicitudID)
        {
            var obj = new ComentarioViewModel();
            ViewBag.SolicitudID = obj.intSolicitudID;
            obj.intSolicitudID = SolicitudID;

            return PartialView("_AgregarComentario", obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult _ComentarioInsert(ComentarioViewModel obj)
        {
            ViewBag.SolicitudID = obj.intSolicitudID;
            Historial nuevoHistorial = new Historial()
            {
                SolicitudID = obj.intSolicitudID,
                FechaCreacion = DateTime.Now,
                CreadoPor = currentUser.codigoEmpleado,
                CreadoPorUsuarioDeRed = currentUser.usuario,
                Comentario = obj.strValor,
                EstatusID = 10
            };

            unitOfWork.HistorialRepository.Insert(nuevoHistorial);
            unitOfWork.Save();

            var db = new dbSolicitudesContext();
            IEnumerable<Historial> historiales = db.Historials.Where(p => p.SolicitudID == obj.intSolicitudID).ToList();

            return PartialView("_Comentarios", historiales);
        }

        [HttpPost]
        public JsonResult ComentarioInsert(ComentarioViewModel obj)
        {
            try
            {
                Historial nuevoHistorial = new Historial()
                {
                    SolicitudID = obj.intSolicitudID,
                    FechaCreacion = DateTime.Now,
                    CreadoPor = currentUser.codigoEmpleado,
                    CreadoPorUsuarioDeRed = currentUser.usuario,
                    Comentario = obj.strValor,
                    EstatusID = 10
                };
                unitOfWork.HistorialRepository.Insert(nuevoHistorial);
                unitOfWork.Save();
                return Json(new {
                    Result = "OK",
                    User = nuevoHistorial.CreadoPorUsuarioDeRed,
                    Date = nuevoHistorial.FechaCreacion.ToString()
                });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

    }
}