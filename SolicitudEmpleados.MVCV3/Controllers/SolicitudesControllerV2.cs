﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SeguridadComun;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;
using SolicitudEmpleado.MVCV3.Componentes.Email;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public partial class SolicitudesController
    {
        /// <summary>
        /// Crear nueva solicitud
        /// </summary>
        /// <param name="id">TipoSolicitud ID </param>
        /// <returns>Vista creada</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Nueva(string id)
        {
            var db = new dbSolicitudesContext();

            var objInvitacion = db.Invitaciones.Where(p => p.Token == id).FirstOrDefault();

            if (objInvitacion == null)
            {
                return HttpNotFound();
            }

            var solicitud = db.Solicitudes.FirstOrDefault(f => f.InvitacionId == objInvitacion.InvitacionId);
            var modelo = new EditarSolicitudViewModel();

            if (solicitud == null)
            {
                solicitud = new Solicitud();
                var objTipo = db.Tipos.Find(objInvitacion.TipoID);
                solicitud.TipoSolicitudID = (int)objTipo.TipoSolicitudID;
                solicitud.TipoID = objTipo.TipoID;
                solicitud.Tipos = objTipo;
                solicitud.InvitacionId = objInvitacion.InvitacionId;
                solicitud.TipoIdentificacionID = 1;
                modelo.Procesar = unitOfWork.SolicitudesRepository.getPropiedadesByTipoID(objTipo.TipoID).ToList();
            }
            else
            {
                modelo.Procesar = unitOfWork.SolicitudesRepository.GetPropiedades(solicitud.SolicitudID).ToList();
            }

            modelo.solicitud = solicitud;
            ViewBag.TipoSolicitudNombre = modelo.solicitud.Tipos.TiposSolicitud.Nombre + " " + modelo.solicitud.Tipos.Nombre;

            return View(modelo);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Nueva(EditarSolicitudViewModel modelo, string returnUrl)
        {
            if (modelo == null || modelo.solicitud == null)
            {
                return HttpNotFound();
            }
            var solicitud = new Solicitud();

            if (modelo.solicitud.SolicitudID > 0)
            {
                solicitud = unitOfWork.SolicitudesRepository.GetByID(modelo.solicitud.SolicitudID);
            }
            else
            {
                solicitud = modelo.solicitud;
                solicitud.EstatusID = 1;
                solicitud.EmpresaID = 1;
                solicitud.Identificacion = "";
                solicitud.NombreEmpleado = "";
                solicitud.Email = "noemail@noemail.noemail";
                solicitud.Posicion = "";
                solicitud.Departamento = "";
                solicitud.Sueldo = 0;
                solicitud.Monto = 0;
                solicitud.FechaSolicitud = DateTime.Now;
                solicitud.TipoEmpleadoID = 2;
            }

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            var objInvitacion = unitOfWork.InvitacionRepository.GetByID(solicitud.InvitacionId);
            if (objInvitacion == null || objInvitacion.Token == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {

                string nombreUsuario = objInvitacion.Nombre.Trim() + " " + (String.IsNullOrEmpty(objInvitacion.Apellido) ? "" : objInvitacion.Apellido.Trim());
                objInvitacion.Token = null;
                unitOfWork.InvitacionRepository.Update(objInvitacion);

                int estadoIdInicial = 0;

                if (modelo.solicitud.EstatusID > 0)
                {
                    estadoIdInicial = solicitud.EstatusID;
                    solicitud.EstatusID = modelo.solicitud.EstatusID;
                    solicitud.Estatu = unitOfWork.EstatusRepository.GetByID(modelo.solicitud.EstatusID);
                }

                var nuevaSolicitud = false;

                if (solicitud.SolicitudID > 0)
                {
                    solicitud.AuditoriaEditar(0);
                    var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                    unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Procesar, solicitud, 0, nombreUsuario, listaHistorial);
                    unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, 0, nombreUsuario, estadoIdInicial);
                    unitOfWork.SolicitudesRepository.Update(solicitud);
                }
                else
                {
                    solicitud.AuditoriaCrear(0);
                    unitOfWork.SolicitudesRepository.Insert(solicitud);
                    nuevaSolicitud = true;
                }

                try
                {
                    if (nuevaSolicitud)
                    {
                        unitOfWork.Save();
                        var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                        unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Procesar, solicitud, 0, nombreUsuario, listaHistorial);
                        unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, 0, nombreUsuario, estadoIdInicial);
                    }

                    unitOfWork.Save();

                    modelo.SolicitudEnviada = true;

                    if (estadoIdInicial != solicitud.EstatusID)
                    {
                        TempData["MensajeError"] = "ok|Solicitud Guardada como " + solicitud.Estatu.Nombre + ".";
                    }
                    else
                    {
                        TempData["MensajeError"] = "ok|Solicitud Guardada Correctamente.";
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }
                    return View(modelo);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    TempData["MensajeError"] = "error|No se ha podido guardar la solicitud. Por los siguientes errores: " + ex.Message;
                    return View(modelo);
                }

            }
            else
            {
                foreach (var item in ModelState.Values)
                {
                    var asdfasdf = item;
                    var asdfasdffda = item.Errors;
                    var asdfasdfasdfas = item.Value;
                }
            }

            EditarSolicitudViewModel retVal = new EditarSolicitudViewModel();
            retVal.SolicitudEnviada = modelo.SolicitudEnviada;
            retVal.solicitud = solicitud;
            retVal.Procesar = unitOfWork.SolicitudesRepository.GetPropiedades(retVal.solicitud.SolicitudID).ToList(); 

            return View(retVal);

        }

        /// <summary>
		/// Crear solicitud de tipo Id
		/// </summary>
		/// <returns>Vista creada</returns>
		[HttpGet]
        public ActionResult Crear(int id)
        {
            var db = new dbSolicitudesContext();
            var objTipo = db.Tipos.Find(id);
            if (objTipo == null)
            {
                return HttpNotFound();
            }
            var solicitud = new Solicitud();
            var modelo = new EditarSolicitudViewModel();
            modelo.MostrarSoloErrorPrincipal = true;
            solicitud.TipoSolicitudID = (int)objTipo.TipoSolicitudID;
            solicitud.TipoID = objTipo.TipoID;
            solicitud.Tipos = objTipo;
            solicitud.TipoIdentificacionID = 1;
            modelo.solicitud = solicitud;
            modelo.Procesar = unitOfWork.SolicitudesRepository.getPropiedadesByTipoID(objTipo.TipoID).ToList();
            ViewBag.TipoSolicitudNombre = modelo.solicitud.Tipos.TiposSolicitud.Nombre + " " + modelo.solicitud.Tipos.Nombre;
            if (objTipo.TiposSolicitud != null && !string.IsNullOrEmpty(objTipo.TiposSolicitud.ViewCreate))
            {
                return View(objTipo.TiposSolicitud.ViewCreate, modelo);
            }
            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(EditarSolicitudViewModel modelo, string returnUrl)
        {
            if (modelo == null || modelo.solicitud == null)
            {
                return HttpNotFound();
            }
            var solicitud = new Solicitud();

            if (modelo.solicitud.SolicitudID > 0)
            {
                solicitud = unitOfWork.SolicitudesRepository.GetByID(modelo.solicitud.SolicitudID);
            }
            else
            {
                solicitud = modelo.solicitud;
                solicitud.EstatusID = 1;
                solicitud.EmpresaID = 1;
                solicitud.Identificacion = currentUser.cedulaForm;
                solicitud.CodigoEmpleado = currentUser.codigoEmpleado;
                solicitud.NombreEmpleado = currentUser.nombreCompleto;
                solicitud.Email = (string.IsNullOrEmpty(currentUser.correo) ? "NOT@TINE.MAIL" : currentUser.correo);
                solicitud.Posicion = "";
                solicitud.Departamento = "";
                solicitud.Sueldo = 0;
                solicitud.Monto = 0;
                solicitud.FechaSolicitud = DateTime.Now;
                solicitud.TipoEmpleadoID = 2;
            }

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                int estadoIdInicial = 0;

                if (modelo.solicitud.EstatusID > 0)
                {
                    estadoIdInicial = solicitud.EstatusID;
                    solicitud.EstatusID = modelo.solicitud.EstatusID;
                    solicitud.Estatu = unitOfWork.EstatusRepository.GetByID(modelo.solicitud.EstatusID);
                }

                var nuevaSolicitud = false;

                if (solicitud.SolicitudID > 0)
                {
                    solicitud.AuditoriaEditar(0);
                    var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                    unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Procesar, solicitud, 0, currentUser.usuario, listaHistorial);
                    unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, 0, currentUser.usuario, estadoIdInicial);
                    unitOfWork.SolicitudesRepository.Update(solicitud);
                }
                else
                {
                    solicitud.AuditoriaCrear(0);
                    unitOfWork.SolicitudesRepository.Insert(solicitud);
                    nuevaSolicitud = true;
                }

                try
                {
                    if (nuevaSolicitud)
                    {
                        unitOfWork.Save();
                        var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                        unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Procesar, solicitud, 0, currentUser.usuario, listaHistorial);
                        unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, 0, currentUser.usuario, estadoIdInicial);
                    }

                    unitOfWork.Save();

                    modelo.SolicitudEnviada = true;

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar la solicitud. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                }

            }

            EditarSolicitudViewModel retVal = new EditarSolicitudViewModel();
            retVal.MostrarSoloErrorPrincipal = false;
            retVal.SolicitudEnviada = modelo.SolicitudEnviada;
            retVal.solicitud = solicitud;
            retVal.Procesar = AsignarValoresFormulario(modelo.Procesar,retVal.solicitud.SolicitudID);

            return View(retVal);

        }

        private List<spGetPropiedadesDeSolicitudResult> AsignarValoresFormulario(IList<spGetPropiedadesDeSolicitudResult> lista, int solicitudId)
        {
            var retVal = unitOfWork.SolicitudesRepository.GetPropiedades(solicitudId);

            foreach (var item in retVal)
            {
                var obj = lista.Where(f => f.PropiedadID == item.PropiedadID).FirstOrDefault();
                item.Valor = obj.Valor;
            }

            return retVal.ToList();
        }

    }
}
