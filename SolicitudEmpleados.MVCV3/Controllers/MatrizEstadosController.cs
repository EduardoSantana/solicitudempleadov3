﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class MatrizEstadosController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: MatrizEstados
        public ActionResult Index(int id = 0)
        {
            ViewBag.ListaTipos = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre");
            var modelo = new ListadoMatrizViewModel();
            modelo.Listado = db.MatrizEstatuses.Include(m => m.EstatusActual).Include(m => m.EstatusPuedePasar);
            
            if (id > 0)
            {
                modelo.Listado = modelo.Listado.Where(p => p.EstatusActualID == id).ToList();
                modelo.DesdeEstadoId = id;
                return View(modelo);
            }

            return View(modelo);
        }

        // GET: MatrizEstados/Create
        public ActionResult Create(int id = 0)
        {
            ViewBag.EstatusActualID = new SelectList(db.Estatus.Where(p=>p.Activo == true), "EstatusID", "Nombre", id);
            ViewBag.EstatusPuedePasarID = new SelectList(db.Estatus.Where(p=>p.Activo == true), "EstatusID", "Nombre");

            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: MatrizEstados/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EstatusActualID,EsSupervisor,EstatusPuedePasarID")] MatrizEstado matrizEstado)
        {
            if (ModelState.IsValid)
            {

                db.MatrizEstatuses.Add(matrizEstado);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.EstatusActualID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusActualID);
                    ViewBag.EstatusPuedePasarID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusPuedePasarID);
                    ModelState.AddModelError("", "No se ha podido Guardar Matriz Estado. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(matrizEstado);
                }
                catch (Exception ex)
                {

                    ViewBag.EstatusActualID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusActualID);
                    ViewBag.EstatusPuedePasarID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusPuedePasarID);

                    ModelState.AddModelError("", "No se ha podido Guardar Matriz Estado. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(matrizEstado);
                }
                
                return RedirectToAction("Index", new { id = matrizEstado.EstatusActualID });
            }

            ViewBag.EstatusActualID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusActualID);
            ViewBag.EstatusPuedePasarID = new SelectList(db.Estatus.Where(p => p.Activo == true), "EstatusID", "Nombre", matrizEstado.EstatusPuedePasarID);

            if (Request.IsAjaxRequest())
                return PartialView(matrizEstado);

            return View(matrizEstado);
        }

        // GET: MatrizEstados/Delete/5
        public ActionResult Delete(int? id, bool? id1, int? id2)
        {
            if (id == null || id1 == null || id2 == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MatrizEstado matrizEstado = db.MatrizEstatuses.Find(id, id1, id2);
            if (matrizEstado == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(matrizEstado);

            return View(matrizEstado);
        }

        // POST: MatrizEstados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, bool id1, int id2)
        {
            MatrizEstado matrizEstado = db.MatrizEstatuses.Find(id, id1, id2);
            db.MatrizEstatuses.Remove(matrizEstado);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar Matriz Estado. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(matrizEstado);
            }

            if (Request.IsAjaxRequest())
                return PartialView();

            return RedirectToAction("Index", new { id = matrizEstado.EstatusActualID });

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
