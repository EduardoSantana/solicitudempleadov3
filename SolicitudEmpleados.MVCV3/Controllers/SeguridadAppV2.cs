﻿using Microsoft.AspNet.Identity.EntityFramework;
using SeguridadComun;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleados.MVCV3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SolicitudEmpleados.MVCV3.Controllers
{
	public class SeguridadAppV2
	{
		private string _userName { get; set; }
		private Usuario _usuario { get; set; }
		private List<SeguridadMod> _modulos { get; set; }
		public SeguridadAppV2(string userName, Usuario user)
		{
			_userName = userName;
			_usuario = user;
			initClass();
		}
		public SeguridadAppV2(string userName)
		{
			_userName = userName;
			initClass();
		}

		private void initClass()
		{
			this.idAplicacion = 53;
			this.idGrupo = 0;
			this.nombreAplicacion = "NombreAplicacion";
			this.aceptado = true;
			this.admin = true;
			this.CumpleReglas = true;
			this.EsAdministrador = true;
			this.UsuarioAccesa = true;
			this.updateListaAcceso();
			this.updateListaModulos();

		}

		private void updateListaModulos() 
		{
			var _db = new dbSolicitudesContext();
			this._modulos = new List<SeguridadMod>();
			foreach (Tipo item in _db.Tipos)
			{
				this._modulos.Add(BuscarModulo(item.TipoID));
			}
		}
		//
		// Summary:
		//     Grupo por defecto al que sera logueado el usuario en caso de asi requerirlo
		public int? grupoDefecto { get; set; }
		//
		// Summary:
		//     Identificacion de la aplicacion en la base de datos
		public int idAplicacion { get; set; }
		//
		// Summary:
		//     Id del grupo al que pertenece para esa aplicacion
		public int idGrupo { get; set; }
		//
		// Summary:
		//     Nombre de la aplicacion en la base de datos
		public string nombreAplicacion { get; set; }
		//
		// Summary:
		//     Nombre del grupo
		public string nombreGrupo { get; set; }
		//
		// Summary:
		//     La informacion completa del usuario
		public Usuario usuario { get { return _usuario; } }
		//
		// Summary:
		//     Valor que indica si el usuario se encuentra banneado
		public bool? aceptado { get; set; }
		//
		// Summary:
		//     Valor que contiene si el usuario es administrador o no
		public bool? admin { get; set; }
		//
		// Summary:
		//     Un string para conectarse a la base de datos
		protected string connstr { get; set; }
		//
		// Summary:
		//     Valor que indica que ya hemos verificado todos los aspectos de la seguridad para
		//     determinar si el usuario tiene permisos
		protected bool? postCheck { get; set; }
		//
		// Summary:
		//     Url del web service de Recursos Humanos
		protected string webRRHH { get; set; }
		//
		// Summary:
		//     El URL del WebService
		protected string webs { get; set; }

		//
		// Summary:
		//     Valor que indica si el usuario actual se encuentra excluido del sistema por alguna
		//     de las reglas enviadas
		public bool CumpleReglas { get; set; }

		//
		// Summary:
		//     Indica si el usuario conectado es administrador en la aplicacion
		public bool EsAdministrador { get; set; }

		//
		// Summary:
		//     Lista de objetos SeguridadMod que representa la seguridad para cada modulo para
		//     esta instancia.
		public List<SeguridadMod> Modulos { get { return _modulos; } }
		//
		// Summary:
		//     Indica si el usuario tiene o no acceso a la aplicacion segun la seguridad
		public bool UsuarioAccesa { get; set; }

		private List<Accesos> listaAccesos { get; set; }

		public bool TieneAcceso(int idAcceso)
		{
			updateListaAcceso();
			return listaAccesos.Where(p => p.idAcceso == idAcceso).Any();
		}

		public bool TieneAcceso(string Acceso)
		{
			updateListaAcceso();
			return listaAccesos.Where(p => p.Nombre == Acceso).Any();
		}

		private void updateListaAcceso() 
		{
			if (listaAccesos == null)
			{
				var _db = new ApplicationDbContext();

				listaAccesos = (from rol in _db.Roles
								from user in rol.Users
								from acc in rol.Accesos
								where user.UserId == _userName
								select acc).ToList();

				//var _lista = (from p in _db.Users from p2 in p.Roles where p.Id == _userName select p2.RoleId).ToList();
				//listaAccesos = (from p in _db.Accesos from p2 in p.Roles where _lista.Contains(p2.Id) select p).ToList();

			}
		}
		
		//
		// Summary:
		//     Metodo que busca un modulo por su numero de ID y lo retorna si lo encuentra,
		//     si no retorna el modulo vacio
		//
		// Parameters:
		//   id:
		//     Id del modulo a buscar
		//
		// Returns:
		//     El modulo encontrado por el metodo
		public SeguridadMod BuscarModulo(int idTipo)
		{
			var _mod = this.Modulos.FirstOrDefault(p => p.idModulo == idTipo);
			if (_mod != null)
				return _mod;

			var _db = new dbSolicitudesContext();
			var objTipo = _db.Tipos.Find(idTipo);
			if (objTipo == null)
			{
				return new SeguridadMod(idTipo, "Not Tipo En DB", "-----", true);
			}
			
			return getSeguridadDesdeObjTipo(objTipo);
		}

		public SeguridadMod BuscarModulo(string nombreTipo)
		{
			var _mod = this.Modulos.FirstOrDefault(p => p.nombre == nombreTipo);
			if (_mod != null)
				return _mod;

			var _db = new dbSolicitudesContext();
			var objTipo = _db.Tipos.FirstOrDefault(p=>p.Nombre == nombreTipo);
			if (objTipo == null) 
			{
				return new SeguridadMod(0, "Not Tipo En DB", "-----", true);
			}

			return getSeguridadDesdeObjTipo(objTipo);
			
		}

		//public SeguridadMod BuscarModuloByAcceso(int idAcceso)
		//{
		//	string permisos = "rweds";

		//	if (!TieneAcceso(idAcceso))
		//	{
		//		permisos = permisos.Replace("e", "-");
		//		permisos = permisos.Replace("d", "-");
		//		permisos = permisos.Replace("w", "-");
		//		permisos = permisos.Replace("r", "-");
		//	}

		//	return new SeguridadMod(idAcceso, "AccesoId: " + idAcceso.ToString(), permisos, true);

		//}

		//public SeguridadMod BuscarModuloByAcceso(string nombreAcceso)
		//{
		//	string permisos = "rweds";

		//	if (!TieneAcceso(nombreAcceso))
		//	{
		//		permisos = permisos.Replace("e", "-");
		//		permisos = permisos.Replace("d", "-");
		//		permisos = permisos.Replace("w", "-");
		//		permisos = permisos.Replace("r", "-");
		//	}

		//	return new SeguridadMod(0, "AccesoNombre: " + nombreAcceso, permisos, true);

		//}

		private SeguridadMod getSeguridadDesdeObjTipo(Tipo objTipo)
		{
			string permisos = "rweds";

			if (!TieneAcceso((objTipo.Leer != null ? (int)objTipo.Leer : -1)))
			{
				permisos = permisos.Replace("r", "-");
			}
			if (!TieneAcceso((objTipo.Escribir != null ? (int)objTipo.Escribir : -1)))
			{
				permisos = permisos.Replace("w", "-");
			}
			if (!TieneAcceso((objTipo.Editar != null ? (int)objTipo.Editar : -1)))
			{
				permisos = permisos.Replace("e", "-");
				permisos = permisos.Replace("s", "-");
			}
			if (!TieneAcceso((objTipo.Eliminar != null ? (int)objTipo.Eliminar : -1)))
			{
				permisos = permisos.Replace("d", "-");
			}
			return new SeguridadMod(objTipo.TipoID, objTipo.Nombre, permisos, true);
		}

	}
}