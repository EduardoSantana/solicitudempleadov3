﻿using SeguridadComun;
using SolicitudEmpleado.DAL;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{

    public abstract class BaseController : BaseSeguridadController
	{
        protected UnitOfWork unitOfWork { get; private set; }

        //protected WebServiceRH ws { get; set; }

        //protected WebServiceProd wsProd = new WS.WebServiceProd();
        
        protected Usuario currentUser
        {
            get
            {
                return ObtenerUsuario();
            }
        }

        public BaseController()
        {
            //ws = new WebServiceRH("Anonimo", "::");
            unitOfWork = new UnitOfWork();
        }

        public BaseController(UnitOfWork db)
        {
            //ws = new WebServiceRH(currentUser.usuario, currentUser.ip);
            unitOfWork = db;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }

        //public Oficina BuscarOficina(string codigo)
        //{
        //    string serviceUrl = ConfigurationManager.AppSettings["Seguridad.Url.Oficinas"];
        //    var service = new OficinasService(serviceUrl);

        //    var request = new OficinaRequest()
        //    {
        //        Info = new ServiceRequestInfo()
        //        {
        //            Aplicacion = ConfigurationManager.AppSettings["Seguridad.App.Nombre"],
        //            IP = Request.UserHostAddress,
        //            Usuario = User.Identity.Name
        //        },
        //        Codigo = codigo
        //    };

        //    var response = service.BuscarOficina(request);
        //    return response.Oficina;
        //}

        #region llenado de opciones comunes en varias paginas
        
        [HttpPost]
        public JsonResult GetEmpresaOptions()
        {
            try
            {
                var empresas = getListaEmpresa().Select(
                    c => new { DisplayText = c.Nombre, Value = c.EmpresaID });
                return Json(new { Result = "OK", Options = empresas });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public IEnumerable<Empresa> getListaEmpresa()
        {
            var obj = new List<Empresa>();
            var item = new Empresa();
            item.EmpresaID = 0;
            item.Nombre = "Seleccione la Empresa...";
            obj.Add(item);
            return obj.Union(unitOfWork.EmpresaRepository.Get().Where(name => name.Activo == true).OrderBy(p=>p.Nombre).ToList());
        }

        public IEnumerable<Estatu> getListaEstatus(params int[] listaEstados)
        {
            var obj = new List<Estatu>();
            var item = new Estatu();
            item.EstatusID = 0;
            item.Nombre = "Seleccione el Estado...";
            obj.Add(item);

            if (listaEstados.Length > 0)
            {
                obj.AddRange(unitOfWork.EstatusRepository.Get().Where(name => name.Activo == true && listaEstados.Contains(name.EstatusID)).OrderBy(p => p.Nombre).ToList());
            }
            else
            {
                obj.AddRange(unitOfWork.EstatusRepository.Get().Where(name => name.Activo == true).OrderBy(p => p.Nombre).ToList());
            }

            return obj;
        }

        public IEnumerable<Tipo> getListaTipo(params int[] listaEstados)
        {
            var obj = new List<Tipo>();
            var item = new Tipo();
            item.TipoID = 0;
            item.Nombre = "Seleccione el Tipo...";
            obj.Add(item);

            if (listaEstados.Length > 0)
            {
                obj.AddRange(unitOfWork.TiposRepository.Get().Where(name => name.Activo == true && listaEstados.Contains(name.TipoID)).OrderBy(p => p.Nombre).ToList());
            }
            else
            {
                obj.AddRange(unitOfWork.TiposRepository.Get().Where(name => name.Activo == true).OrderBy(p => p.Nombre).ToList());
            }

            return obj;
        }

        public IEnumerable<spGetAsignadaResult> getListaAsignado(int asignadoA = 0, int solicitudId = 0)
        {
			int idModulo = getModuloLeerDeLaSolicitud(solicitudId);
            var obj = new List<spGetAsignadaResult>();
            if (asignadoA != 0)
            {
                var usuario = BuscarUsuario(asignadoA);
                if (usuario != null)
                {
                    var item = new spGetAsignadaResult();
                    item.AsignadoA = usuario.codigoEmpleado;
                    item.AsignadoANombre = usuario.nombreCompleto;
                    obj.Add(item);
                }
                else
                {
                    obj.AddRange(unitOfWork.SolicitudesRepository.GetAsignados(idModulo, asignadoA).OrderBy(p => p.AsignadoANombre));
                }

            }
            else
            {
                var item = new spGetAsignadaResult();
                item.AsignadoA = 0;
                item.AsignadoANombre = "Seleccione el Usuario...";
                obj.Add(item);
                obj.AddRange(unitOfWork.SolicitudesRepository.GetAsignados(idModulo, asignadoA).OrderBy(p => p.AsignadoANombre));
            }
            
            return obj.ToList();
        }

		private int getModuloLeerDeLaSolicitud(int solicitudId)
		{
			int? retVal = -1;
			var _objSolicitud = unitOfWork.SolicitudesRepository.GetByID(solicitudId);
			if (_objSolicitud != null && _objSolicitud.Tipos != null && _objSolicitud.Tipos.Leer != null)
			{
				retVal = unitOfWork.SolicitudesRepository.GetByID(solicitudId).Tipos.Leer;
			}
			return Convert.ToInt32(retVal);
		}

        #endregion
    }
}