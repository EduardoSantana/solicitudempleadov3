﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class PropiedadTipoesController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: PropiedadTipoes
        public ActionResult Index()
        {
            return View(db.PropiedadTipoes.ToList());
        }

        // GET: PropiedadTipoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropiedadTipo propiedadTipo = db.PropiedadTipoes.Find(id);
            if (propiedadTipo == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(propiedadTipo);

            return View(propiedadTipo);
        }

        // GET: PropiedadTipoes/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: PropiedadTipoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoPropiedadID,Nombre,Columnas,Orden,Agregar")] PropiedadTipo propiedadTipo)
        {
            if (ModelState.IsValid)
            {
                propiedadTipo.AuditoriaCrear(currentUser.codigoEmpleado);
                db.PropiedadTipoes.Add(propiedadTipo);
               
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ModelState.AddModelError("", "No se ha podido guardar PropiedadTipo. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(propiedadTipo);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar PropiedadTipo. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(propiedadTipo);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(propiedadTipo);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(propiedadTipo);

            return View(propiedadTipo);
        }

        // GET: PropiedadTipoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropiedadTipo propiedadTipo = db.PropiedadTipoes.Find(id);
            if (propiedadTipo == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(propiedadTipo);

            return View(propiedadTipo);
        }

        // POST: PropiedadTipoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoPropiedadID,Nombre,Columnas,Orden,Agregar")] PropiedadTipo propiedadTipo)
        {
            if (ModelState.IsValid)
            {
                propiedadTipo.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(propiedadTipo).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(propiedadTipo);

            return View(propiedadTipo);
        }

        // GET: PropiedadTipoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropiedadTipo propiedadTipo = db.PropiedadTipoes.Find(id);
            if (propiedadTipo == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(propiedadTipo);

            return View(propiedadTipo);
        }

        // POST: PropiedadTipoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PropiedadTipo propiedadTipo = db.PropiedadTipoes.Find(id);
            db.PropiedadTipoes.Remove(propiedadTipo);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar PropiedadTipo. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(propiedadTipo);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
