﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class TiposSolicitudsController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: TiposSolicituds
        public ActionResult Index()
        {
            return View(db.TiposSolicituds.ToList());
        }

        // GET: TiposSolicituds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposSolicitud tiposSolicitud = db.TiposSolicituds.Find(id);
            if (tiposSolicitud == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(tiposSolicitud);

            return View(tiposSolicitud);
        }

        // GET: TiposSolicituds/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: TiposSolicituds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoSolicitudID,Nombre,Descripcion")] TiposSolicitud tiposSolicitud)
        {
            if (ModelState.IsValid)
            {
                tiposSolicitud.AuditoriaCrear(currentUser.codigoEmpleado);
                db.TiposSolicituds.Add(tiposSolicitud);
               
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ModelState.AddModelError("", "No se ha podido guardar TiposSolicitud. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(tiposSolicitud);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar TiposSolicitud. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(tiposSolicitud);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(tiposSolicitud);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(tiposSolicitud);

            return View(tiposSolicitud);
        }

        // GET: TiposSolicituds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposSolicitud tiposSolicitud = db.TiposSolicituds.Find(id);
            if (tiposSolicitud == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(tiposSolicitud);

            return View(tiposSolicitud);
        }

        // POST: TiposSolicituds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "TipoSolicitudID,Nombre,Descripcion,ViewIndex,ViewDetails,ViewEdit,ViewCreate")] TiposSolicitud tiposSolicitud)
        {
            if (ModelState.IsValid)
            {
                tiposSolicitud.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(tiposSolicitud).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(tiposSolicitud);

            return View(tiposSolicitud);
        }

        // GET: TiposSolicituds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposSolicitud tiposSolicitud = db.TiposSolicituds.Find(id);
            if (tiposSolicitud == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(tiposSolicitud);

            return View(tiposSolicitud);
        }

        // POST: TiposSolicituds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposSolicitud tiposSolicitud = db.TiposSolicituds.Find(id);
            db.TiposSolicituds.Remove(tiposSolicitud);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar TiposSolicitud. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(tiposSolicitud);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
