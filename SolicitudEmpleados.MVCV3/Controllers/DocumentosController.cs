﻿using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class DocumentosController : BaseController
    {
        public ActionResult Detalle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var solicitud = unitOfWork.SolicitudesRepository.GetByID(id);

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            //var idModulo = getModuloDeLaSolicitud(solicitud.SolicitudID);

            var modulo = BuscarModulo(solicitud.Tipos.TipoID);

            if (!modulo.permisoLeer)
            {
                return RedirectToAction("Procesar", "Solicitudes", new { id = id });
            }

            if (!modulo.permisoEditar)
            {
                return RedirectToAction("Procesar", "Solicitudes", new { id = id});
            }

            var modelo = new DetalleDocumentosViewModel();

            modelo.SolicitudId = (int)id;

            modelo.Documentos = unitOfWork.SolicitudesRepository.GetPropiedades((int)id, PropiedadesTipos.Documentos).ToList();

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Detalle(DetalleDocumentosViewModel modelo)
        {
            if (modelo == null || modelo.SolicitudId < 1)
            {
                return HttpNotFound();
            }

            var solicitud = unitOfWork.SolicitudesRepository.GetByID(modelo.SolicitudId);

            if (solicitud == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                var listaHistorial = new List<spGetPropiedadesDeSolicitudResult>();
                unitOfWork.SolicitudesRepository.GuardarPropiedades(modelo.Documentos, solicitud, currentUser.codigoEmpleado, currentUser.usuario, listaHistorial);
                unitOfWork.SolicitudesRepository.GuardarPropiedadesHistorial(listaHistorial, solicitud, currentUser.codigoEmpleado, currentUser.usuario, solicitud.EstatusID);
                try
                {
                    unitOfWork.Save();
                    TempData["MensajeError"] = "ok|Documento se ha guardado correctamente.";
                }
                catch (Exception ex)
                {
                    TempData["MensajeError"] = "error|" + ex.Message;
                }

                return RedirectToAction("Procesar", "Solicitudes", new { id = modelo.SolicitudId });
            }

            return RedirectToAction("Procesar", "Solicitudes", new { id = modelo.SolicitudId });
            
        }
    }
}