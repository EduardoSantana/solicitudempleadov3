﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class HistorialController : BaseController
    {
        // GET: Historial
        public ActionResult Detalle(int id)
        {
            var coleccion = unitOfWork.HistorialRepository.Get().Where(p => p.HistorialID == id).FirstOrDefault();

            if (Request.IsAjaxRequest())
                return PartialView(coleccion);

            return View(coleccion);
        }
    }
}