﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class PropiedadesController : BaseController
    {
        public List<SelectListItem> ListaDeTipos
        {
            get
            {
                List<SelectListItem> retVal = new List<SelectListItem>();

                var item1 = new SelectListItem()
                {
                    Value = "bool",
                    Text = "Booleano"
                };
                var item2 = new SelectListItem()
                {
                    Value = "currency",
                    Text = "Moneda"
                };
                var item3 = new SelectListItem()
                {
                    Value = "Integer",
                    Text = "Entero"
                };
                var item4 = new SelectListItem()
                {
                    Value = "multiline",
                    Text = "Multi Lineas"
                };
                var item5 = new SelectListItem()
                {
                    Value = "readonly",
                    Text = "Solo Lectura"
                };
                var item6 = new SelectListItem()
                {
                    Value = "varchar",
                    Text = "Alfanumerico"
                };

                retVal.Add(item1);
                retVal.Add(item2);
                retVal.Add(item3);
                retVal.Add(item4);
                retVal.Add(item5);
                retVal.Add(item6);

                return retVal;
            }
        }

        private dbSolicitudesContext db = new dbSolicitudesContext();

        public ActionResult ModificarTipo(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var modelo = new EditarPropiedadViewModel();
            var listaTipos = new List<int>();

			var seguridad = ObtenerSeguridad();

			foreach (var item in seguridad.Modulos)
			{
				if (item.permisoLeer)
				{
					listaTipos.Add(item.idModulo);
				}
			}

            modelo.PropiedadId = id;
            modelo.Nombre = db.Propiedades.Find(id).Nombre;
            modelo.Listado = unitOfWork.TiposRepository.Get()
                .Where(p => listaTipos.Contains(p.TipoID))
                .Select(p => new TiposPropiedadCustom { 
                    Tipos = p,
                    Habilitar = p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any(),
                    Existe = p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any(),
                    Requerido = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().Requerido : false),
                    Guardar = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().Guardar : false),
                    GuardarHistorial = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().GuardarHistorial : false),
                    EsUnico = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().EsUnico : false),
                    Orden = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().Orden : 0),
                    TipoID = p.TipoID,
                    TiposPropiedadesID = (p.TiposPropiedades.Where(pp => pp.PropiedadID == id).Any() ? p.TiposPropiedades.Where(pp => pp.PropiedadID == id).FirstOrDefault().TiposPropiedadesID : 0)
            }).ToList();

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarTipo(EditarPropiedadViewModel modelo)
        {
            if (modelo == null || modelo.Listado == null || modelo.PropiedadId == 0)
            {
                return HttpNotFound();
            }
            
            var tipoPropiedadsControler = new TiposPropiedadsController();
            
            foreach (var item in modelo.Listado)
            {
                var nuevo = new TiposPropiedad();
                nuevo.TiposPropiedadesID = item.TiposPropiedadesID;
                nuevo.PropiedadID = modelo.PropiedadId;
                nuevo.TipoID = item.TipoID;
                nuevo.Requerido = item.Requerido;
                nuevo.Guardar = item.Guardar;
                nuevo.GuardarHistorial = item.GuardarHistorial;
                nuevo.EsUnico = item.EsUnico;
                nuevo.Orden = item.Orden;

                if (item.Habilitar && item.Existe)
                {
                    tipoPropiedadsControler.Edit(nuevo);
                }
                if (item.Habilitar && item.Existe == false)
                {
                    tipoPropiedadsControler.Create(nuevo);
                }
                if (item.Habilitar == false && item.Existe)
                {
                    tipoPropiedadsControler.DeleteConfirmed(item.TiposPropiedadesID);
                }
            }

            return RedirectToAction("Index", new { id1 = modelo.PropiedadId });

        }

        // GET: Propiedades
        public ActionResult Index(int id = 0, int id1 = 0)
        {
            ViewBag.ListaTipos = new SelectList(db.PropiedadTipoes.OrderBy(p=>p.Nombre), "TipoPropiedadID", "Nombre");
            var modelo = new SolicitudEmpleado.Models.ListadoPropiedadesViewModel();
            modelo.Propiedades = db.Propiedades.Include(p => p.PropiedadTipo);

            if (id > 0)
            {
                modelo.Propiedades = modelo.Propiedades.Where(p => p.TipoPropiedadID == id); ;
                modelo.TipoPropiedadID = id;
                modelo.Nombre = db.PropiedadTipoes.Find(id).Nombre;
                return View(modelo);
            }
            if (id1 > 0)
            {
                id = db.Propiedades.Find(id1).TipoPropiedadID;
                modelo.Propiedades = modelo.Propiedades.Where(p => p.TipoPropiedadID == id); ;
                modelo.TipoPropiedadID = id;
                modelo.Nombre = db.PropiedadTipoes.Find(id).Nombre;
                return View(modelo);
            }

            return View(modelo);
        }

        // GET: Propiedades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propiedad propiedad = db.Propiedades.Find(id);
            if (propiedad == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(propiedad);

            return View(propiedad);
        }

        // GET: Propiedades/Create
        public ActionResult Create(int id = 0, int solicitudTipoId = 0)
        {
            ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes, "TipoPropiedadID", "Nombre", id);
            ViewBag.ListaDeTipos = ListaDeTipos;
            TempData["solicitudTipoId"] = solicitudTipoId;
       
            if (Request.IsAjaxRequest())
                return PartialView();
          
            return View();
        }

        // POST: Propiedades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create([Bind(Include = "PropiedadID,Nombre,Descripcion,Tipo,RegularExpression,PlaceHolder,PorDefecto,TipoPropiedadID,Opciones,ValidacionJS")] Propiedad propiedad)
        {
            if (ModelState.IsValid)
            {
                if (!RegularExpresion.Validar(propiedad.RegularExpression))
                {
                    ModelState.AddModelError("", "No se ha podido Guardar Propiedad. Error patron invalido para la expresion regular. ");
                    ModelState.AddModelError("RegularExpression", " Patron invalido.");
                    
                    ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);
                    ViewBag.ListaDeTipos = ListaDeTipos;
                    
                    return View(propiedad);
                }
                propiedad.AuditoriaCrear(currentUser.codigoEmpleado);
                db.Propiedades.Add(propiedad);
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p=>p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);
                    ViewBag.ListaDeTipos = ListaDeTipos;

                    ModelState.AddModelError("", "No se ha podido Guardar Propiedad. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(propiedad);
                }
                catch (Exception ex)
                {
                    ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);
                    ViewBag.ListaDeTipos = ListaDeTipos;

                    ModelState.AddModelError("", "No se ha podido Guardar Propiedad. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);

                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(propiedad);
                }

                if (TempData["solicitudTipoId"] != null && TempData["solicitudTipoId"].ToString() != "0")
                {
                    TempData["MensajeError"] = "ok|Se Ha Creado Correctamente.";
                    return RedirectToAction("Index", "TiposPropiedads", new { id1 = TempData["solicitudTipoId"], id2 = propiedad.TipoPropiedadID });
                }
                else
                {
                    TempData["MensajeError"] = "ok|Se Ha Creado Correctamente.";
                    return RedirectToAction("Index", new { id = propiedad.TipoPropiedadID });
                }
            }

            ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);
            ViewBag.ListaDeTipos = ListaDeTipos;
            return View(propiedad);
        }

        // GET: Propiedades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propiedad modelo = db.Propiedades.Find(id);
            if (modelo == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", modelo.TipoPropiedadID);
            ViewBag.ListaDeTipos = ListaDeTipos;

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }

        // POST: Propiedades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "PropiedadID,Nombre,Descripcion,Tipo,RegularExpression,PlaceHolder,PorDefecto,TipoPropiedadID,CreadoPor,FechaCreacion,Opciones,ValidacionJS")] Propiedad propiedad)
        {
            if (ModelState.IsValid)
            {
                if (!RegularExpresion.Validar(propiedad.RegularExpression))
                {
                    ModelState.AddModelError("", "No se ha podido Guardar Propiedad. Error patron invalido para la expresion regular. ");
                    ModelState.AddModelError("RegularExpression", " Patron invalido.");

                    ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);
                    ViewBag.ListaDeTipos = ListaDeTipos;

                    return View(propiedad);
                }

                propiedad.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(propiedad).State = EntityState.Modified;
                db.SaveChanges();

                TempData["MensajeError"] = "ok|Se Ha Actualizado Correctamente.";
                return RedirectToAction("Index", new { id = propiedad.TipoPropiedadID });
            }
            ViewBag.TipoPropiedadID = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre", propiedad.TipoPropiedadID);

            if (Request.IsAjaxRequest())
                return PartialView(propiedad);

            return View(propiedad);
        }

        // GET: Propiedades/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propiedad propiedad = db.Propiedades.Find(id);
            if (propiedad == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(propiedad);

            return View(propiedad);
        }

        // POST: Propiedades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Propiedad propiedad = db.Propiedades.Find(id);
            db.Propiedades.Remove(propiedad);
            db.SaveChanges();
            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}
