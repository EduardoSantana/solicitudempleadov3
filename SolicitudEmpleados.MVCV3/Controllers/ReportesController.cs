﻿using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class ReportesController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        const string formatoFecha = "yyyy-MM-dd";

        // GET: Reportes
        public ActionResult Index()
        {
            var modelo = new CustomReportesViewModel();
            modelo.FechaInicial = DateTime.Now.AddDays(-7);
            modelo.FechaFinal = DateTime.Now;
            modelo.ListaAsignadoA = new SelectList(getListaAsignado(), "AsignadoA", "AsignadoANombre");
            ViewBag.ListaEstatus = new SelectList(getListaEstatus(), "EstatusID", "Nombre");
            ViewBag.ListaTipos = new SelectList(getListaTipo(), "TipoID", "Nombre");
            modelo.FechaInicial = DateTime.Now.AddDays(-7);
            modelo.FechaFinal = DateTime.Now;
            return View(modelo);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CustomReportesViewModel modelo)
        {
            ViewBag.ListaEstatus = new SelectList(db.Estatus, "EstatusID", "Nombre");
            ViewBag.ListaTipos = new SelectList(db.Tipos, "TipoID", "Nombre");
            //if (ModelState.IsValid)
            //{
            //    string rptRuta = ConfigurationManager.AppSettings["RutaReporte"];
            //    var service = new ReportExecutionService();
            //    service.Credentials = CredentialCache.DefaultCredentials;
            //    service.Timeout = 60 * 60 * 1000;
            //    string historyID = null;
            //    service.LoadReport(rptRuta, historyID);

            //    ParameterValue[] mParamArray = new ParameterValue[5];

            //    ParameterValue Param0 = new ParameterValue();
            //    Param0.Name = "FechaInicial";
            //    Param0.Value = modelo.FechaInicial.ToString(formatoFecha);
            //    mParamArray[0] = Param0;

            //    ParameterValue Param1 = new ParameterValue();
            //    Param1.Name = "FechaFinal";
            //    Param1.Value = modelo.FechaFinal.ToString(formatoFecha);
            //    mParamArray[1] = Param1;

            //    ParameterValue Param2 = new ParameterValue();
            //    Param2.Name = "EstatusID";
            //    Param2.Value = modelo.EstatusID.ToString();
            //    mParamArray[2] = Param2;

            //    ParameterValue Param3 = new ParameterValue();
            //    Param3.Name = "TipoID";
            //    Param3.Value = modelo.TipoID.ToString();
            //    mParamArray[3] = Param3;

            //    ParameterValue Param4 = new ParameterValue();
            //    Param4.Name = "AsignadoA";
            //    Param4.Value = modelo.AsignadoA.ToString();
            //    mParamArray[4] = Param4;

            //    service.SetExecutionParameters(mParamArray, "en-us");

            //    string extension = null;
            //    string mimeType = null;
            //    string encoding = null;
            //    Warning[] warnings = null;
            //    string[] streams = null;
            //    string formatoRetornoFile = "application/pdf";
            //    string extencionFile = ".pdf";
            //    if (modelo.Formato != "PDF")
            //    {
            //        formatoRetornoFile = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //        extencionFile = ".xls";
            //    }

            //    var data = service.Render(modelo.Formato, null, out extension, out mimeType, out encoding, out warnings, out streams);
            //    return File(data, formatoRetornoFile, "ReporteSolicitudes" + extencionFile);
            //}
            return View(modelo);
        }
    }
}
