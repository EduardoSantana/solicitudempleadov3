﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	public class ValidarAccesoAttribute : ActionFilterAttribute, IAuthenticationFilter
	{
		private string _acceso { get; set; }

		public ValidarAccesoAttribute(string Acceso)
		{
			_acceso = Acceso;
		}
		public void OnAuthentication(AuthenticationContext filterContext) { }

		public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
		{
			var user = filterContext.HttpContext.User;
			if (user == null || !user.Identity.IsAuthenticated)
			{
				filterContext.Result = new HttpUnauthorizedResult();
			}
			if (user != null && user.Identity.IsAuthenticated)
			{
				var seguridad = new SeguridadAppV2(user.Identity.GetUserId());
				if (!seguridad.TieneAcceso(this._acceso))
				{
					filterContext.Result = new HttpUnauthorizedResult();
				}
			}
		}
	}
}