﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.MVCV3.Componentes.Email;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	public class InvitacionsController : BaseController
	{
		private dbSolicitudesContext db = new dbSolicitudesContext();

		// GET: Invitacions
		public ActionResult Index()
		{
			var invitaciones = db.Invitaciones.Include(i => i.TiposSolicitud);
			return View(invitaciones.ToList());
		}

		// GET: Invitacions/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Invitacion invitacion = db.Invitaciones.Find(id);
			if (invitacion.Token != null)
			{
				invitacion.Token = Url.Action("Nueva", "Solicitudes", new { id = invitacion.Token }, this.Request.Url.Scheme);
			}
			if (invitacion == null)
			{
				return HttpNotFound();
			}

			if (Request.IsAjaxRequest())
				return PartialView(invitacion);

			return View(invitacion);
		}

		// GET: Invitacions/Create
		public ActionResult Create()
		{
			ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre");
			ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal");
			if (Request.IsAjaxRequest())
				return PartialView();

			return View();
		}

		// POST: Invitacions/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "InvitacionId,TipoSolicitudID,Nombre,Apellido,Correo,Cedula,Enviar,Nota,TipoID")] Invitacion invitacion)
		{
			if (ModelState.IsValid)
			{
				var objTipo = db.Tipos.Find(invitacion.TipoID);
				invitacion.TipoSolicitudID = (int)objTipo.TipoSolicitudID;
				var enviarCorreo = false;
				if (invitacion.Enviar)
				{
					invitacion.Token = Guid.NewGuid().ToString();
					invitacion.Enviar = false;
					enviarCorreo = true;
				}
				invitacion.AuditoriaCrear(currentUser.codigoEmpleado);
				db.Invitaciones.Add(invitacion);
				
				try
				{
					db.SaveChanges();

					if (enviarCorreo)
						NotificacionCorreo.NotificacionInvitacion(invitacion.InvitacionId, ObtenerUsuario());

				}
				catch (System.Data.Entity.Validation.DbEntityValidationException ex)
				{

					ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", invitacion.TipoSolicitudID);
					ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal");
					ModelState.AddModelError("", "No se ha podido guardar Invitacion. Por los siguientes errores: ");
					var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
					foreach (var item in listaValidaciones)
					{
						foreach (var itemInside in item.ValidationErrors)
						{
							ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
						}
					}

					return View(invitacion);
				}
				catch (Exception ex)
				{
					ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", invitacion.TipoSolicitudID);
					ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal");
					ModelState.AddModelError("", "No se ha podido guardar Invitacion. Por los siguientes errores: ");
					ModelState.AddModelError("", ex.Message);
					if (ex.InnerException != null)
					{
						string mensajeErrorInner = ex.InnerException.ToString();
						if (mensajeErrorInner.Length > 500)
						{
							mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
						}
						ModelState.AddModelError("", mensajeErrorInner);
					}

					return View(invitacion);
				}

				if (Request.IsAjaxRequest())
					return PartialView(invitacion);

				return RedirectToAction("Index");
			}

			ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", invitacion.TipoSolicitudID);
			ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal");
			if (Request.IsAjaxRequest())
				return PartialView(invitacion);

			return View(invitacion);
		}

		// GET: Invitacions/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Invitacion invitacion = db.Invitaciones.Find(id);
			if (invitacion == null)
			{
				return HttpNotFound();
			}
			ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", invitacion.TipoSolicitudID);
			ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal", invitacion.TipoID);
			if (Request.IsAjaxRequest())
				return PartialView(invitacion);

			return View(invitacion);
		}

		// POST: Invitacions/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "InvitacionId,TipoSolicitudID,Nombre,Apellido,Correo,Cedula,Enviar,Nota,TipoID,CreadoPor,FechaCreacion")] Invitacion invitacion)
		{
			if (ModelState.IsValid)
			{
				var objTipo = db.Tipos.Find(invitacion.TipoID);
				invitacion.TipoSolicitudID = (int)objTipo.TipoSolicitudID;
				var enviarCorreo = false;
				if (invitacion.Enviar)
				{
					enviarCorreo = true;
					invitacion.Token = Guid.NewGuid().ToString();
					invitacion.Enviar = false;
				}
				invitacion.AuditoriaEditar(currentUser.codigoEmpleado);
				db.Entry(invitacion).State = EntityState.Modified;
				db.SaveChanges();

				if (enviarCorreo)
					NotificacionCorreo.NotificacionInvitacion(invitacion.InvitacionId, ObtenerUsuario());

				if (Request.IsAjaxRequest())
					return PartialView();
				return RedirectToAction("Index");
			}
			ViewBag.TipoSolicitudID = new SelectList(db.TiposSolicituds, "TipoSolicitudID", "Nombre", invitacion.TipoSolicitudID);
			ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "NombreCal");
			if (Request.IsAjaxRequest())
				return PartialView(invitacion);

			return View(invitacion);
		}

		// GET: Invitacions/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Invitacion invitacion = db.Invitaciones.Find(id);
			if (invitacion == null)
			{
				return HttpNotFound();
			}
			if (Request.IsAjaxRequest())
				return PartialView(invitacion);

			return View(invitacion);
		}

		// POST: Invitacions/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			Invitacion invitacion = db.Invitaciones.Find(id);
			db.Invitaciones.Remove(invitacion);

			try
			{
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Eliminar Invitacion. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

				return View(invitacion);
			}

			if (Request.IsAjaxRequest())
				return PartialView();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

	}
}
