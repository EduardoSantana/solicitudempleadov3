﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/Generico

        public ActionResult General()
        {
            return View();
        }

        //
        // GET: /Error/Codigo

        public ActionResult Codigo(int id)
        {
            switch (id)
            {
                case 401:
                    ViewBag.Titulo = "Módulo no Autorizado";
                    ViewBag.Mensaje = "No tiene permisos para visualizar el módulo al que intentó acceder.";
                    break;
                case 403:
                    ViewBag.Titulo = "Acceso no Autorizado";
                    ViewBag.Mensaje = "No tiene permisos para acceder a esta aplicación.";
                    break;
                case 404:
                    ViewBag.Titulo = "Recurso no Disponible";
                    ViewBag.Mensaje = "El recurso al que intentó acceder no se encuentra disponible o no fue encontrado.";
                    break;
                default:
                    return RedirectToAction("General");
            }

            return View();
        }
    }
}