﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleados.MVCV3.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	[ValidarAcceso("AccesoAccesos")]
    public class AccesosController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Accesos
        public ActionResult Index()
        {
            return View(db.Accesos.ToList());
        }

        // GET: Accesos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var clAccesos = db.Accesos.Find(id);
            if (clAccesos == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(clAccesos);

            return View(clAccesos);
        }

        // GET: Accesos/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Accesos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idAcceso,Nombre,Descripcion")] Accesos clAccesos)
        {
            if (ModelState.IsValid)
            {
                db.Accesos.Add(clAccesos);
               
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ModelState.AddModelError("", "No se ha podido guardar clAccesos. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(clAccesos);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar clAccesos. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(clAccesos);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(clAccesos);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(clAccesos);

            return View(clAccesos);
        }

        // GET: Accesos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accesos clAccesos = db.Accesos.Find(id);
            if (clAccesos == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(clAccesos);

            return View(clAccesos);
        }

        // POST: Accesos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idAcceso,Nombre,Descripcion")] Accesos clAccesos)
        {
            if (ModelState.IsValid)
            {

                db.Entry(clAccesos).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(clAccesos);

            return View(clAccesos);
        }

        // GET: Accesos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accesos clAccesos = db.Accesos.Find(id);
            if (clAccesos == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(clAccesos);

            return View(clAccesos);
        }

        // POST: Accesos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Accesos clAccesos = db.Accesos.Find(id);
            db.Accesos.Remove(clAccesos);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar clAccesos. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(clAccesos);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
