﻿using Microsoft.AspNet.Identity;
using SeguridadComun;
using SolicitudEmpleados.MVCV3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	[Authorize]
	public class BaseSeguridadController : Controller
	{
		private SeguridadAppV2 _seguridadApp { get; set; }
		private SeguridadComun.Usuario _CurrentUser { get; set; }
		private SeguridadComun.Usuario convertirUsuario(ApplicationUser usuario)
		{
			var retVal = new SeguridadComun.Usuario()
			{
				idUsuario = usuario.idUsuario,
				activo = true,
				apellido = usuario.apellido,
				cedula = 0,
				cedulaForm = usuario.cedulaForm,
				celular = 0,
				celularForm = usuario.celularForm,
				codigoEmpleado = (usuario.codigoEmpleado != null) ? (int)usuario.codigoEmpleado : 0,
				codOficina = usuario.codOficina,
				correo = usuario.correo,
				departamento = usuario.departamento,
				empresa = ((usuario.empresa != null) ? (int)usuario.empresa : 1),
				ext = ((usuario.ext != null) ? (int)usuario.ext : 0),
				ip = usuario.ip,
				navegador = usuario.navegador,
				nombre = usuario.nombre,
				nomOficina = usuario.nomOficina,
				puesto = usuario.puesto,
				telefono = 0,
				telefonoForm = usuario.telefonoForm,
				usuario = usuario.UserName,

			};
			return retVal;
		}

		public bool TieneAcceso(int? idAcceso)
		{
			if (idAcceso == null)
				idAcceso = -1;
			return ObtenerSeguridad().TieneAcceso((int)idAcceso);
		}

		public bool TieneAcceso(string Acceso)
		{
			return ObtenerSeguridad().TieneAcceso(Acceso);
		}

		public SeguridadMod BuscarModulo(int idTipo)
		{
			return ObtenerSeguridad().BuscarModulo(idTipo);
		}

		public SeguridadMod BuscarModulo(string nombreTipo)
		{
			return ObtenerSeguridad().BuscarModulo(nombreTipo);
		}

		public Usuario BuscarUsuario(string userName)
		{
			var db = new ApplicationDbContext();
			ApplicationUser usuario = db.Users.Where(p => p.UserName == userName).FirstOrDefault();
			if (usuario != null)
			{
				return convertirUsuario(usuario);
			}

			return new Usuario();
		}

		public Usuario BuscarUsuario(int idUsuario)
		{
			var db = new ApplicationDbContext();
			ApplicationUser usuario = db.Users.Where(p => p.idUsuario == idUsuario).FirstOrDefault();
			if (usuario != null)
			{
				return convertirUsuario(usuario);
			}

			return new Usuario();
		}

		public Usuario ObtenerUsuario()
		{
			if (_CurrentUser == null)
			{
				string userName = User.Identity.Name;
				string userId = User.Identity.GetUserId();
				var db = new ApplicationDbContext();
				var tempUser = db.Users.Where(p => p.Id == userId).FirstOrDefault();
				if (tempUser != null)
				{
					_CurrentUser = convertirUsuario(tempUser);
				}
				else
				{
					_CurrentUser = new Usuario(userName);
				}
			}

			return _CurrentUser;

		}

		public SeguridadAppV2 ObtenerSeguridad()
		{
			if (_seguridadApp == null)
			{
				_seguridadApp = new SeguridadAppV2(User.Identity.GetUserId(), ObtenerUsuario());
			}
			return _seguridadApp;
		}
	}

}