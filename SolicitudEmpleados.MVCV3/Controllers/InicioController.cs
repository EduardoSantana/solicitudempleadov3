﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeguridadComun;
using System.Security.Principal;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	[Authorize]
    public class InicioController : BaseSeguridadController
    {
        
        //[ValidarModulo("SolicitudPrestamo")]
        public ActionResult Index()
        {
			var asdf = Request.IsAuthenticated;
			var asdfasdf = BuscarUsuario(User.Identity.Name);
            return View();
        }

        //
        // GET: /Inicio/Acerca

        public ActionResult Acerca()
        {
            return View();
        }
        
    }
}
