﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class TransaccionesDisponiblidadsController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: TransaccionesDisponiblidads
        public ActionResult Index(int id = 0)
        {

            ViewBag.ListaReferencia = new SelectList(db.ReferenciasEikons, "ReferenciaEikonId", "Nombre");
            var modelo = new ListadoTransaccionesViewModel();
            modelo.Listado = db.TransaccionesDisponiblidades.Include(t => t.Propiedad).Include(t => t.ReferenciaEikon);

            if (id > 0)
            {
                modelo.Listado = modelo.Listado.Where(p => p.ReferenciaEikonId == id); ;
                modelo.ReferenciEikonId = id;
                return View(modelo);
            }

            return View(modelo);

        }

        // GET: TransaccionesDisponiblidads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransaccionesDisponiblidad transaccionesDisponiblidad = db.TransaccionesDisponiblidades.Find(id);
            if (transaccionesDisponiblidad == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(transaccionesDisponiblidad);

            return View(transaccionesDisponiblidad);
        }

        // GET: TransaccionesDisponiblidads/Create
        public ActionResult Create()
        {
            ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones || p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).OrderBy(p => p.Nombre), "PropiedadID", "Nombre");
            ViewBag.ReferenciaEikonId = new SelectList(db.ReferenciasEikons, "ReferenciaEikonId", "Nombre");
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: TransaccionesDisponiblidads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TransaccionId,ReferenciaEikonId,Codigo,PropiedadID,Nombre")] TransaccionesDisponiblidad transaccionesDisponiblidad)
        {
            if (ModelState.IsValid)
            {
                transaccionesDisponiblidad.AuditoriaCrear(currentUser.codigoEmpleado);
                db.TransaccionesDisponiblidades.Add(transaccionesDisponiblidad);
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView(transaccionesDisponiblidad);

                return RedirectToAction("Index");
            }

            ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones || p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).OrderBy(p => p.Nombre), "PropiedadID", "Nombre", transaccionesDisponiblidad.PropiedadID);
            ViewBag.ReferenciaEikonId = new SelectList(db.ReferenciasEikons, "ReferenciaEikonId", "Nombre", transaccionesDisponiblidad.ReferenciaEikonId);
            if (Request.IsAjaxRequest())
                return PartialView(transaccionesDisponiblidad);

            return View(transaccionesDisponiblidad);
        }

        // GET: TransaccionesDisponiblidads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransaccionesDisponiblidad transaccionesDisponiblidad = db.TransaccionesDisponiblidades.Find(id);
            if (transaccionesDisponiblidad == null)
            {
                return HttpNotFound();
            }
            ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones || p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).OrderBy(p => p.Nombre), "PropiedadID", "Nombre", transaccionesDisponiblidad.PropiedadID);
            ViewBag.ReferenciaEikonId = new SelectList(db.ReferenciasEikons, "ReferenciaEikonId", "Nombre", transaccionesDisponiblidad.ReferenciaEikonId);
            if (Request.IsAjaxRequest())
                return PartialView(transaccionesDisponiblidad);

            return View(transaccionesDisponiblidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TransaccionId,ReferenciaEikonId,Codigo,PropiedadID,Nombre")] TransaccionesDisponiblidad transaccionesDisponiblidad)
        {
            if (ModelState.IsValid)
            {
                transaccionesDisponiblidad.AuditoriaEditar(currentUser.codigoEmpleado);
                db.Entry(transaccionesDisponiblidad).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p => p.TipoPropiedadID == PropiedadesTipos.EditarDeducciones || p.TipoPropiedadID == PropiedadesTipos.EditarIngresos).OrderBy(p => p.Nombre), "PropiedadID", "Nombre", transaccionesDisponiblidad.PropiedadID);
            ViewBag.ReferenciaEikonId = new SelectList(db.ReferenciasEikons, "ReferenciaEikonId", "Nombre", transaccionesDisponiblidad.ReferenciaEikonId);
            if (Request.IsAjaxRequest())
                    return PartialView(transaccionesDisponiblidad);

            return View(transaccionesDisponiblidad);
        }

        // GET: TransaccionesDisponiblidads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransaccionesDisponiblidad transaccionesDisponiblidad = db.TransaccionesDisponiblidades.Find(id);
            if (transaccionesDisponiblidad == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(transaccionesDisponiblidad);

            return View(transaccionesDisponiblidad);
        }

        // POST: TransaccionesDisponiblidads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TransaccionesDisponiblidad transaccionesDisponiblidad = db.TransaccionesDisponiblidades.Find(id);
            db.TransaccionesDisponiblidades.Remove(transaccionesDisponiblidad);
            db.SaveChanges();
            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
