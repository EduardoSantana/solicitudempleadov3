﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleados.MVCV3.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using SolicitudEmpleado.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SolicitudEmpleados.MVCV3.Controllers
{
	[ValidarAcceso("AccesoRoles")]
	public class RolesController : BaseController
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		// GET: Roles
		public ActionResult Index()
		{
			return View(db.Roles.ToList());
		}

		// GET: Roles/Details/5
		public ActionResult Details(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var roles = db.Roles.Find(id);
			if (roles == null)
			{
				return HttpNotFound();
			}

			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(roles);
		}

		// GET: Roles/Accesos/5
		public ActionResult Accesos(string id)
		{
			var modelo = new RolesAccesosViewModel();
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var roles = db.Roles.Find(id);
			if (roles == null)
			{
				return HttpNotFound();
			}

			modelo.Id = id;
			modelo.Name = roles.Name;
			modelo.Listado = db.Accesos.Where(p => p.Roles.Any(p2 => p2.Id == id)).Select(p3 => new RolesAccesosListadoViewModel { idAcceso = p3.idAcceso, Descripcion = p3.Descripcion, Nombre = p3.Nombre }).ToList();

			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(modelo);
		}

		public ActionResult AgregarAcceso(string id)
		{
			var modelo = new RolesAccesosListadoViewModel();

			modelo.RoleId = id;

			ViewBag.idAcceso = new SelectList(db.Accesos.Where(p=>!p.Roles.Any(p2=>p2.Id == id)).ToList(), "idAcceso", "Descripcion");

			if (Request.IsAjaxRequest())
				return PartialView(modelo);

			return View(modelo);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AgregarAcceso(RolesAccesosListadoViewModel modelo)
		{
			ViewBag.idAcceso = new SelectList(db.Accesos.Where(p => !p.Roles.Any(p2 => p2.Id == modelo.RoleId)).ToList(), "idAcceso", "Descripcion", modelo.idAcceso);

			try
			{

				var objRol = db.Roles.Find(modelo.RoleId);
				var objAcceso = db.Accesos.Find(modelo.idAcceso);
				objRol.Accesos.Add(objAcceso);
				db.SaveChanges();
				
				//var roleManager = HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

				//var roleManager = HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
				//if (roleManager != null)
				//{
				//	var objRol = roleManager.Roles.Where(p=> p.Id == modelo.RoleId).FirstOrDefault();
				//	var asdfasdf = db.Roles.FirstOrDefault().Accesos;
				//	var objAcceso = db.Accesos.Find(modelo.idAcceso);
				//	objRol.Accesos.Add(objAcceso);
				//	db.SaveChanges();
				//}

			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}

				return View(modelo);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

				return View(modelo);
			}

			return RedirectToAction("Accesos", new { id = modelo.RoleId });

		}

		public ActionResult EliminarAcceso(string id, int idAcceso)
		{

			try
			{
				var objRol = db.Roles.Find(id);
				foreach (var item in objRol.Accesos.ToList())
				{
					if (item.idAcceso == idAcceso)
					{
						objRol.Accesos.Remove(item);
					}
				}
				db.SaveChanges();
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}

			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

			}

			return RedirectToAction("Accesos", new { id = id });

		}

		// GET: Roles/Usuarios/5
		[ValidarAcceso("AccesoUsuarios")]
		public ActionResult Usuarios(string id)
		{
			var modelo = new RolesUsuariosViewModel();
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var roles = db.Roles.Find(id);
			if (roles == null)
			{
				return HttpNotFound();
			}

			modelo.Id = id;
			modelo.Name = roles.Name;
			modelo.Listado = db.Users.Where(p => p.Roles.Any(p2 => p2.RoleId == id)).Select(p3 => new RolesListViewModel {
				IdUsuario = p3.idUsuario,
				nombre = p3.nombre,
				apellido = p3.apellido,
				codigoEmpleado = p3.codigoEmpleado,
				UserId = p3.Id,
				UserName = p3.UserName
			}).ToList();

			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(modelo);
		}
		[ValidarAcceso("AccesoUsuarios")]
		public ActionResult AgregarUsuario(string id)
		{
			var modelo = new RolesListViewModel();

			modelo.RolId = id;

			ViewBag.UserId = new SelectList(db.Users.Where(p=>!p.Roles.Any(p2=>p2.RoleId == id)).ToList(), "Id", "UserName");

			if (Request.IsAjaxRequest())
				return PartialView(modelo);

			return View(modelo);

		}

		[HttpPost]
		[ValidarAcceso("AccesoUsuarios")]
		[ValidateAntiForgeryToken]
		public ActionResult AgregarUsuario(RolesListViewModel modelo)
		{
			ViewBag.UserId = new SelectList(db.Users.Where(p => !p.Roles.Any(p2 => p2.RoleId == modelo.RolId)).ToList(), "Id", "UserName", modelo.UserId);

			try
			{
				
				var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
				if (UserManager != null)
				{
					var objRol = db.Roles.Find(modelo.RolId);
					UserManager.AddToRole(modelo.UserId, objRol.Name);
				}
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}

				return View(modelo);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

				return View(modelo);
			}

			return RedirectToAction("Usuarios", new { id = modelo.RolId });

		}
		[ValidarAcceso("AccesoUsuarios")]
		public ActionResult EliminarUsuario(string id, string userId)
		{

			try
			{
				var objRol = db.Roles.Find(id);
				foreach (var item in objRol.Users.ToList())
				{
					if (item.UserId == userId)
					{
						objRol.Users.Remove(item);
					}
				}
				db.SaveChanges();
			}
			catch (System.Data.Entity.Validation.DbEntityValidationException ex)
			{

				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
				foreach (var item in listaValidaciones)
				{
					foreach (var itemInside in item.ValidationErrors)
					{
						ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
					}
				}

			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Agregar. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

			}

			return RedirectToAction("Usuarios", new { id = id });

		}

		// GET: Roles/Create
		public ActionResult Create()
		{
			if (Request.IsAjaxRequest())
				return PartialView();

			return View();
		}

		// POST: Roles/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,Name")] IdentityRole roles)
		{
			if (ModelState.IsValid)
			{

				db.Roles.AddOrUpdate(r => r.Name, new ApplicationRole { Name = roles.Name , Description = roles.Name  });

				try
				{
					db.SaveChanges();
				}
				catch (System.Data.Entity.Validation.DbEntityValidationException ex)
				{

					ModelState.AddModelError("", "No se ha podido guardar Roles. Por los siguientes errores: ");
					var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
					foreach (var item in listaValidaciones)
					{
						foreach (var itemInside in item.ValidationErrors)
						{
							ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
						}
					}

					return View(roles);
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", "No se ha podido guardar Roles. Por los siguientes errores: ");
					ModelState.AddModelError("", ex.Message);
					if (ex.InnerException != null)
					{
						string mensajeErrorInner = ex.InnerException.ToString();
						if (mensajeErrorInner.Length > 500)
						{
							mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
						}
						ModelState.AddModelError("", mensajeErrorInner);
					}

					return View(roles);
				}

				if (Request.IsAjaxRequest())
					return PartialView(roles);

				return RedirectToAction("Index");
			}

			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(roles);
		}

		// GET: Roles/Edit/5
		public ActionResult Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var roles = db.Roles.Find(id);
			if (roles == null)
			{
				return HttpNotFound();
			}
			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(roles);
		}

		// POST: Roles/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Name")] IdentityRole roles)
		{
			if (ModelState.IsValid)
			{
				db.Entry(roles).State = EntityState.Modified;
				db.SaveChanges();
				if (Request.IsAjaxRequest())
					return PartialView();
				return RedirectToAction("Index");
			}
			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(roles);
		}

		// GET: Roles/Delete/5
		public ActionResult Delete(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var roles = db.Roles.Find(id);
			if (roles == null)
			{
				return HttpNotFound();
			}
			if (Request.IsAjaxRequest())
				return PartialView(roles);

			return View(roles);
		}

		// POST: Roles/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(string id)
		{
			var roles = db.Roles.Find(id);
			db.Roles.Remove(roles);

			try
			{
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", "No se ha podido Eliminar Roles. Por los siguientes errores: ");
				ModelState.AddModelError("", ex.Message);
				if (ex.InnerException != null)
				{
					string mensajeErrorInner = ex.InnerException.ToString();
					if (mensajeErrorInner.Length > 500)
					{
						mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
					}
					ModelState.AddModelError("", mensajeErrorInner);
				}

				return View(roles);
			}


			if (Request.IsAjaxRequest())
				return PartialView();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
