﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Helpers;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleados.MVCV3.Controllers
{
    public class TiposPropiedadsController : BaseController
    {
        private dbSolicitudesContext db = new dbSolicitudesContext();

        // GET: TiposPropiedads
        public ActionResult Index(int id = 0, int id1 = 0, int id2 = 0)
        {
            ViewBag.ListaPropiedades = new SelectList(db.Propiedades.OrderBy(p=>p.Nombre), "PropiedadID", "Nombre");
            ViewBag.ListaTipos = new SelectList(db.Tipos.Where(p => p.Activo).OrderBy(p => p.Nombre), "TipoID", "Nombre");
            ViewBag.ListaPropiedadTipos = new SelectList(db.PropiedadTipoes.OrderBy(p => p.Nombre), "TipoPropiedadID", "Nombre");

            var modelo = new ListadoTiposPropiedadesViewModel();
            modelo.Listado = db.TiposPropiedades.Include(t => t.Propiedade).Include(t => t.Tipos);

            if (id > 0)
            {
                modelo.Listado = modelo.Listado.Where(p => p.PropiedadID == id).ToList();
                modelo.PropiedadId = id;
            }
            if (id1 > 0)
            {
                modelo.Listado = modelo.Listado.Where(p => p.TipoID == id1).ToList();
                modelo.TipoId = id1;
            }
            if (id2 > 0)
            {
                modelo.Listado = modelo.Listado.Where(p => p.Propiedade.TipoPropiedadID == id2).ToList();
                modelo.PropiedadTipoId = id2;
            }

            return View(modelo);
        }

        // GET: TiposPropiedads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPropiedad tiposPropiedad = db.TiposPropiedades.Find(id);
            if (tiposPropiedad == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(tiposPropiedad);

            return View(tiposPropiedad);
        }

        // GET: TiposPropiedads/Create
        public ActionResult Create(int id = 0, int id1 = 0, int id2 = 0)
        {
            if (id2 > 0)
            {
                ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p=>p.TipoPropiedadID == id2), "PropiedadID", "Nombre");
            }
            else
            {
                ViewBag.PropiedadID = new SelectList(db.Propiedades.OrderBy(p => p.Nombre), "PropiedadID", "Nombre");
            }

            ViewBag.TipoID = new SelectList(db.Tipos.Where(p => p.Activo), "TipoID", "Nombre", id1);

            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: TiposPropiedads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TiposPropiedadesID,TipoID,PropiedadID,Requerido,Guardar,GuardarHistorial,Orden")] TiposPropiedad tiposPropiedad)
        {
            if (ModelState.IsValid)
            {
                if (tiposPropiedad.Orden == null)
                {
                    tiposPropiedad.Orden = 0;
                }

                db.TiposPropiedades.Add(tiposPropiedad);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.PropiedadID = new SelectList(db.Propiedades.OrderBy(p => p.Nombre), "PropiedadID", "Nombre", tiposPropiedad.PropiedadID);
                    ViewBag.TipoID = new SelectList(db.Tipos.Where(p => p.Activo), "TipoID", "Nombre", tiposPropiedad.TipoID);

                    ModelState.AddModelError("", "No se ha podido Guardar Tipo Propiedad. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(tiposPropiedad);
                }
                catch (Exception ex)
                {

                    ViewBag.PropiedadID = new SelectList(db.Propiedades.OrderBy(p => p.Nombre), "PropiedadID", "Nombre", tiposPropiedad.PropiedadID);
                    ViewBag.TipoID = new SelectList(db.Tipos.Where(p => p.Activo), "TipoID", "Nombre", tiposPropiedad.TipoID);

                    ModelState.AddModelError("", "No se ha podido Guardar Tipo Propiedad. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);

                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(tiposPropiedad);
                }

                TempData["MensajeError"] = "ok|Se Ha Actualizado Correctamente.";
                return RedirectToAction("Index", new { id1 = tiposPropiedad.TipoID, id2 = db.Propiedades.Find(tiposPropiedad.PropiedadID).TipoPropiedadID });
            }

            return RedirectToAction("Index", new { id1 = tiposPropiedad.TipoID, id2 = db.Propiedades.Find(tiposPropiedad.PropiedadID).TipoPropiedadID });
        }

        // GET: TiposPropiedads/Edit/5
        public ActionResult Edit(int? id, int id1 = 0, int id2 = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPropiedad tiposPropiedad = db.TiposPropiedades.Find(id);
            if (tiposPropiedad == null)
            {
                return HttpNotFound();
            }

            if (id2 > 0)
            {
                ViewBag.PropiedadID = new SelectList(db.Propiedades.Where(p => p.TipoPropiedadID == id2).OrderBy(p => p.Nombre), "PropiedadID", "Nombre", tiposPropiedad.PropiedadID);
            }
            else
            {
                ViewBag.PropiedadID = new SelectList(db.Propiedades.OrderBy(p => p.Nombre), "PropiedadID", "Nombre", tiposPropiedad.PropiedadID);
            }

            ViewBag.TipoID = new SelectList(db.Tipos.Where(p => p.Activo), "TipoID", "Nombre", tiposPropiedad.TipoID);

            if (Request.IsAjaxRequest())
                return PartialView(tiposPropiedad);

            return View(tiposPropiedad);
        }

        // POST: TiposPropiedads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TiposPropiedadesID,TipoID,PropiedadID,Requerido,Guardar,GuardarHistorial,Orden")] TiposPropiedad tiposPropiedad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposPropiedad).State = EntityState.Modified;
                db.SaveChanges();
                TempData["MensajeError"] = "ok|Se Ha Actualizado Correctamente.";
                return RedirectToAction("Index", new { id1 = tiposPropiedad.TipoID, id2 = db.Propiedades.Find(tiposPropiedad.PropiedadID).TipoPropiedadID });

            }

            ViewBag.PropiedadID = new SelectList(db.Propiedades.OrderBy(p=>p.Nombre), "PropiedadID", "Nombre", tiposPropiedad.PropiedadID);
            ViewBag.TipoID = new SelectList(db.Tipos, "TipoID", "Nombre", tiposPropiedad.TipoID);

            return RedirectToAction("Index", new { id1 = tiposPropiedad.TipoID, id2 = db.Propiedades.Find(tiposPropiedad.PropiedadID).TipoPropiedadID });

        }

        // GET: TiposPropiedads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPropiedad tiposPropiedad = db.TiposPropiedades.Find(id);
            if (tiposPropiedad == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(tiposPropiedad);

            return View(tiposPropiedad);
        }

        // POST: TiposPropiedads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposPropiedad tiposPropiedad = db.TiposPropiedades.Find(id);
            db.TiposPropiedades.Remove(tiposPropiedad);
            db.SaveChanges();
            TempData["MensajeError"] = "ok|Se Ha Eliminado Correctamente.";

            //if (Request.IsAjaxRequest())
            //    return PartialView();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
