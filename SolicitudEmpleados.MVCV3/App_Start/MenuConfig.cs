﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.MVCV3.Componentes.Extensiones.Menu;
using SolicitudEmpleado.MVCV3.Componentes.Extensiones;

namespace SolicitudEmpleados.MVCV3
{
	/// <summary>
	/// Configuracion del Menu de Intranet
	/// </summary>
	public static class MenuConfig
	{
		/// <summary>
		/// Obtiene el texto de usuario anonimo
		/// </summary>
		const string Anonimo = "Anónimo";

		/// <summary>
		/// Configura las cadenas del menu que seran procesadas
		/// </summary>
		public static void Configurar()
		{
			ElementosProcesados.AgregarElemento("{Usuario}", MostrarUsuario);
		}

		/// <summary>
		/// Obtiene el nombre del usuario a partir del contexto HTTP
		/// </summary>
		/// <param name="viewContext">Contexto HTTP</param>
		/// <returns>Cadena con el nombre del ususario conectado</returns>
		private static string MostrarUsuario(ViewContext viewContext)
		{
			if (viewContext == null || viewContext.Controller == null)
				return Anonimo;

			var seguridad = viewContext.BaseController().ObtenerSeguridad();

			if (seguridad != null)
				return seguridad.usuario.usuario;

			return Anonimo;
		}
	}
}