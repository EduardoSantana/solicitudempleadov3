﻿

CREATE TABLE [dbo].[Accesos](
	[idAcceso] [int] NOT NULL,
	[Nombre] [varchar](256) NOT NULL,
	[Descripcion] [varchar](256) NULL,
 CONSTRAINT [PK_Accesos] PRIMARY KEY CLUSTERED 
(
	[idAcceso] ASC
)WITH (PAD_INDEX = OFF) ON [PRIMARY]
) ON [PRIMARY]



CREATE TABLE [dbo].[RolesAccesos](
	[idRol] NVARCHAR (128) NOT NULL,
	[idAcceso] [int] NOT NULL,
 CONSTRAINT [PK_RolesAccesos] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC,
	[idAcceso] ASC
)WITH (PAD_INDEX = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RolesAccesos]  WITH CHECK ADD  CONSTRAINT [FK_RolesAccesos_Accesos] FOREIGN KEY([idAcceso])
REFERENCES [dbo].[Accesos] ([idAcceso])
GO

ALTER TABLE [dbo].[RolesAccesos] CHECK CONSTRAINT [FK_RolesAccesos_Accesos]
GO

ALTER TABLE [dbo].[RolesAccesos]  WITH CHECK ADD  CONSTRAINT [FK_RolesAccesos_Roles] FOREIGN KEY([idRol])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[RolesAccesos] CHECK CONSTRAINT [FK_RolesAccesos_Roles]
GO

