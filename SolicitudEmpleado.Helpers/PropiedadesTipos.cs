﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
    public struct PropiedadesTipos
    {
        public const int EditarIngresos = 4;
        public const int TotalesIngresos = 6;
        public const int EditarDeducciones = 5;
        public const int TotalesDeducciones = 7;
        public const int Procesar = 8;
        public const int Detalle = 1;
        public const int Validaciones = 3;
        public const int ModulosQueTiene = 9;
        public const int Politicas = 10;
        public const int Documentos = 11;
        public const int TiposNotificaciones = 12;
    }
}
