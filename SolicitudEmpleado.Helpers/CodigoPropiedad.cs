﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
    public struct CodigoPropiedad
    {
        public const int SueldoFijo = 16;
        public const int Comisiones = 29;
        public const int GastosRepresentacion = 31;
        public const int OtrosIngresos = 33;
        public const int PrestamoDirecto = 35;
        public const int VacacionesFelices = 36;
        public const int PrestamoHipotecario = 37;
        public const int SeguroVidaHipotecario = 38;
        public const int SeguroIncendioHipotecario = 39;
        public const int PrestamoVehiculo = 40;
        public const int PolizaVehiculo = 41;
        public const int Cooperativa = 42;
        public const int SeguroMedico = 43;
        public const int AporteSeguridadSocial = 44;
        public const int AportePlanRetiro = 45;
        public const int OtrasDeducciones = 46;
        public const int TotalIngreso = 17;
        public const int DisponibleBruta = 18;
        public const int TotalDeducciones = 19;
        public const int DisponibilidadNeta = 20;
        public const int CalificaPorMonto = 48;
        public const int EsPrestamoUnico = 49;
        public const int LiquidacionNeta = 53;
        public const int LiquidacionBruta = 52;
        public const int DisponiblidadCargada = 54;
        public const int ModuloDeConsumo = 55;
        public const int ModuloHipotecario = 56;
        public const int DocumentosPendientes = 62;
        public const int PrestamosRechazado = 82;
        public const int URLTasadores = 77;
		public const int URLSeguro = 78;
		public const int Nombre = 80;
		public const int Apellido = 81;
		public const int Cedula = 82;
		public const int Sexo = 00;
		public const int TipoDeSangre = 83;
		public const int EstadoCivil = 84;
		public const int Telefono = 87;
		public const int FechaNacimiento = 88;
		public const int LugarNacimiento = 89;
		public const int Nacionalidad = 90;
		public const int Direccion = 91;
		public const int Correo = 116;
        public const int CabezeraNombre = 93;
        public const int CabezeraRNC = 94;
        public const int CabezeraCanalIndirecto = 95;
        public const int CabezeraVigenciadeContrato = 96;
        public const int CabezeraOtrosSubsidios = 97;
        public const int CabezeraLineasActuales = 98;
        public const int CabezeraUsuariosAbiertos = 99;
        public const int CabezeraUsuariosCerrados = 100;
        public const int DetalledePlanPlan = 101;
        public const int DetalledePlanAdicional = 102;
        public const int PlanesBusinessFitPlan = 104;
        public const int DetalledePlanCantidad = 105;
        public const int DetalledePlanMinutos = 106;
        public const int DetalledePlanRentaUnitaria = 107;
        public const int DetalledePlanRentaNeta = 108;
        public const int DetalledePlanAdicionalCantidad = 109;
        public const int DetalledePlanAdicionalMinutos = 110;
        public const int DetalledePlanAdicionalRentaUnitaria = 111;
        public const int DetalledePlanAdicionalRentaNeta = 112;
        public const int PlanesBusinessFitCantidad = 113;
        public const int PlanesBusinessFitMinutos = 114;
        public const int PlanesBusinessFitRentaUnitaria = 115;
        public const int PlanesBusinessFitRentaNeta = 116;
        public const int ModelosdeEquiposModelo = 117;
        public const int ModelosdeEquiposPrecio = 118;
        public const int ModelosdeEquiposCantidad = 119;
        public const int ModelosdeEquiposSubtotal = 120;
        public const int PlanesdeDataPlan = 121;
        public const int PlanesdeDataCantidad = 122;
        public const int PlanesdeDataRentaUnitaria = 123;
        public const int PlanesdeDataSubtotal = 124;
        public const int ServiciosOpcionalesRoamingDescripcion = 125;
        public const int ServiciosOpcionalesRoamingCantidad = 152;
        public const int ServiciosOpcionalesRoamingPrecio = 128;
        public const int ServiciosOpcionalesRoamingSubTotal = 129;

        public const int ServiciosOpcionalesDistanciaDescripcion = 1155;
        public const int ServiciosOpcionalesDistanciaCantidad = 1153;
        public const int ServiciosOpcionalesDistanciaPrecio = 1156;
        public const int ServiciosOpcionalesDistanciaSubTotal = 1157;

        public const int DetalledePlanIlimitado = 2156;
        public const int DetalledePlanIlimitadoCantidad = 2157;
        public const int DetalledePlanIlimitadoMinutos = 2158;
        public const int DetalledePlanIlimitadoRentaUnitaria = 2159;
        public const int DetalledePlanIlimitadoRentaNeta = 2160;

    }
}
