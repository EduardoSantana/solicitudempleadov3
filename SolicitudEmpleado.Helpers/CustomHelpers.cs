﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using SolicitudEmpleado.Data;
using System.Text;
using SolicitudEmpleado.Models;
using Newtonsoft.Json;

namespace SolicitudEmpleado.Helpers
{
    public static class CustomHelpers
    {
        public static string DisplayForPropiedad(this HtmlHelper helper, string valor, string formato)
        {
            string retVal = valor;
            if (formato == "currency" && !String.IsNullOrEmpty(valor))
            {
                retVal = decimal.Parse(valor.Replace(",", "")).ToString("C");
            }
            if (formato == "bool" && !String.IsNullOrEmpty(valor))
            {
                bool temp;
                if (bool.TryParse(valor, out temp))
                {
                    retVal = (temp ? "Si" : "No");
                }
                else
                {
                    retVal = valor;
                }
            }
            return retVal;
        }

        public static string DisplayForPropiedad(this HtmlHelper helper, spGetPropiedadesDeSolicitudResult obj)
        {
            string retVal = obj.Valor;
            if (obj.Tipo == "currency" && !String.IsNullOrEmpty(obj.Valor))
            {
                retVal = decimal.Parse(obj.Valor.Replace(",", "")).ToString("C");
            }
            if (obj.Tipo == "bool" && !String.IsNullOrEmpty(obj.Valor))
            {
                bool temp;
                if (bool.TryParse(obj.Valor, out temp))
                {
                    retVal = (temp ? "Si" : "No");
                }
            }

            string options = (!string.IsNullOrEmpty(obj.Opciones) ? obj.Opciones : (!string.IsNullOrEmpty(obj.OpcionesCustom) ? obj.OpcionesCustom : ""));

            if (!string.IsNullOrEmpty(options))
            {
                var optionsList = JsonConvert.DeserializeObject<List<cboPropiedades>>(options);
                var objSelected = optionsList.Where(p => p.id == retVal).FirstOrDefault();
                if (objSelected != null)
                {
                    retVal = objSelected.name;
                }
            }
            return retVal;
        }

        public static MvcHtmlString EditorForPropiedad(this HtmlHelper helper, spGetPropiedadesDeSolicitudResult obj, string nameID, string CustomClass = "")
        {
            nameID = nameID + ".Valor";
            StringBuilder sb = new StringBuilder();
            string retVal = obj.Valor;
            string controlID = nameID.Replace("[", "_").Replace("]", "_").Replace(".", "_");

            string options = (!string.IsNullOrEmpty(obj.Opciones) ? obj.Opciones : (!string.IsNullOrEmpty(obj.OpcionesCustom) ? obj.OpcionesCustom : ""));
            
            if (!string.IsNullOrEmpty(options))
            {
                sb.Append("<select class='form-control " + CustomClass + "' ");
            }
            else
            {
                if (obj.Tipo == "multiline")
                {
                    sb.Append("<textarea class='form-control text-box multi-line " + CustomClass + "' ");
                }
                if (obj.Tipo == "bool")
                {
                    sb.Append("<input type='checkbox' class='form-control check-box " + CustomClass + "' ");
                }
                else
                {
                    sb.Append("<input class='form-control text-box single-line " + CustomClass + "' ");
                }
            }

            sb.Append("data-val='true' ");

			if (obj.ValidacionJS != null && Convert.ToBoolean(obj.ValidacionJS))
			{
				sb.Append("data-validacionjs='true' ");
			}

			sb.Append("data-propiedadid='" + obj.PropiedadID.ToString() + "' ");

            if (obj.Tipo == "readonly")
            {
                sb.Append("readOnly='readonly' ");
            }

            if (obj.Tipo == "currency")
            {
                sb.Append("data-val-number='Este valor debe ser numerico.' ");
                if (String.IsNullOrEmpty(retVal))
                {
                    retVal = "0.00";
                }
                else
                {
                    retVal = decimal.Parse(retVal.Replace(",", "")).ToString("N");
                }
            }
            else
            {
                sb.Append("data-val-length='El campo debe tener un maximo de 255 caracteres.' ");
                sb.Append("data-val-length-max='255' ");
            }
            if (obj.Requerido)
            {
                sb.Append("data-val-required='El campo es requerido. '");
            }
            if (!String.IsNullOrEmpty(obj.RegularExpression))
            {
                sb.Append(@"data-val-regex='This field must match the regular expression' ");
                sb.Append(@"data-val-regex-pattern='" + obj.RegularExpression.Trim() + "' ");
            }
            if (!String.IsNullOrEmpty(obj.PlaceHolder))
            {
                sb.Append(@"placeholder='" + obj.PlaceHolder.Trim() + "' ");
            }
            sb.Append("id='" + controlID + "' ");
            sb.Append("name='" + nameID + "' ");

            if (!string.IsNullOrEmpty(options))
            {
                sb.Append(">");
                if (!String.IsNullOrEmpty(obj.PlaceHolder))
                {
                    sb.Append("<option value=''>" + obj.PlaceHolder.Trim() + "</option>");
                }
                else
                {
                    sb.Append("<option value=''>--Seleccionar--</option>");
                }
                var optionsList = JsonConvert.DeserializeObject<List<cboPropiedades>>(options);
                foreach (var item in optionsList.OrderBy(f=>f.name))
                {
                    sb.Append("<option value='" + item.id + "'");
                    if (item.id == retVal)
                    {
                        sb.Append(" selected='selected'");
                    }
                    sb.Append(">" + item.name + "</option>");
                }
                sb.Append("</select>");
            }
            else
            {
                if (obj.Tipo == "multiline")
                {
                    sb.Append(">" + retVal + "</textarea>");
                }
                else if (obj.Tipo == "bool")
                {
                    if (Convert.ToBoolean(retVal))
                    {
                        sb.Append("checked='checked' ");
                    }
                    sb.Append("value='true' ");
                    sb.Append(">");
                }
                else
                {
                    sb.Append("value='" + retVal + "' ");
                    sb.Append("type='text' ");
                    sb.Append(">");
                }

                if (obj.Tipo == "bool")
                {
                    sb.Append("<input name='" + nameID + "' type='hidden' value='false'> ");
                }
            }

            return new MvcHtmlString(sb.ToString());

        }

        public static MvcHtmlString ValidationMessageForPropiedad(this HtmlHelper helper, string nameID)
        {
            nameID = nameID + ".Valor";
            StringBuilder sb = new StringBuilder("");
            sb.Append("<span class='text-danger field-validation-valid' ");
            sb.Append("data-valmsg-for='" + nameID + "' ");
            sb.Append("data-valmsg-replace='true'> </span>");

            return new MvcHtmlString(sb.ToString());

        }

        public static MvcHtmlString GetBotonesDeAcciones(this HtmlHelper helper, IEnumerable<SelectListItem> listaEstados, string name, int idGuardar)
        {

            StringBuilder sb = new StringBuilder();

            string botonHTML = "        <button type='submit' value='{0}' name='{2}' class='btn btn-primary'> {1} </button>         ";

            foreach (SelectListItem item in listaEstados)
            {
                if (item.Value != "0" && item.Value != "")
                {
                    if (int.Parse(item.Value) == idGuardar)
                    {
                        sb.Append(string.Format(botonHTML, item.Value, "Guardar", name));
                    }
                    else
                    {
                        sb.Append(string.Format(botonHTML, item.Value, item.Text, name));
                    }
                }
            }

            return new MvcHtmlString(sb.ToString());

        }
    }
}

