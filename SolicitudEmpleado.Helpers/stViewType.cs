﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
    public struct stViewType
    {
        public const int Buscar = 0;
        public const int Pendientes = 1;
        public const int MisSolicitudes = 2;
        public const int EnProceso = 3;
    }
}
