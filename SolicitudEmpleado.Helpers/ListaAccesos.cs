﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
	public enum ListaAccesos
	{
		ModuloHipotecarioLeer = 1,
		ModuloHipotecarioEscribir,
		ModuloHipotecarioEditar,
		ModuloHipotecarioEliminar,
		ModuloConsumoLeer,
		ModuloConsumoEscribir,
		ModuloConsumoEditar,
		ModuloConsumoEliminar
	}

}
