﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
    public static class ExtencionAuditoria
    {
        public static bool AuditoriaCrear(this object obj, int codigoEmpleado)
        {
            bool retVal = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty("CreadoPor");
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, codigoEmpleado, null);
                    retVal = true;
                }
            }
            catch (Exception) {}
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty("FechaCreacion");
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, DateTime.Now, null);
                    retVal = true;
                }
            }
            catch (Exception) { }

            return retVal;
        }
        public static bool AuditoriaEditar(this object obj, int codigoEmpleado)
        {
            bool retVal = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty("ModificadoPor");
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, codigoEmpleado, null);
                    retVal = true;
                }
            }
            catch (Exception) { }
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty("FechaModificacion");
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, DateTime.Now, null);
                    retVal = true;
                }
            }
            catch (Exception) { }

            bool sobreEscribirCreacion = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty("FechaCreacion");
                if (myPropInfo != null)
                {
                    DateTime mivalor = (DateTime)myPropInfo.GetValue(obj);
                    DateTime comparar = new DateTime(1753,1,1);
                    if (mivalor < comparar)
                    {
                        sobreEscribirCreacion = true;
                    }
                }
            }
            catch (Exception) { }

            if (sobreEscribirCreacion)
            {
                obj.AuditoriaCrear(codigoEmpleado);
            }

            return retVal;
        }
    }
}
