﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Helpers
{
    public static class RegularExpresion
    {
        public static bool Validar(string patron)
        {
			if (string.IsNullOrEmpty(patron))
			{
				return true;
			}
            try
            {
                new System.Text.RegularExpressions.Regex(patron);
                return true;
            }
            catch { }
            return false;
        }
    }

}
