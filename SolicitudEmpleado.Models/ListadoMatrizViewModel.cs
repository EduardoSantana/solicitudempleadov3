﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class ListadoMatrizViewModel
    {
        public int DesdeEstadoId { get; set; }
        public IEnumerable<SolicitudEmpleado.Data.MatrizEstado> Listado { get; set; }
    }
}
