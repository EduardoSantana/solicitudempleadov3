﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
	public class RolesUsuariosViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public List<RolesListViewModel> Listado { get; set; }
	}
}
