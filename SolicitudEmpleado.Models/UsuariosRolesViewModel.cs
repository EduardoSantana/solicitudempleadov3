﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
	public class UsuariosRolesViewModel
	{
		public int idUsuario { get; set; }
		[Required]
		[StringLength(256)]
		public string apellido { get; set; }
		[Required]
		[StringLength(256)]
		public string nombre { get; set; }

		public string UserName { get; set; }

		public string UserId { get; set; }

		public List<RolesListViewModel> Listado { get; set; }
	}

	public class RolesListViewModel
	{
		public string RolId { get; set; }
		public string RolName { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }
		public string nombre { get; set; }
		public string apellido { get; set; }
		public int? IdUsuario { get; set; }
		public int? codigoEmpleado { get; set; }
	}
}
