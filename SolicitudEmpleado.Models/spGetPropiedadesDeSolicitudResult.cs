﻿namespace SolicitudEmpleado.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class spGetPropiedadesDeSolicitudResult
    {

        [Required]
        public int PropiedadID { get; set; }

        public bool Requerido { get; set; }

        public bool Guardar { get; set; }

        public bool GuardarHistorial { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [StringLength(150)]
        [DisplayName("Descripcion")]
        public string Descripcion { get; set; }

        [StringLength(10)]
        [DisplayName("Tipo")]
        public string Tipo { get; set; }

        [StringLength(200)]
        [DisplayName("Regular Expression")]
        public string RegularExpression { get; set; }

        [StringLength(50)]
        [DisplayName("PlaceHolder")]
        public string PlaceHolder { get; set; }

        [StringLength(255)]
        [DisplayName("Por Defecto")]
        public string PorDefecto { get; set; }

        [StringLength(255)]
        [DisplayName("Valor")]
        public string Valor { get; set; }

        [Required]
        public int SolicitudID { get; set; }

        public bool Existe { get; set; }

		public bool? ValidacionJS { get; set; }

        public int TipoPropiedadID { get; set; }

		public string TipoPropiedadNombre { get; set; }

        public bool EsUnico { get; set; }

        public int? Orden { get; set; }

        public int? Columnas { get; set; }

        public string Opciones { get; set; }

        public string OpcionesCustom { get; set; }
        
        public bool Agregar { get; set; }
    }
}
