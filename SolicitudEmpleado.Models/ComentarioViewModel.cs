﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolicitudEmpleado.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SolicitudEmpleado.Models
{
    public class ComentarioViewModel
    {
        public int intSolicitudID { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(1000)]
        public string strValor { get; set; }
    }
}