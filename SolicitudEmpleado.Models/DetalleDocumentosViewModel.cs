﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class DetalleDocumentosViewModel
    {
        public int SolicitudId { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> Documentos { get; set; }
    }
}
