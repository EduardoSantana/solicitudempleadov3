﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class ListadoPropiedadesViewModel
    {
        public int TipoPropiedadID { get; set; }
        public string Nombre { get; set; }
        public IEnumerable<SolicitudEmpleado.Data.Propiedad> Propiedades { get; set; }
    }
}
