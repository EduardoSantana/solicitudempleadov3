﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolicitudEmpleado.Data;
using System.ComponentModel;

namespace SolicitudEmpleado.Models
{
    public class EditarPropiedadViewModel
    {
        public int PropiedadId { get; set; }
        public string Nombre { get; set; }
        public List<TiposPropiedadCustom> Listado { get; set; }
    }

    public class TiposPropiedadCustom : TiposPropiedad
    {
        public bool Habilitar { get; set; }
        public bool Existe { get; set; }
    }
}
