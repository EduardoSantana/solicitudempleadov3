﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class DetalleDisponibViewModel
    {

        public string Identificacion {get;set;}

        public int TipoIdentificacionID { get; set; }

        [DisplayName("Codigo Empleado")]
        public int codigoEmpleado { get; set; }
        [DisplayName("Nombre")]
        public string nombreEmpleado { get; set; }
        [DisplayName("Identificacion")]
        public string cedula { get; set; }
        [DisplayName("Posicion")]
        public string posicion { get; set; }
        [DisplayName("Tiempo de Servicio")]
        public string tiempoServicio { get; set; }
        [DisplayName("Fecha de Ingreso")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime fechaIngreso { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> ingresos { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> deduccio { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> totalIngresos { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> totalDeduccio { get; set; }
    }
}
