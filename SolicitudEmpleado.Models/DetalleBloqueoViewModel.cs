﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolicitudEmpleado.Data;

namespace SolicitudEmpleado.Models
{
    public class DetalleBloqueoViewModel
    {
        public viewUsuarioBloqueados bloqueo;
        public List<BloqueosLog> bloqueosLogs;
    }
}