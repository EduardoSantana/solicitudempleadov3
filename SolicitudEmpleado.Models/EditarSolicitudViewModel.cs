﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleado.Models
{
    public class EditarSolicitudViewModel
    {
        public Solicitud solicitud { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> EditarIngresos { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> EditarDeducciones { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> TotalIngreso { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> TotalDeducciones { get; set; }
        public List<spGetPropiedadesDeSolicitudResult> Procesar { get; set; }
        public bool MostrarSoloErrorPrincipal { get; set; }
		public bool SolicitudEnviada { get; set; }
    }
}