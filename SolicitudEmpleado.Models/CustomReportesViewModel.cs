﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolicitudEmpleado.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SolicitudEmpleado.Models
{
    public class CustomReportesViewModel
    {
        [DisplayName("Fecha Inicial")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Por favor indicar la Fecha Inicial.")]
        public DateTime FechaInicial { get; set; }

        [DisplayName("Fecha Final")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Por favor indicar la Fecha Final.")]
        public DateTime FechaFinal { get; set; }

        [DisplayName("Estado")]
        public int EstatusID { get; set; }

        [DisplayName("Tipo")]
        public int TipoID { get; set; }

        public int AsignadoA { get; set; }

        public SelectList ListaAsignadoA { get; set; }

        public string Formato { get; set; }

        public List<SelectListItem> ListaFormatos
        {
            get
            {
                List<SelectListItem> retVal = new List<SelectListItem>();
                
                var item1 = new SelectListItem()
                {
                    Value = "PDF",
                    Text = "PDF"
                };
                var item2 = new SelectListItem()
                {
                    Value = "EXCEL",
                    Text = "Excel"
                };

                retVal.Add(item1);
                retVal.Add(item2);
                               
                return retVal;
            }
        }
    }
}
