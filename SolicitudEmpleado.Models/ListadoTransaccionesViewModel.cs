﻿using SolicitudEmpleado.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class ListadoTransaccionesViewModel
    {
        public IEnumerable<TransaccionesDisponiblidad> Listado { get; set; }
        public int ReferenciEikonId { get; set; }
    }
}
