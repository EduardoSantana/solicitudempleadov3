﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolicitudEmpleado.Data;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleado.Models
{
    public class DetalleSolicitudViewModel : EditarSolicitudViewModel
    {
        public IList<spGetPropiedadesDeSolicitudResult> Detalle { get; set; }
        public bool ExisteDisponibilidadCargada { get; set; }
    }
}