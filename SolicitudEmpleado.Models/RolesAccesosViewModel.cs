﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
	public class RolesAccesosViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public List<RolesAccesosListadoViewModel> Listado { get; set; }
	}
	public class RolesAccesosListadoViewModel
	{
		[Required]
		public string RoleId { get; set; }

		[Required]
		public int idAcceso { get; set; }

		[Required]
		[StringLength(256)]
		public string Nombre { get; set; }

		[StringLength(256)]
		public string Descripcion { get; set; }
	}
}
