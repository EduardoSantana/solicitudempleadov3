﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class NuevaNotificacionViewModel
    {
        public int SolicitudId { get; set; }

        [DisplayName("Tipo Notificacion")]
        public int TipoNotificacion { get; set; }
         
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage="Por favor escribir un mensaje.")]
        public string Mensaje { get; set; }
    }
}
