﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.Models
{
    public class ListadoTiposPropiedadesViewModel
    {
        public int PropiedadId { get; set; }
        public int PropiedadTipoId { get; set; }
        public int TipoId { get; set; }
        public IEnumerable<SolicitudEmpleado.Data.TiposPropiedad> Listado { get; set; }
    }
}
