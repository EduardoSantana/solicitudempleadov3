﻿namespace SolicitudEmpleado.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class spGetAsignadaResult
    {
        [Key]
        public int AsignadoA { get; set; }

        [StringLength(75)]
        public string AsignadoANombre { get; set; }

    }
}
