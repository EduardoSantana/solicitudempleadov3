﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutenticacionData.Data
{
	public class clValidarUsuario
	{
		public static bool IsValid(string userName, string userPass)
		{
			var db = new dbAutenticacion();
			var retVal = db.Usuarios.Where(p => p.activo == true && p.usuario == userName && p.clave == userPass).Any();
			return retVal;
		}
	}
}
