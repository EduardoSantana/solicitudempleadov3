﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutenticacionData.Data
{
	public enum ListaAccesos
	{
		ModuloHipotecarioLeer,
		ModuloHipotecarioEscribir,
		ModuloHipotecarioEditar,
		ModuloHipotecarioEliminar,
		ModuloConsumoLeer,
		ModuloConsumoEscribir,
		ModuloConsumoEditar,
		ModuloConsumoEliminar
	}
}
