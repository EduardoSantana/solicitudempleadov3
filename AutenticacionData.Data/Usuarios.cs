namespace AutenticacionData.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios()
        {
            Roles = new HashSet<Roles>();
        }

        [Key]
        public int idUsuario { get; set; }

        public bool activo { get; set; }

        [Required]
        [StringLength(256)]
        public string apellido { get; set; }

        [StringLength(256)]
        public string cedula { get; set; }

        [StringLength(256)]
        public string cedulaForm { get; set; }

        [StringLength(256)]
        public string celular { get; set; }

        [StringLength(256)]
        public string celularForm { get; set; }

        [StringLength(256)]
        public string codigoEmpleado { get; set; }

        [StringLength(256)]
        public string codOficina { get; set; }

        [StringLength(256)]
        public string correo { get; set; }

        [StringLength(256)]
        public string departamento { get; set; }

        public int? empresa { get; set; }

        public int? ext { get; set; }

        [StringLength(256)]
        public string ip { get; set; }

        [StringLength(256)]
        public string navegador { get; set; }

        [Required]
        [StringLength(256)]
        public string nombre { get; set; }

        [StringLength(256)]
        public string nombreCompleto { get; set; }

        [StringLength(256)]
        public string nomOficina { get; set; }

        [StringLength(256)]
        public string puesto { get; set; }

        [StringLength(256)]
        public string telefono { get; set; }

        [StringLength(256)]
        public string telefonoForm { get; set; }

        [StringLength(256)]
        public string usuario { get; set; }

        [StringLength(256)]
        public string clave { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Roles> Roles { get; set; }
    }
}
