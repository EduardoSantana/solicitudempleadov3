namespace AutenticacionData.Data
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class dbAutenticacion : DbContext
	{
		public dbAutenticacion()
			: base("name=dbAutenticacion")
		{
		}

		public virtual DbSet<Accesos> Accesos { get; set; }
		public virtual DbSet<Roles> Roles { get; set; }
		public virtual DbSet<Usuarios> Usuarios { get; set; }

		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Accesos>()
				.Property(e => e.Nombre)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.Property(e => e.Descripcion)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.HasMany(e => e.Roles)
				.WithMany(e => e.Accesos)
				.Map(m => m.ToTable("RolesAccesos").MapLeftKey("idAcceso").MapRightKey("idRol"));

			modelBuilder.Entity<Roles>()
				.Property(e => e.Nombre)
				.IsUnicode(false);

			modelBuilder.Entity<Roles>()
				.Property(e => e.Descripcion)
				.IsUnicode(false);

			modelBuilder.Entity<Roles>()
				.HasMany(e => e.Usuarios)
				.WithMany(e => e.Roles)
				.Map(m => m.ToTable("UsuariosRoles").MapLeftKey("idRol").MapRightKey("idUsuario"));

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.apellido)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.cedula)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.cedulaForm)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.celular)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.celularForm)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.codigoEmpleado)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.codOficina)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.correo)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.departamento)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.ip)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.navegador)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.nombre)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.nombreCompleto)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.nomOficina)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.puesto)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.telefono)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.telefonoForm)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.usuario)
				.IsUnicode(false);

			modelBuilder.Entity<Usuarios>()
				.Property(e => e.clave)
				.IsUnicode(false);
		}
	}
}
