namespace SolicitudEmpleado.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbSolicitudesContext : DbContext
    {
        public dbSolicitudesContext()
            : base("name=dbSolicitudesContext")
        {
        }

		public virtual DbSet<Bloqueo> Bloqueos { get; set; }
		public virtual DbSet<Invitacion> Invitaciones { get; set; }
        public virtual DbSet<BloqueosLog> BloqueosLogs { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Plane> Planes { get; set; }
        public virtual DbSet<Modelos> Modelos { get; set; }
        public virtual DbSet<Plane_Adicional> Planes_Adicional { get; set; }
        public virtual DbSet<Plane_Bsfit> Planes_Bsfit { get; set; }
        public virtual DbSet<Plane_Data> Planes_Data { get; set; }
        public virtual DbSet<Plane_Flotilla> Planes_Flotilla { get; set; }
        public virtual DbSet<Plane_Minutos> Planes_Minutos { get; set; }
        public virtual DbSet<Estatu> Estatus { get; set; }
        public virtual DbSet<MatrizEstado> MatrizEstatuses { get; set; }
        public virtual DbSet<Historial> Historials { get; set; }
        public virtual DbSet<HistorialPropiedad> HistorialPropiedades { get; set; }
        public virtual DbSet<Propiedad> Propiedades { get; set; }
        public virtual DbSet<PropiedadTipo> PropiedadTipoes { get; set; }
        public virtual DbSet<Solicitud> Solicitudes { get; set; }
        public virtual DbSet<SolicitudesPropiedad> SolicitudesPropiedades { get; set; }
        public virtual DbSet<Tipo> Tipos { get; set; }
        public virtual DbSet<TiposPropiedad> TiposPropiedades { get; set; }
        public virtual DbSet<TiposSolicitud> TiposSolicituds { get; set; }
        public virtual DbSet<TipoIdentificacion> TiposIdentificacion { get; set; }
        public virtual DbSet<TipoEmpleado> TipoEmpleados { get; set; }
        public virtual DbSet<ReferenciaEikon> ReferenciasEikons { get; set; }
        public virtual DbSet<TransaccionesDisponiblidad> TransaccionesDisponiblidades { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

			modelBuilder.Entity<Bloqueo>()
				.Property(e => e.Motivo)
				.IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Token)
				 .IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Nombre)
				 .IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Apellido)
				 .IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Cedula)
				 .IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Correo)
				 .IsUnicode(false);

			modelBuilder.Entity<Invitacion>()
				 .Property(e => e.Nota)
				 .IsUnicode(false);

            modelBuilder.Entity<TransaccionesDisponiblidad>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<BloqueosLog>()
                .Property(e => e.Motivo)
                .IsUnicode(false);

            modelBuilder.Entity<BloqueosLog>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<TipoEmpleado>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<ReferenciaEikon>()
              .Property(e => e.Nombre)
              .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Estatu>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Estatu>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Historial>()
                .HasMany(e => e.HistorialPropiedades)
                .WithRequired(e => e.Historial)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HistorialPropiedad>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.RegularExpression)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.PlaceHolder)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .Property(e => e.PorDefecto)
                .IsUnicode(false);

            modelBuilder.Entity<Propiedad>()
                .HasMany(e => e.HistorialPropiedades)
                .WithRequired(e => e.Propiedade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Propiedad>()
                .HasMany(e => e.SolicitudesPropiedades)
                .WithRequired(e => e.Propiedade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Propiedad>()
                .HasMany(e => e.TiposPropiedades)
                .WithRequired(e => e.Propiedade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PropiedadTipo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<PropiedadTipo>()
                .HasMany(e => e.Propiedades)
                .WithRequired(e => e.PropiedadTipo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Solicitud>()
                .Property(e => e.TelefonoCelular)
                .IsUnicode(false);

            modelBuilder.Entity<Solicitud>()
                .HasMany(e => e.Historials)
                .WithRequired(e => e.Solicitude)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Solicitud>()
                .HasMany(e => e.SolicitudesPropiedades)
                .WithRequired(e => e.Solicitude)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SolicitudesPropiedad>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<Tipo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Tipo>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Tipo>()
                .HasMany(e => e.TiposPropiedades)
                .WithRequired(e => e.Tipos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TiposSolicitud>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<TiposSolicitud>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoIdentificacion>()
               .Property(e => e.Nombre)
               .IsUnicode(false);

            modelBuilder.Entity<Plane>()
              .Property(e => e.name)
              .IsUnicode(false);

            modelBuilder.Entity<Plane_Adicional>()
            .Property(e => e.name)
            .IsUnicode(false);

            modelBuilder.Entity<Plane_Bsfit>()
            .Property(e => e.name)
            .IsUnicode(false);

            modelBuilder.Entity<Plane_Data>()
            .Property(e => e.name)
            .IsUnicode(false);

            modelBuilder.Entity<Plane_Flotilla>()
            .Property(e => e.name)
            .IsUnicode(false);

            modelBuilder.Entity<Plane_Minutos>()
            .Property(e => e.name)
            .IsUnicode(false);

            modelBuilder.Entity<Modelos>()
             .Property(e => e.name)
             .IsUnicode(false);

            modelBuilder.Entity<Modelos>()
            .Property(e => e.id)
            .IsUnicode(false);


        }
    }
}
