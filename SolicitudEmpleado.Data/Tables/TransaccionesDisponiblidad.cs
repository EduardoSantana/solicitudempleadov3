﻿namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransaccionesDisponiblidad")]
    public partial class TransaccionesDisponiblidad
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("TransaccionId")]
        public int TransaccionId { get; set; }

        public int ReferenciaEikonId { get; set; }

        public int Codigo { get; set; }
        
        [ScaffoldColumn(false)]
        public int? CreadoPor { get; set; }

        [ScaffoldColumn(false)]
        public DateTime FechaCreacion { get; set; }

        public int? PropiedadID { get; set; }

        public string Nombre { get; set; }

        public virtual ReferenciaEikon ReferenciaEikon { get; set; }
        
        public virtual Propiedad Propiedad { get; set; }

    }
}
