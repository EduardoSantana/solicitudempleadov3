namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PropiedadTipo")]
    public partial class PropiedadTipo
    {
        public PropiedadTipo()
        {
            Propiedades = new HashSet<Propiedad>();
        }

        [Key]
        public int TipoPropiedadID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public int? CreadoPor { get; set; }

        [DisplayName("Fecha Creacion")]
        public DateTime? FechaCreacion { get; set; }

        public int? ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public int? Columnas { get; set; }

        public int? Orden { get; set; }

        public bool Agregar { get; set; }

        public virtual ICollection<Propiedad> Propiedades { get; set; }
    }
}
