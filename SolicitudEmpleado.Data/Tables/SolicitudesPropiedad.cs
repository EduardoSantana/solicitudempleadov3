namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SolicitudesPropiedades")]
    public partial class SolicitudesPropiedad
    {
        [Key]
        public int SolicitudesPropiedadesID { get; set; }

        public int SolicitudID { get; set; }

        public int PropiedadID { get; set; }

        [StringLength(255)]
        public string Valor { get; set; }

        public virtual Propiedad Propiedade { get; set; }

        public virtual Solicitud Solicitude { get; set; }
    }
}
