namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BloqueosLog")]
    public partial class BloqueosLog
    {
        [Key]
        public int BloqueoID { get; set; }

        public int UsuarioID { get; set; }

        [StringLength(75)]
        public string Nombre { get; set; }

        [StringLength(500)]
        public string Motivo { get; set; }

        [StringLength(500)]
        public string Comentario { get; set; }

        public int? CreadoPor { get; set; }
        
        [StringLength(75)]
        public string CreadoPor_UsuarioDeRed { get; set; }

        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FechaCreacion { get; set; }

        public bool Bloqueado { get; set; }

        [Required]
        [DisplayName("Empresa")]
        public int EmpresaID { get; set; }

        public virtual Empresa Empresa { get; set; }

        [StringLength(32)]
        public string IP { get; set; }

        public string Estado
        {
            get
            {
                string retVal = "Desbloqueado";
                if (this.Bloqueado)
                {
                    retVal = "Bloqueado";
                }
                return retVal;
            }
        }
    }
}
