namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TiposPropiedades")]
    public partial class TiposPropiedad
    {
        [Key]
        public int TiposPropiedadesID { get; set; }

        public int TipoID { get; set; }

        public int PropiedadID { get; set; }

        public bool Requerido { get; set; }

        public bool Guardar { get; set; }

        [DisplayName("Historial")]
        public bool GuardarHistorial { get; set; }

        [DisplayName("Unico")]
        public bool EsUnico { get; set; }

        public int? Orden { get; set; }

        public virtual Propiedad Propiedade { get; set; }

        public virtual Tipo Tipos { get; set; }
    }
}
