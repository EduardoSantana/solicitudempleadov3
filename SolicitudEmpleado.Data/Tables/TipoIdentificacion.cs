﻿namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoIdentificacion")]
    public partial class TipoIdentificacion
    {
        public TipoIdentificacion()
        {
            Solicitudes = new HashSet<Solicitud>();
        }

        [Key]
        public int TipoIdentificacionID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Tipo Identificacion")]
        public string Nombre { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }
    }
}
