namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Bloqueo
    {
        public int BloqueoID { get; set; }

        [DisplayName("Codigo")]
        [Required(ErrorMessage = "Por favor digite un codigo.")]
        public int UsuarioID { get; set; }
       
        [StringLength(75)]
        public string Nombre { get; set; }

        [Required(ErrorMessage="Por favor digite un motivo.")]
        [StringLength(500)]
        [DataType(DataType.MultilineText, ErrorMessage = "Este campo solo acepta 500 caracteres como maximo.")]
        public string Motivo { get; set; }

        [Required(ErrorMessage = "Por favor digite un comentario o observacion.")]
        [StringLength(1000, ErrorMessage="Este campo solo acepta 1000 caracteres como maximo.")]
        [DataType(DataType.MultilineText)]
        public string Comentario { get; set; }

        public int? CreadoPor { get; set; }

        [StringLength(75)]
        [DisplayName("Usuario")]
        public string CreadoPor_UsuarioDeRed { get; set; }

        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FechaCreacion { get; set; }

        [Required]
        [DisplayName("Empresa")]
        public int EmpresaID { get; set; }

        [DisplayName("Empresa")]
        public virtual Empresa Empresa { get; set; }

        [StringLength(32)]
        public string IP { get; set; }
        
        [StringLength(32)]
        [DisplayName("Motivo")]
        public string cc_Motivo
        {
            get
            {
                string retVal = "";
                if (this.Motivo != null)
                {
                    retVal = this.Motivo;
                    if (this.Motivo.Length > 32)
                    {
                        retVal = retVal.Substring(0,32);
                    }
                }
                return retVal;
            }
        }

        [StringLength(32)]
        [DisplayName("Comentario")]
        public string cc_Comentario
        {
            get
            {
                string retVal = "";
                if (this.Comentario != null)
                {
                    retVal = this.Comentario;
                    if (this.Comentario.Length > 32)
                    {
                        retVal = retVal.Substring(0, 32);
                    }
                }
                return retVal;
            }
        }

        
        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime fecha
        {
            get
            {
                DateTime retVal = new DateTime();
                 if (this.FechaCreacion != null)
                 { 
                     retVal = (DateTime)this.FechaCreacion;
                 }
                   
                return retVal;
            }
        }
        
    }
}
