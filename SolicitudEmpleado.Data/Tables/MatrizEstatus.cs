namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MatrizEstatus")]
    public partial class MatrizEstado
    {
        [Key, Column(Order = 0)]
        [DisplayName("Estado Actual")]
        public int EstatusActualID { get; set; }

        [Key, Column(Order = 1)]
        [DisplayName("Es Supervisor")]
        public bool EsSupervisor { get; set; }

        [Key, Column(Order = 2)]
        [DisplayName("Puede Pasar")]
        public int EstatusPuedePasarID { get; set; }

        [ForeignKey("EstatusActualID")]
        public virtual Estatu EstatusActual { get; set; }

        [ForeignKey("EstatusPuedePasarID")]
        public virtual Estatu EstatusPuedePasar { get; set; }

    }
}

