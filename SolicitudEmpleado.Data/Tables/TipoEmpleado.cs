﻿namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoEmpleado")]
    public partial class TipoEmpleado
    {
        public TipoEmpleado()
        {
            Solicitudes = new HashSet<Solicitud>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("Tipo Empleado")]
        public int TipoEmpleadoID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Tipo Empleado")]
        public string Nombre { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }
    }
}
