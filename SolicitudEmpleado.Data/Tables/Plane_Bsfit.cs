namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Planes_Bsfit")]
    public partial class Plane_Bsfit
    {
        public Plane_Bsfit()
        {

        }

        [Key]
        [DisplayName("No")]
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Nombre")]
        public string name { get; set; }

        [Required]
        [DisplayName("Precio")]
        public int precio { get; set; } //  ] NULL,
        [Required]
        [DisplayName("Minutos")]
        public int minutos { get; set; } //  ] NULL,
        [Required]
        [DisplayName("Active")]
        public bool active { get; set; } //  ] NULL,

        [DisplayName("Creado Por")]
        public int CreadoPor { get; set; } //  ] NOT NULL,
        [DisplayName("Fecha Creacion")]
        public DateTime FechaCreacion { get; set; } //  ] NOT NULL,
        public int? ModificadoPor { get; set; } //  ] NULL,
        [DisplayName("Fecha Modifiacion")]
        public DateTime? FechaModificacion { get; set; } //  ] NULL,
    }
}
