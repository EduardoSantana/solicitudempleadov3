namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Propiedades")]
    public partial class Propiedad
    {
        public Propiedad()
        {
            HistorialPropiedades = new HashSet<HistorialPropiedad>();
            SolicitudesPropiedades = new HashSet<SolicitudesPropiedad>();
            TiposPropiedades = new HashSet<TiposPropiedad>();
            TrasaccionesDisponibilidades = new HashSet<TransaccionesDisponiblidad>();
        }

        [Key]
        public int PropiedadID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Propiedad")]
        public string Nombre { get; set; }

        [StringLength(1000)]
        public string Descripcion { get; set; }

        [StringLength(10)]
        public string Tipo { get; set; }

        [StringLength(200)]
        public string RegularExpression { get; set; }

        [StringLength(50)]
        public string PlaceHolder { get; set; }

        [StringLength(50)]
        public string PorDefecto { get; set; }

        [ScaffoldColumn(false)]
        public int? CreadoPor { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? FechaCreacion { get; set; }

        [ScaffoldColumn(false)]
        public int? ModificadoPor { get; set; }
         
        [ScaffoldColumn(false)]
        public DateTime? FechaModificacion { get; set; }

        public int TipoPropiedadID { get; set; }

        [StringLength(8000)]
        public string Opciones { get; set; }
		public Boolean? ValidacionJS { get; set; }

        public virtual ICollection<HistorialPropiedad> HistorialPropiedades { get; set; }

        public virtual PropiedadTipo PropiedadTipo { get; set; }

        public virtual ICollection<SolicitudesPropiedad> SolicitudesPropiedades { get; set; }

        public virtual ICollection<TiposPropiedad> TiposPropiedades { get; set; }

        public virtual ICollection<TransaccionesDisponiblidad> TrasaccionesDisponibilidades { get; set; }


    }
}
