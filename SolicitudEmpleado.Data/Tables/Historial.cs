namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Historial")]
    public partial class Historial
    {
        public Historial()
        {
            HistorialPropiedades = new HashSet<HistorialPropiedad>();
        }

        public int HistorialID { get; set; }

        [DisplayName("Solicitud No")]
        public int SolicitudID { get; set; }

        public int? EstatusID { get; set; }

        public int? CreadoPor { get; set; }

        [StringLength(256)]
        public string CreadoPorUsuarioDeRed { get; set; }

        [DisplayName("Fecha")]
        public DateTime? FechaCreacion { get; set; }

        [DisplayName("Evento")]
        public virtual Estatu Estatu { get; set; }

        public virtual ICollection<HistorialPropiedad> HistorialPropiedades { get; set; }

        public virtual Solicitud Solicitude { get; set; }

        [StringLength(1000)]
        public string Comentario { get; set; }
    }
}
