﻿namespace SolicitudEmpleado.Data
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("Invitaciones")]
	public partial class Invitacion
	{
		public Invitacion()
		{
		}

		[Key]
		public int InvitacionId { get; set; }
		
		[DisplayName("Tipo Solicitud")]
		public int TipoSolicitudID { get; set; }

		[DisplayName("Sub Tipo")]
		public int TipoID { get; set; }

		public virtual TiposSolicitud TiposSolicitud { get; set; }

		public virtual Tipo Tipo { get; set; }

		[Required]
		[StringLength(500)]
		[DisplayName("Nombre")]
		public string Nombre { get; set; }

		[StringLength(500)]
		[DisplayName("Apellido")]
		public string Apellido { get; set; }

		[Required]
		[StringLength(500)]
		[DisplayName("Correo")]
		public string Correo { get; set; }

		[StringLength(500)]
		[DisplayName("Cedula")]
		public string Cedula { get; set; }

		public bool Enviar { get; set; }

		[StringLength(500)]
		[DataType(DataType.MultilineText)]
		[DisplayName("Nota")]
		public string Nota { get; set; }

		[StringLength(500)]
		[DisplayName("Token")]
		[ScaffoldColumn(false)]
		public string Token { get; set; }

		public int? CreadoPor { get; set; }

		[DisplayName("Fecha Creacion")]
		public DateTime? FechaCreacion { get; set; }

	}
}
