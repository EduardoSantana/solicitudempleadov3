namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TiposSolicitud")]
    public partial class TiposSolicitud
    {
        public TiposSolicitud()
        {
            Estatus = new HashSet<Estatu>();
            Solicitudes = new HashSet<Solicitud>();
            Tipos = new HashSet<Tipo>();
        }

        [Key]
        public int TipoSolicitudID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Tipo Solicitud")]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public virtual ICollection<Estatu> Estatus { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }

        public virtual ICollection<Tipo> Tipos { get; set; }

		[ScaffoldColumn(false)]
		public string ViewIndex { get; set; }

		[ScaffoldColumn(false)]
		public string ViewDetails { get; set; }

		[ScaffoldColumn(false)]
		public string ViewEdit { get; set; }

		[ScaffoldColumn(false)]
		public string ViewCreate { get; set; }
    }
}
