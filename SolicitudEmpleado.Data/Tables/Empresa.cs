namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Empresa
    {
        public Empresa()
        {
            Solicitudes = new HashSet<Solicitud>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("Empresa")]
        public int EmpresaID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Empresa")]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        [StringLength(200)]
        public string Correo { get; set; }

        [StringLength(50)]
        public string Telefono { get; set; }

        public bool Activo { get; set; }

        public int? CreadoPor { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public int? ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public int? ReferenciaEikonId { get; set; }

        public virtual ReferenciaEikon ReferenciaEikon { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }
    }
}
