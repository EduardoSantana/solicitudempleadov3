﻿namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReferenciaEikons")]
    public partial class ReferenciaEikon
    {
        public ReferenciaEikon()
        {
            Empresas = new HashSet<Empresa>();
            TrasaccionesDisponibilidades = new HashSet<TransaccionesDisponiblidad>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("Eikon Id")]
        public int ReferenciaEikonId { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Referencia Eikon")]
        public string Nombre { get; set; }

        public virtual ICollection<Empresa> Empresas { get; set; }

        public virtual ICollection<TransaccionesDisponiblidad> TrasaccionesDisponibilidades { get; set; }
    }
}
