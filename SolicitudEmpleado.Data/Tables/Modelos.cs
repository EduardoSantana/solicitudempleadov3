namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Modelos")]
    public partial class Modelos
    {
        public Modelos()
        {

        }

        [Key]
        [DisplayName("No")]
        public string id { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Nombre")]
        public string name { get; set; }

        [Required]
        [DisplayName("Precio")]
        public decimal? precio { get; set; } //  ] NULL,
      
        [Required]
        [DisplayName("Active")]
        public bool active { get; set; } //  ] NULL,

        [DisplayName("Creado Por")]
        public int CreadoPor { get; set; } //  ] NOT NULL,
        [DisplayName("Fecha Creacion")]
        public DateTime FechaCreacion { get; set; } //  ] NOT NULL,
        public int? ModificadoPor { get; set; } //  ] NULL,
        [DisplayName("Fecha Modifiacion")]
        public DateTime? FechaModificacion { get; set; } //  ] NULL,
    }
}
