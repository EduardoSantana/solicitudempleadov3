namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistorialPropiedades")]
    public partial class HistorialPropiedad
    {
        [Key]
        public int HistorialPropiedadesID { get; set; }

        public int HistorialID { get; set; }

        public int PropiedadID { get; set; }

        [StringLength(255)]
        public string Valor { get; set; }

        public virtual Historial Historial { get; set; }

        public virtual Propiedad Propiedade { get; set; }
    }
}
