namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tipos")]
    public partial class Tipo
    {
        public Tipo()
        {
            Solicitudes = new HashSet<Solicitud>();
            TiposPropiedades = new HashSet<TiposPropiedad>();
        }

        [Key]
        public int TipoID { get; set; }

        [Required]
        [StringLength(50)]

        [DisplayName("Tipo")]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public int? TipoSolicitudID { get; set; }

        [ScaffoldColumn(false)]
        public int? CreadoPor { get; set; }
        
        [ScaffoldColumn(false)]
        public DateTime? FechaCreacion { get; set; }
       
        [ScaffoldColumn(false)]
        public int? ModificadoPor { get; set; }
      
        [ScaffoldColumn(false)]
        public DateTime? FechaModificacion { get; set; }

        public bool Activo { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }

        public virtual TiposSolicitud TiposSolicitud { get; set; }

        public virtual ICollection<TiposPropiedad> TiposPropiedades { get; set; }

		[DisplayName("Tipo")]
		[ScaffoldColumn(false)]
		public string NombreCal
		{
			get
			{
				string retVal = this.Nombre;
				if (this.TiposSolicitud != null)
				{
					retVal = this.TiposSolicitud.Nombre + "/" + this.Nombre;
				}
				return retVal;
			}
		}
		
		[ScaffoldColumn(false)]
		public int? Leer { get; set; }
		[ScaffoldColumn(false)]
		public int? Escribir { get; set; }
		[ScaffoldColumn(false)]
		public int? Editar { get; set; }
		[ScaffoldColumn(false)]
		public int? Eliminar { get; set; }
	}
}
