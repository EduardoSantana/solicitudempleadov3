namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Estatu
    {
        public Estatu()
        {
            Historials = new HashSet<Historial>();
            Solicitudes = new HashSet<Solicitud>();
            //EstatusActuales = new HashSet<MatrizEstado>();
            //EstatusPuedePasares = new HashSet<MatrizEstado>();
        }

        [Key]
        public int EstatusID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Estado")]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public int? TipoSolicitudID { get; set; }

        public bool Activo { get; set; }

        [StringLength(50)]
        public string Accion { get; set; }

        public bool EnviarCorreo { get; set; }

        public virtual TiposSolicitud TiposSolicitud { get; set; }

        public virtual ICollection<Historial> Historials { get; set; }

        public virtual ICollection<Solicitud> Solicitudes { get; set; }

        //public virtual ICollection<MatrizEstado> EstatusActuales { get; set; }

        //public virtual ICollection<MatrizEstado> EstatusPuedePasares { get; set; }
        
    }
}
