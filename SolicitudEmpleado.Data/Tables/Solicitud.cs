namespace SolicitudEmpleado.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Solicitudes")]
    public partial class Solicitud
    {
        public Solicitud()
        {
            TipoSolicitudID = 1;
            Historials = new HashSet<Historial>();
            SolicitudesPropiedades = new HashSet<SolicitudesPropiedad>();
        }

        [Key]
        [DisplayName("Solicitud No")]
        public int SolicitudID { get; set; }

        public int EmpresaID { get; set; }

        public int TipoSolicitudID { get; set; }

        public int TipoID { get; set; }

        public int EstatusID { get; set; }

        [DisplayName("Fecha Solicitud")]
        public DateTime? FechaSolicitud { get; set; }

        [DisplayName("Codigo")]
        public int? CodigoEmpleado { get; set; }

        [StringLength(75)]
        [DisplayName("Nombre")]
        public string NombreEmpleado { get; set; }

        [StringLength(75)]
        public string Direccion { get; set; }

        [StringLength(20)]
        [DisplayName("Telefono Oficina")]
        public string TelefonoOficina { get; set; }

        [StringLength(8)]
        public string Extension { get; set; }

        [StringLength(15)]
        [DisplayName("Telefono Celular")]
        public string TelefonoCelular { get; set; }

        [StringLength(75)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [StringLength(100)]
        public string Posicion { get; set; }

        [StringLength(100)]
        public string Departamento { get; set; }

        [StringLength(50)]
        [DisplayName("Tiempo Servicio")]
        public string TiempoServicio { get; set; }

        [StringLength(22)]
        [DisplayName("N�m.Cuenta N�mina")]
        public string Cuenta { get; set; }

        [DataType(DataType.Currency)]
        public decimal? Sueldo { get; set; }

        public int? CreadoPor { get; set; }

        [DisplayName("Fecha Creacion")]
        public DateTime? FechaCreacion { get; set; }

        public int? ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        [DisplayName("Asignado A")]
        public int? AsignadoA { get; set; }

        [StringLength(50)]
        [DisplayName("Referencia")]
        public string NumeroSolicitud { get; set; }

        [StringLength(75)]
        [DisplayName("Usuario De Red")]
        public string UsuarioDeRed { get; set; }

        [StringLength(20)]
        [DisplayName("Identificacion")]
        public string Identificacion { get; set; }

        [DisplayName("Asignado")]
        public string AsignadoANombre { get; set; }

        public int? TipoEmpleadoID { get; set; }

        [DisplayName("Monto")]
        [DataType(DataType.Currency)]
        public decimal Monto { get; set; }

        [DisplayName("Monto Aprobado")]
        [DataType(DataType.Currency)]
        public decimal MontoAprobado { get; set; }

        public int? TipoIdentificacionID { get; set; }

		public int? InvitacionId { get; set; }
        public virtual TipoIdentificacion TipoIdentificacion { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual TipoEmpleado TipoEmpleado { get; set; }

        public virtual Estatu Estatu { get; set; }

        public virtual ICollection<Historial> Historials { get; set; }

        public virtual Tipo Tipos { get; set; }

        public virtual TiposSolicitud TiposSolicitud { get; set; }

        public virtual ICollection<SolicitudesPropiedad> SolicitudesPropiedades { get; set; }

    }


}
