﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SolicitudEmpleado.Data
{
    public partial class viewUsuarioBloqueados
    {
        [DisplayName("Codigo")]
        public int UsuarioID { get; set; }

        [DisplayName("Nombre")]
        public string UsuarioNombre { get; set; }

        public int EmpresaID { get; set; }

        [DisplayName("Empresa")]
        public string EmpresaNombre { get; set; }

        public string Estado { get; set; }

        [DisplayName("Usuario")]
        public string CreadoPor { get; set; }

        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FechaCreacion { get; set; }

        public bool Bloqueado { get; set; }

        public int BloqueoID { get; set; }

        public static List<viewUsuarioBloqueados> LoadUsuariosBloqueados()
        {
            dbSolicitudesContext dbContext = new dbSolicitudesContext();
            return dbContext.Database.SqlQuery<viewUsuarioBloqueados>("SELECT * FROM [dbo].[viewUsuarioBloqueados]").ToList();
        }
    }
}
