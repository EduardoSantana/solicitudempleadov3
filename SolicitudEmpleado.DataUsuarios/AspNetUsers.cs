namespace SolicitudEmpleado.DataUsuarios
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AspNetUsers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AspNetUsers()
        {
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetRoles = new HashSet<AspNetRoles>();
        }

        public string Id { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idUsuario { get; set; }

        public bool? activo { get; set; }

        [Required]
        [StringLength(256)]
        public string apellido { get; set; }

        [StringLength(256)]
        public string cedula { get; set; }

        [StringLength(256)]
        public string cedulaForm { get; set; }

        [StringLength(256)]
        public string celular { get; set; }

        [StringLength(256)]
        public string celularForm { get; set; }

        public int? codigoEmpleado { get; set; }

        [StringLength(256)]
        public string codOficina { get; set; }

        [StringLength(256)]
        public string correo { get; set; }

        [StringLength(256)]
        public string departamento { get; set; }

        public int? empresa { get; set; }

        public int? ext { get; set; }

        [StringLength(256)]
        public string ip { get; set; }

        [StringLength(256)]
        public string navegador { get; set; }

        [Required]
        [StringLength(256)]
        public string nombre { get; set; }

        [StringLength(256)]
        public string nombreCompleto { get; set; }

        [StringLength(256)]
        public string nomOficina { get; set; }

        [StringLength(256)]
        public string puesto { get; set; }

        [StringLength(256)]
        public string telefono { get; set; }

        [StringLength(256)]
        public string telefonoForm { get; set; }

        [StringLength(256)]
        public string usuario { get; set; }

        [StringLength(256)]
        public string clave { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetRoles> AspNetRoles { get; set; }
    }
}
