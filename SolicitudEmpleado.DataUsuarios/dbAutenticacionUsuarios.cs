namespace SolicitudEmpleado.DataUsuarios
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class dbAutenticacionUsuarios : DbContext
	{
		public dbAutenticacionUsuarios()
			: base("name=dbAutenticacionUsuarios")
		{
		}

		public virtual DbSet<Accesos> Accesos { get; set; }
		public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
		public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
		public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Accesos>()
				.Property(e => e.Nombre)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.Property(e => e.Descripcion)
				.IsUnicode(false);

			modelBuilder.Entity<Accesos>()
				.HasMany(e => e.AspNetRoles)
				.WithMany(e => e.Accesos)
				.Map(m => m.ToTable("RolesAccesos").MapLeftKey("idAcceso").MapRightKey("idRol"));

			modelBuilder.Entity<AspNetRoles>()
				.HasMany(e => e.AspNetUsers)
				.WithMany(e => e.AspNetRoles)
				.Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

			modelBuilder.Entity<AspNetUsers>()
				.HasMany(e => e.AspNetUserLogins)
				.WithRequired(e => e.AspNetUsers)
				.HasForeignKey(e => e.UserId);
		}
	}
}
