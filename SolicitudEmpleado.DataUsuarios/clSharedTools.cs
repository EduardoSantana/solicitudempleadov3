﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudEmpleado.DataUsuarios
{
	public class clSharedTools
	{
		public enum ListaAccesos
		{
			ModuloHipotecarioLeer = 1,
			ModuloHipotecarioEscribir,
			ModuloHipotecarioEditar,
			ModuloHipotecarioEliminar,
			ModuloConsumoLeer,
			ModuloConsumoEscribir,
			ModuloConsumoEditar,
			ModuloConsumoEliminar
		}

		public static List<AspNetUsers> getUsersInPerms(int userId, int moduloId)
		{
			//int idModuloConsumo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosConsumo"]);
			//int idModuloHipotecario = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Seguridad.PrestamosHipotecarios"]);

			//if (moduloId == idModuloConsumo)
			//{
			//	moduloId = (int)ListaAccesos.ModuloConsumoLeer;
			//}
			//if (moduloId == idModuloHipotecario)
			//{
			//	moduloId = (int)ListaAccesos.ModuloHipotecarioLeer;
			//}
			
			var db = new dbAutenticacionUsuarios();

			if(userId > 0)
			{
				var retVal1 = (from p in db.AspNetUsers
							  from p2 in p.AspNetRoles
							  from p3 in p2.Accesos
							  where p.codigoEmpleado == userId && p3.idAcceso == moduloId
							  select p).ToList();

				return retVal1;
			}

			var retVal = (from p in db.AspNetUsers
						   from p2 in p.AspNetRoles
						   from p3 in p2.Accesos
						   where p3.idAcceso == moduloId
						   select p).ToList();

			return retVal;


		}
	}
}
