﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyLoan.CorreosManager
{
	public class MailsManager
	{
		private static dbMailsCorreosDataContext db = new dbMailsCorreosDataContext();
		private static List<CorreosEnviar> correos
		{
			get
			{
				db = new dbMailsCorreosDataContext();
				var fechaActual = DateTime.Now;
				var retVal = db.CorreosEnviars.Where(p => p.Enviado == false && p.FechaEnviar < fechaActual).ToList();
				return retVal;
			}
		}
		public static void Enviar() {
			foreach (CorreosEnviar item in correos)
			{
				SMTPMailHelper.SendMail(item.From, item.To, item.Subject, item.Body);
				var obj = db.CorreosEnviars.FirstOrDefault(p => p.CorreoId == item.CorreoId);
				obj.Enviado = true;
				obj.FechaEnviado = DateTime.Now;
				db.SubmitChanges();
			}
		}
		public static void Remover(CorreosEnviar item)
		{
			db.CorreosEnviars.DeleteOnSubmit(item);
			db.SubmitChanges();
		}
		public static void Agregar(CorreosEnviar item)
		{
			if (item.FechaEnviar == null)
				item.FechaEnviar = DateTime.Now.AddMinutes(item.Prioridad);
			
			db.CorreosEnviars.InsertOnSubmit(item);
			db.SubmitChanges();
		}
		public static void Enviar(CorreosEnviar item)
		{
			item.FechaEnviar = DateTime.Now.AddMinutes(-1);
			Agregar(item);
			Enviar();			
		}
	}

}
