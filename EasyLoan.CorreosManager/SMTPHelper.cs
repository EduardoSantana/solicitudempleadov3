﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net.Mail;
using System.Configuration;
using System.Net;
namespace EasyLoan.CorreosManager
{
	public class SMTPMailHelper
	{
		private static string SMTPServer
		{
			get { return ConfigurationManager.AppSettings.Get("SMTPServer"); }
		}

		private static string SMTPUserId
		{
			get { return ConfigurationManager.AppSettings.Get("SMTPUserId"); }
		}

		private static string SMTPPassword
		{
			get { return ConfigurationManager.AppSettings.Get("SMTPPassword"); }
		}

		private static int SMTPPort
		{
			get { return Convert.ToInt16(ConfigurationManager.AppSettings.Get("SMTPPort")); }
		}

		public static void SendMail(string sendTo, string subject, string body)
		{
			dynamic fromAddress = new MailAddress(SMTPUserId);
			dynamic toAddress = new MailAddress(sendTo);
			MailMessage mail = new MailMessage();

			var _with1 = mail;
			_with1.From = fromAddress;
			_with1.To.Add(toAddress);
			_with1.Subject = subject;

			if (body.ToLower().Contains("<html"))
			{
				_with1.IsBodyHtml = true;
			}

			_with1.Body = body;

			SmtpClient smtp = new SmtpClient(SMTPServer, SMTPPort);
			smtp.Credentials = new NetworkCredential(SMTPUserId, SMTPPassword);
			smtp.Send(mail);
		}

		public static void SendMail(string sendFrom, string sendTo, string subject, string body)
		{
			dynamic fromAddress = new MailAddress(sendFrom);
			dynamic toAddress = new MailAddress(sendTo);
			MailMessage mail = new MailMessage();

			var _with2 = mail;
			_with2.From = fromAddress;
			_with2.To.Add(toAddress);
			_with2.Subject = subject;

			if (body.ToLower().Contains("<html"))
			{
				_with2.IsBodyHtml = true;
			}

			_with2.Body = body;

			SmtpClient smtp = new SmtpClient(SMTPServer, SMTPPort);
			smtp.Credentials = new NetworkCredential(SMTPUserId, SMTPPassword);
			smtp.Send(mail);
		}

	}

}