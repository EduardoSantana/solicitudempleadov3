﻿Create Table Bitacoras (
  BitacoraId int identity primary key,
  EventoId int not null,
  Comentario varchar(2000),
  UsuarioId varchar(250)
);
Alter table Bitacoras Add CorreoId int null
Create Table Eventos (
  EventoId int identity primary key,
  Nombre varchar(500),
  Descripcion varchar(500)
);
Insert Into Eventos (Nombre, Descripcion) Values ('Notificacion Correo', 'Notificacion Correo');

Create Table CorreosEnviar(
	CorreoId int primary identity key,
	[From] varchar(500),
	[To] varchar(2000),
	[Subject] varchar(500),
	Body varchar(max),
	UsuarioId varchar(500),
	AppId int,
	Enviado bit not null, 
	FechaEnviado DateTime, 
	Error bit, 
	DetalleError varchar(max),
	FechaError DateTime,
	Reintentos int	
)
Alter Table CorreosEnviar Add FechaEnviar DateTime 
Alter Table CorreosEnviar Add Prioridad int not null
Alter Table CorreosEnviar Add FechaCreacion DateTime Not null Default (getDate());

Si tiene la fecha de visita menos a la fecha actual, entonces enviar un correo de ese cliente agregando los datos del cliente y del pretamo.
Vemos el ultimo 

select * from Prestamo where Prestamo_No = 522;
select * from Cliente;
select * from aspnet_Users;

Declare @NombreCliente as Varchar(80)  = '';
Declare @NombrePrestamdor as Varchar(80) = '';
Declare @MontoAdeudado as Varchar(30) = '50,000.00';
Declare @MontoDesembolsado as Varchar(30) = '30,000.00';
Declare @FechaDesembolsado as Varchar(30) = '2017/01/01';

select 
	@NombreCliente = c.Nombre, 
	@NombrePrestamdor = u.cci_Nombre,
	@MontoAdeudado = p.Monto_Adeudado,
	@MontoDesembolsado = p.Monto_Desembolso,
	@FechaDesembolsado = CONVERT ( varchar(30), p.Fecha_Desembolso , 111 )
from Prestamo p
inner join Cliente c on p.Cliente_No = c.Cliente_No
inner join aspnet_Users u on p.CreadoPor = u.UserName
where Prestamo_No = 522;


  
--WHILE ( SELECT AVG(ListPrice) FROM dbo.DimProduct) < $300  
--BEGIN  
--    UPDATE dbo.DimProduct  
--        SET ListPrice = ListPrice * 2;  
--    SELECT MAX ( ListPrice) FROM dbo.DimProduct  
--    IF ( SELECT MAX (ListPrice) FROM dbo.DimProduct) > $500  
--        BREAK;  
--END  
  


DECLARE @body as Varchar(max) = '
<!DOCTYPE html>
<html>
    <head>
        <title>Recordatorio de Pago</title>
    </head>
	<body style="font-family: Arial, Helvetica, sans-serif;">
		<p>
			Hola {NombreCliente},
		</p>
		<br />
		<p>
			Por favor recordar hacer el pago de RD$ {MontoAdeudado} que corresponde a la suma del total adeudado al día de hoy. Por favor evite mora e intereses por atrasos.
		</p>
		<br />
		<p>
			A continuación detalle del monto:
		</p>
		<table border="1" cellpadding="3" style="font-family: Arial, Helvetica, sans-serif;">
			<tr>
				<td align="left">Monto desembolsado:</td>
				<td align="right">RD$ {MontoDesembolsado}</td>
			</tr>
			<tr>
				<td align="left">Intereses para el {fecha}:</td>
				<td align="right">{interes}</td>
			</tr>
			<tr>
				<td align="right"><b>TOTAL:</b></td>
				<td align="right"><b>{MontoAdeudado}</b></td>
			</tr>
		</table>
		<p>
			Si ya ha realizado el pago por favor desestimar este mensaje.
		</p>
		<br />
		<p>
			Recordar que el Desembolso fue realizado el día {FechaDesembolsado}, por el Sr. {NombrePrestamdor} al Sr. {NombreCliente}.
		</p>
		<br />
		<p>Saludos Cordiales,</p>
		<p>Easy Loan App</p>
	</body>
</html>'