﻿using SolicitudEmpleado.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleado.DAL
{
    public interface ISolicitudesRepository : IGenericRepository<Solicitud>
    {
        IList<int> GetTiposPrestamos(int accesoId);
        IList<int> GetEstadosPuedePasar(int estatusActualId, bool esSupervisor);
        IList<int> GetEstadosDropDown(bool esSupervisor, int estatusPuedePasarId = 3);
        IList<spGetAsignadaResult> GetAsignados(int idGrupo = 0, int asignadoA = 0);
        IList<spGetPropiedadesDeSolicitudResult> GetPropiedades(int solicitudId);
		IList<spGetPropiedadesDeSolicitudResult> GetPropiedades(int solicitudId, int tipoPropiedad, bool isPorDefecto = false, int tipoId = 1);
		IList<spGetPropiedadesDeSolicitudResult> getPropiedadesByTipoID(int tipoId);
        IList<spGetPropiedadesDeSolicitudResult> GetDisponiblidadLista(int codigoEmpleado, int empresaId, bool tomarNombreDesdeWebService = true);
        IList<spGetPropiedadesDeSolicitudResult> GetDisponiblidadLista(int codigoEmpleado, int empresaId, int SolicitudId);
        void GuardarPropiedades(List<spGetPropiedadesDeSolicitudResult> listaPropiedades, Solicitud solicitud, int creadoPor, string usuarioDeRed, List<spGetPropiedadesDeSolicitudResult> listaHistorial);
        void GuardarPropiedades(List<spGetPropiedadesDeSolicitudResult> listaTotales, Solicitud solicitud, int creadoPor, string usuarioDeRed, List<spGetPropiedadesDeSolicitudResult> listaEditar, ref string strDisponibilidadBruta, int totalesTipo, List<spGetPropiedadesDeSolicitudResult> listaHistorial);
        void GuardarPropiedadesHistorial(List<spGetPropiedadesDeSolicitudResult> listaHistorial, Solicitud solicitud, int creadoPor, string usuarioDeRed, int EstatusIDInicial);
        spGetPropiedadesDeSolicitudResult getPropiedadById(int solicitudId, int propiedadId);
    }
}