﻿using System;
using SolicitudEmpleado.Data;
using System.Collections.Generic;
using SolicitudEmpleado.Models;

namespace SolicitudEmpleado.DAL
{
    public class UnitOfWork : IDisposable
    {
        private dbSolicitudesContext context = new dbSolicitudesContext();
        private IGenericRepository<Bloqueo> bloqueoRepository;
        private IGenericRepository<BloqueosLog> bloqueosLogRepository;
        private IGenericRepository<Empresa> empresaRepository;
        private IGenericRepository<MatrizEstado> matrizEstadosRepository; 
        private IGenericRepository<Estatu> estatusRepository;
        private IGenericRepository<Historial> historialRepository;
        private ISolicitudesRepository solicitudesRepository;
        private IGenericRepository<Tipo> tiposRepository;
		private IGenericRepository<Invitacion> invitacionRepository;

        public IGenericRepository<Bloqueo> BloqueoRepository
        {
            get
            {
                if (this.bloqueoRepository == null)
                {
                    this.bloqueoRepository = new GenericRepository<Bloqueo>(context);
                }
                return bloqueoRepository;
            }
        }

        public IGenericRepository<BloqueosLog> BloqueosLogRepository
        {
            get
            {

                if (this.bloqueosLogRepository == null)
                {
                    this.bloqueosLogRepository = new GenericRepository<BloqueosLog>(context);
                }
                return bloqueosLogRepository;
            }
        }

        public IGenericRepository<Empresa> EmpresaRepository
        {
            get
            {

                if (this.empresaRepository == null)
                {
                    this.empresaRepository = new GenericRepository<Empresa>(context);
                }
                return empresaRepository;
            }
        }

        public IGenericRepository<MatrizEstado> MatrizEstadosRepository
        {
            get
            {

                if (this.matrizEstadosRepository == null)
                {
                    this.matrizEstadosRepository = new GenericRepository<MatrizEstado>(context);
                }
                return matrizEstadosRepository;
            }
        }

        public IGenericRepository<Estatu> EstatusRepository
        {
            get
            {

                if (this.estatusRepository == null)
                {
                    this.estatusRepository = new GenericRepository<Estatu>(context);
                }
                return estatusRepository;
            }
        }

        public IGenericRepository<Historial> HistorialRepository
        {
            get
            {

                if (this.historialRepository == null)
                {
                    this.historialRepository = new GenericRepository<Historial>(context);
                }
                return historialRepository;
            }
        }

        public ISolicitudesRepository SolicitudesRepository
        {
            get
            {
                if (this.solicitudesRepository == null)
                {
                    this.solicitudesRepository = new SolicitudesRepository(context);
                }
                return solicitudesRepository;
            }
        }

        public IGenericRepository<Tipo> TiposRepository
        {
            get
            {

                if (this.tiposRepository == null)
                {
                    this.tiposRepository = new GenericRepository<Tipo>(context);
                }
                return tiposRepository;
            }
        }

		public IGenericRepository<Invitacion> InvitacionRepository
		{
			get
			{

				if (this.invitacionRepository == null)
				{
					this.invitacionRepository = new GenericRepository<Invitacion>(context);
				}
				return invitacionRepository;
			}
		}


        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}