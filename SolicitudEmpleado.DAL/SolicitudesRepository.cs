﻿using SolicitudEmpleado.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using SolicitudEmpleado.Models;
using SolicitudEmpleado.Helpers;

namespace SolicitudEmpleado.DAL
{
    public class SolicitudesRepository : GenericRepository<Solicitud>, ISolicitudesRepository
    {
        #region Solicitudes Controller

        public SolicitudesRepository(dbSolicitudesContext context) : base(context) { }

        public IList<spGetPropiedadesDeSolicitudResult> GetPropiedades(int solicitudId)
        {
            int propiedadId = 0;

            IList<spGetPropiedadesDeSolicitudResult> propiedades = consultaPropiedades(solicitudId, propiedadId);

            return propiedades;
        }

        public IList<spGetPropiedadesDeSolicitudResult> GetPropiedades(int solicitudId, int tipoPropiedad, bool isPorDefecto = false, int tipoId = 1)
        {
            int propiedadId = 0;

            IList<spGetPropiedadesDeSolicitudResult> propiedades = consultaPropiedades(solicitudId, propiedadId, tipoPropiedad, tipoId);

            if (isPorDefecto)
            {
                foreach (spGetPropiedadesDeSolicitudResult item in propiedades)
                {
                    item.Valor = item.PorDefecto;
                }
            }

            return propiedades;
        }

        /// <summary>
        /// Get Propiedad Por Id de Propiedad
        /// </summary>
        /// <param name="solicitudId">Solicitud Id</param>
        /// <param name="propiedadId">Propiedad Id</param>
        /// <returns>spGetPropiedadesDeSolicitudResult</returns>
        public spGetPropiedadesDeSolicitudResult getPropiedadById(int solicitudId, int propiedadId)
        {
            IList<spGetPropiedadesDeSolicitudResult> propiedades = consultaPropiedades(solicitudId, propiedadId);
            return propiedades.FirstOrDefault();
        }

		/// <summary>
		/// Get Propiedad Por Tipo Id
		/// </summary>
		/// <param name="tipoId">Solicitud Id</param>
		/// <returns>spGetPropiedadesDeSolicitudResult</returns>
		public IList<spGetPropiedadesDeSolicitudResult> getPropiedadesByTipoID(int tipoId)
		{
			return consultaPropiedades(0,0,0,tipoId);
		}

        public IList<spGetAsignadaResult> GetAsignados(int idModulo = 0, int asignadoA = 0)
        {
            List<spGetAsignadaResult> propiedades = new List<spGetAsignadaResult>();
            
            if (idModulo > 0)
            {
				var usuarios = DataUsuarios.clSharedTools.getUsersInPerms(asignadoA, idModulo);
                //var conexion = new ConexionMSQL(System.Configuration.ConfigurationManager.ConnectionStrings["SegIntranet"].ConnectionString);
                //var usuarios = SeguridadShared
                //    .BuscarUsuariosModulo(conexion, idModulo)
                //    .Distinct();

                foreach (var item in usuarios)
                {
					if (item.codigoEmpleado != null)
					{
						propiedades.Add(new spGetAsignadaResult() { AsignadoA = (int)item.codigoEmpleado, AsignadoANombre = item.nombre.Trim() + " " + item.apellido.Trim() });
					}
                }
            }
            else
            {
                propiedades = context.Database.SqlQuery<spGetAsignadaResult>("[dbo].[spGetAsignadoA]").ToList();
            }

            if (asignadoA > 0)
            {
                propiedades = propiedades.Where(p => p.AsignadoA == asignadoA).ToList();
            }

            return propiedades;
        }

        private IList<spGetPropiedadesDeSolicitudResult> consultaPropiedades(int solicitudId, int propiedadId, int tipoPropiedadId = 0, int tipoId = 1)
        {
            IList<spGetPropiedadesDeSolicitudResult> propiedades;

            SqlParameter solicitudIdParam = new SqlParameter("@SolicitudID", solicitudId);
            SqlParameter propiedadIdParam = new SqlParameter("@PropiedadID", propiedadId);
            SqlParameter tipoPropiedadParam = new SqlParameter("@TipoPropiedad", tipoPropiedadId);
			SqlParameter tipoIdParam = new SqlParameter("@TipoID1", tipoId);

			propiedades = context.Database.SqlQuery<spGetPropiedadesDeSolicitudResult>("[dbo].[spGetPropiedadesDeSolicitud] @SolicitudID, @PropiedadID, @TipoPropiedad, @TipoID1", solicitudIdParam, propiedadIdParam, tipoPropiedadParam, tipoIdParam).ToList();

            return propiedades;
        }

        public void GuardarPropiedades(List<spGetPropiedadesDeSolicitudResult> listaPropiedades, Solicitud solicitud, int creadoPor, string usuarioDeRed, List<spGetPropiedadesDeSolicitudResult> listaHistorial)
        {
            foreach (spGetPropiedadesDeSolicitudResult item in listaPropiedades)
            {
                var tempPropiedad = getPropiedadById(solicitud.SolicitudID, item.PropiedadID);

                tempPropiedad.Valor = item.Valor;

                bool isReadOnly = (tempPropiedad.Tipo == "readonly");

                bool isCurrency = (tempPropiedad.Tipo == "currency");

                if ((isReadOnly || isCurrency) && (!string.IsNullOrEmpty(tempPropiedad.Valor)))
                {
                    tempPropiedad.Valor = tempPropiedad.Valor.Replace(",", "");
                }

                if (tempPropiedad.Guardar)
                {
                    if (tempPropiedad.PropiedadID == CodigoPropiedad.CalificaPorMonto && tempPropiedad.Valor != null && Convert.ToDecimal(tempPropiedad.Valor) > 0)
                    {
                        solicitud.MontoAprobado = Convert.ToDecimal(tempPropiedad.Valor);
                    }
                    if (tempPropiedad.Existe)
                    {
                        solicitud.SolicitudesPropiedades.Where(sp => sp.PropiedadID == tempPropiedad.PropiedadID).FirstOrDefault().Valor = tempPropiedad.Valor;
                    }
                    else
                    {
                        solicitud.SolicitudesPropiedades.Add(new SolicitudesPropiedad() { PropiedadID = tempPropiedad.PropiedadID, Valor = tempPropiedad.Valor });
                    }
                }
                if (tempPropiedad.GuardarHistorial)
                {
                    listaHistorial.Add(tempPropiedad);
                }

            }

        }

        public void GuardarPropiedades(List<spGetPropiedadesDeSolicitudResult> listaTotales, Solicitud solicitud, int creadoPor, string usuarioDeRed, List<spGetPropiedadesDeSolicitudResult> listaEditar, ref string strDisponibilidadBruta, int totalesTipo, List<spGetPropiedadesDeSolicitudResult> listaHistorial)
        {
            string strDisponibilidadNeta = "0";

            string strTotalIngresos = TotalSuma(listaEditar.ToArray(), ref strDisponibilidadBruta, ref strDisponibilidadNeta, totalesTipo);

            if (totalesTipo == 1)
            {
                foreach (spGetPropiedadesDeSolicitudResult item in listaTotales)
                {
                    if (item.PropiedadID == CodigoPropiedad.TotalIngreso)
                    {
                        item.Valor = strTotalIngresos;
                    }
                    if (item.PropiedadID == CodigoPropiedad.DisponibleBruta)
                    {
                        item.Valor = strDisponibilidadBruta;
                    }
                }
            }
            if (totalesTipo == 2)
            {
                foreach (spGetPropiedadesDeSolicitudResult item in listaTotales)
                {
                    if (item.PropiedadID == CodigoPropiedad.TotalIngreso)
                    {
                        item.Valor = strTotalIngresos;
                    }
                    if (item.PropiedadID == CodigoPropiedad.DisponibilidadNeta)
                    {
                        item.Valor = strDisponibilidadNeta;
                        //Para Poner la Disponiblidad en Negativo.
                        //double outNeta = 0;
                        //double.TryParse(strDisponibilidadNeta, out outNeta);
                        //item.Valor = ((outNeta > 0) ? strDisponibilidadNeta : "0");
                    }
                }
            }
            if (solicitud != null)
            {
                GuardarPropiedades(listaTotales, solicitud, creadoPor, usuarioDeRed, listaHistorial);
            }
        }

        public void GuardarPropiedadesHistorial(List<spGetPropiedadesDeSolicitudResult> listaHistorial, Solicitud solicitud, int creadoPor, string usuarioDeRed, int estatusIDInicial)
        {
            Historial nuevoHistorial = new Historial()
            {
                SolicitudID = solicitud.SolicitudID,
                FechaCreacion = DateTime.Now,
                CreadoPor = creadoPor,
                CreadoPorUsuarioDeRed = usuarioDeRed,
                EstatusID = solicitud.EstatusID
            };

            foreach (spGetPropiedadesDeSolicitudResult item in listaHistorial)
            {
                if (item.GuardarHistorial)
                {
                    nuevoHistorial.HistorialPropiedades.Add(new HistorialPropiedad() { PropiedadID = item.PropiedadID, Valor = item.Valor });
                }
            }

            if (nuevoHistorial.HistorialPropiedades.Any())
            {
                solicitud.Historials.Add(nuevoHistorial);
            }
            
        }
        
        private string TotalSuma(spGetPropiedadesDeSolicitudResult[] editar, ref string idDisponiblidadBruta, ref string idDisponiblidadNeta, int tipo)
        {
            decimal retVal = 0;
            if (editar.Any())
            {
                for (int i = 0; i < editar.Length; i++)
                {

                    if (editar[i].Valor.Replace(",", "").IsNumeric())
                    {
                        retVal += decimal.Parse(editar[i].Valor.Replace(",", ""));
                    }

                }

                if (tipo == 1)
                {
                    decimal decDisponiblidadBruta = (retVal * 45 / 100);
                    idDisponiblidadBruta = decDisponiblidadBruta.ToString();
                }
                if (tipo == 2)
                {
                    decimal decDisponiblidadBruta = decimal.Parse(idDisponiblidadBruta);
                    decimal decDisponiblidadNeta = (decDisponiblidadBruta - retVal);
                    idDisponiblidadNeta = decDisponiblidadNeta.ToString();
                }

            }

            return retVal.ToString();
        }

        public IList<int> GetEstadosPuedePasar(int estatusActualId, bool esSupervisor)
        {
            var _db = new dbSolicitudesContext();

            var retVal = _db.MatrizEstatuses
                .Where(p => p.EstatusActualID == estatusActualId && p.EsSupervisor == esSupervisor)
                .Select(p => p.EstatusPuedePasarID)
                .ToList();

            return retVal;
        }

        public IList<int> GetEstadosDropDown(bool esSupervisor, int estatusPuedePasarId = 3)
        {
            var _db = new dbSolicitudesContext();

            var retVal = _db.MatrizEstatuses
                .Where(p => p.EstatusPuedePasarID == estatusPuedePasarId && p.EsSupervisor == esSupervisor)
                .Select(p => p.EstatusActualID)
                .ToList();

            return retVal;
        }

		public IList<int> GetTiposPrestamos(int accesoId)
        {
            var _db = new dbSolicitudesContext();

            var retVal = _db.Tipos.Where(p=>p.Leer == accesoId).Select(p=>p.TipoID).ToList();

            return retVal;
        }

        #endregion

        #region Disponiblidad Controller

        private List<spGetPropiedadesDeSolicitudResult> validacionPrestamoUnico(List<spGetPropiedadesDeSolicitudResult> listaUnir)
        {
            foreach (spGetPropiedadesDeSolicitudResult item in listaUnir)
            {
                if (item.EsUnico)
                {
                    item.Valor = item.PorDefecto;
                }
            }
            return listaUnir;
        }

        private List<spGetPropiedadesDeSolicitudResult> unirDuplicados(List<spGetPropiedadesDeSolicitudResult> listaUnir)
        {
            var retVal = new List<spGetPropiedadesDeSolicitudResult>();
            foreach (spGetPropiedadesDeSolicitudResult mov in listaUnir)
            {
                var itemLista = retVal.Where(p => p.PropiedadID == mov.PropiedadID);
                if (itemLista.Any())
                {
                    double valorSumar1 = double.Parse(((itemLista.FirstOrDefault().Valor != "") ? itemLista.FirstOrDefault().Valor : "0"));
                    double valorSumar2 = double.Parse(mov.Valor);
                    double valorTotal = valorSumar1 + valorSumar2;
                    itemLista.FirstOrDefault().Valor = valorTotal.ToString();
                }
                else
                {
                    retVal.Add(mov);
                }
            }
            return retVal;
        }

        public IList<spGetPropiedadesDeSolicitudResult> GetDisponiblidadLista(int codigoEmpleado, int empresaId, bool tomarNombreDesdeWebService = true)
        {
            var retValor = new List<spGetPropiedadesDeSolicitudResult>();
            retValor.Add(getPropiedadById(CodigoPropiedad.TotalIngreso, 0, 0, ""));
            retValor.Add(getPropiedadById(CodigoPropiedad.TotalDeducciones, 0, 0, ""));
            retValor.Add(getPropiedadById(CodigoPropiedad.DisponibleBruta, 0, 0, ""));
            retValor.Add(getPropiedadById(CodigoPropiedad.DisponibilidadNeta, 0, 0, ""));
            retValor.Add(getPropiedadById(CodigoPropiedad.LiquidacionBruta, 0, 0, ""));
            retValor.Add(getPropiedadById(CodigoPropiedad.LiquidacionNeta, 0, 0, ""));
            return retValor;
        }

        private spGetPropiedadesDeSolicitudResult getPropiedadById(int totalIngreso, object totalIngresos)
        {
            throw new NotImplementedException();
        }

        public IList<spGetPropiedadesDeSolicitudResult> GetDisponiblidadLista(int codigoEmpleado, int empresaId, int solicitudId)
        {
            var retValor = GetDisponiblidadLista(codigoEmpleado, empresaId, false).ToList();
            retValor.AddRange(GetPropiedades(solicitudId, PropiedadesTipos.EditarIngresos, true).ToList());
            retValor.AddRange(GetPropiedades(solicitudId, PropiedadesTipos.EditarDeducciones, true).ToList());
            
            // Unificar los resultados 
            retValor = unirDuplicados(retValor);

            // Poner Cero a Prestamo Unico
            retValor = validacionPrestamoUnico(retValor);

            return retValor.ToList();
        }
        /// <summary>
        /// Get Propiedad Por Id de Propiedad, Asignandole el valor que debe tener.
        /// </summary>
        /// <param name="propiedadId">Id de la Propiedad</param>
        /// <param name="valor">Valor a Asignar</param>
        /// <param name="solicitudId">Si existe solicitud a consultar el valor</param>
        /// <param name="nombre">Si existe un nombre a Asignar a la propiedad visualmente.</param>
        /// <returns>spGetPropiedadesDeSolicitudResult</returns>
        private spGetPropiedadesDeSolicitudResult getPropiedadById(int propiedadId, double valor, int solicitudId = 0, string nombre = "")
        {
            var retVal = new spGetPropiedadesDeSolicitudResult();

            retVal = getPropiedadById(solicitudId, propiedadId);

            if (solicitudId == 0)
            {
                retVal.Valor = valor.ToString();
            }
            if (nombre != "")
            {
                retVal.Nombre = nombre;
            }

            return retVal;
        }
        /// <summary>
        /// Get Propiedad Por Id de la transaccion de disponiblidad, Asignandole el valor que debe tener.
        /// </summary>
        /// <param name="transaccionId">El numero de Trasacion de Eikon</param>
        /// <param name="valor">Valor a Asignar<</param>
        /// <param name="eikonId"></param>
        /// <param name="isIngresos">Si es tipo ingreso o descuento</param>
        /// <param name="solicitudId">Si existe solicitud a consultar el valor</param>
        /// <param name="nombre"Si existe un nombre a Asignar a la propiedad visualmente.></param>
        /// <returns>spGetPropiedadesDeSolicitudResult</returns>
        private spGetPropiedadesDeSolicitudResult getPropiedadById(int transaccionId, double valor, int eikonId, bool isIngresos, int solicitudId = 0, string nombre = "")
        {
            var retVal = new spGetPropiedadesDeSolicitudResult();

            var _db = new dbSolicitudesContext();

            var transaccion = _db.TransaccionesDisponiblidades.Where(p => p.ReferenciaEikonId == eikonId && p.Codigo == transaccionId).FirstOrDefault();

            int propiedadId = 0;

            if (transaccion != null && transaccion.PropiedadID != null)
            {
                propiedadId = (int)transaccion.PropiedadID;
            }
            else
            {
                if (isIngresos)
                {
                    propiedadId = CodigoPropiedad.OtrosIngresos;
                }
                else
                {
                    propiedadId = CodigoPropiedad.OtrasDeducciones;
                }
            }

            retVal = getPropiedadById(propiedadId, valor, solicitudId, nombre);

            return retVal;
        }

        #endregion
    }
}